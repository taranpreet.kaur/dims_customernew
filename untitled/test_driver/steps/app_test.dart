//import 'steps/colour_parameter.dart';
import '../hooks/hook_example.dart';
import 'login_test.dart';
import 'dart:async';
import 'package:flutter_gherkin/flutter_gherkin.dart';
import 'package:gherkin/gherkin.dart';

import 'login_test.dart';


Future<void> main(){
  final config = FlutterTestConfiguration()
      ..features = [RegExp('features/*.*.feature')]
      ..reporters = [
        ProgressReporter(),
        TestRunSummaryReporter(),
        JsonReporter(path: './report.json')
      ]
     // ..hooks = [HookExample()]
      ..stepDefinitions = [loginStep()]
     // ..customStepParameterDefinitions = [ColourParameter()]
      ..restartAppBetweenScenarios = true
      ..targetAppPath = "test_driver/app.dart";
 //   ..tagExpression = "@login" //running scenarios based on the tag expressions
   return GherkinRunner().execute(config);
}