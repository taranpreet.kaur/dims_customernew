import 'package:flutter_driver/driver_extension.dart';
import 'package:customer/main.dart' as app;

void main(){
  //This line enable the extension
  enableFlutterDriverExtension();
  //call the main function of your app. or call runApp wih any widget you are interested in.
  app.main();
}