import UIKit
import Flutter
import GoogleMaps
import Firebase
import CoreLocation
//import GooglePlaces

@UIApplicationMain
@objc class AppDelegate: FlutterAppDelegate, CLLocationManagerDelegate {
  override func application(
    _ application: UIApplication,
    didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?
  ) -> Bool {
      
    GMSServices.provideAPIKey("AIzaSyBqU4wR7dFqFKT-QTCjAXVEGqvYUCu4jEQ")
   // GMSPlacesClient.provideAPIKey("YOUR API KEY")
    GeneratedPluginRegistrant.register(with: self)
  //  FirebaseApp.configure()
  //  FirebaseApp.configure()
  //  GeneratedPluginRegistrant.register(with: self)
    return super.application(application, didFinishLaunchingWithOptions: launchOptions)
  }
    
    
    override func applicationDidEnterBackground(_ application: UIApplication) {
        
        print("App is on Background Mode")

           
        
//
//        timer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(testAPICall), userInfo: nil, repeats: true)
//       // let timer = Timer(timeInterval: 0.4, repeats: true) { _ in self.testAPICall()}
//        print(timer)
        
//        timer2 = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(currentlocationmethod), userInfo: nil, repeats: true)
//       // let timer = Timer(timeInterval: 0.4, repeats: true) { _ in self.testAPICall()}
//        print(timer2)
//     //   currentlocationmethod()
//        doBackgroundTask()
        
    
        }
    
    override func applicationWillEnterForeground(_ application: UIApplication) {
        
        print("App is on Foreground Mode")
        
        
        //currentlocationmethod()
//        timer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(currentlocationmethod), userInfo: nil, repeats: true)
//       // let timer = Timer(timeInterval: 0.4, repeats: true) { _ in self.testAPICall()}
//        print(timer)
        

            // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.

        }
    
}
