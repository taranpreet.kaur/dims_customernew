import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:customer/ForgotOtp.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:http/http.dart' as http;
import 'Constant.dart';
import 'form_model.dart';
import 'my_form_text_field.dart';

class ForgotPassword extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return ForgotState();
  }
}
class ForgotState extends State<ForgotPassword> {

  // email RegExp
  final _emailRegExp = RegExp(
      r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+");
  // uniquely identifies a Form
  final _formKey = GlobalKey<FormState>();
  // holds the form data for access
  final model = FormModel();
  @override
  Widget build(BuildContext context) {
    Color color1 = _colorFromHex("#00ABC5");
    return FlutterEasyLoading(
      child:Scaffold(
        appBar: AppBar(
          title: Text("Forgot Password"),

          flexibleSpace: Image(
            image: AssetImage('assets/appbar_background.png'),
            fit: BoxFit.cover,
          ),
          backgroundColor: Colors.transparent,),
          body: SingleChildScrollView(
          padding: EdgeInsets.all(20.0),
            child: Container(
              child: Column(
                children: <Widget>[
                  Form(
                    key: _formKey,
                    child: Column(
                      children: <Widget>[
                        MyFormTextField(
                          isObscure: false,
                          decoration: InputDecoration(
                              border: OutlineInputBorder(),
                              labelText: "EmailAddress",
                              hintText: "me@abc.com",
                          ),
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Please enter an email address';
                            } else if (!_emailRegExp.hasMatch(value)) {
                              return 'Invalid email address!';
                            }
                            return null;
                          },
                          onSaved: (value) {
                            model.emailAddress = value;
                          },
                        ),
                        // FormSubmitButton(
                        Container(
                            height: 50,
                            width: 500,
                            padding: EdgeInsets.fromLTRB(10, 10, 10, 0),
                            margin: EdgeInsets.fromLTRB(10, 10, 10, 0),
                            child: RaisedButton(
                              textColor: Colors.white,
                              color: color1,
                              child: Text('Submit'),
                              onPressed: () async {
                                if (_formKey.currentState.validate()) {
                                  _formKey.currentState.save();
                                  forgotPassword(model.emailAddress.toString(),context);
                                }
                              },
                            )),
                      ],
                    ),
                  )
                ],
              ),
            )),
      ),);
  }
    @override
    void initState() {
    super.initState();
  }


    forgotPassword(String email,BuildContext context) async {
    // EasyLoading.show(status:"Loading");
    // Navigator.push(context,MaterialPageRoute(builder: (context) => ForgotOtp("email")),);

    EasyLoading.show(status:"Loading");


    // http://192.168.49.1:8080/api/ticket/index
    var url=Constants.base_url+"api/cust/requestOTP";
    var request =  http.MultipartRequest('POST',Uri.parse(url));
    print("request"+request.toString());
    request.fields['email']=email;
    var response=await request.send();
    SnackBar(content: Text('Processing Data'));
    if(response.statusCode == 200) {
      EasyLoading.dismiss(animation: false);
      EasyLoading.showToast("Success");
      Navigator.push(context,MaterialPageRoute(builder: (context) => ForgotOtp(email)),);
    }

    else if(response.statusCode == 400) {
       EasyLoading.showToast("Email is Incorrect");
    }

  }






  Color _colorFromHex(String hexColor) {
    final hexCode = hexColor.replaceAll('#', '');
    return Color(int.parse('FF$hexCode', radix: 16));
  }

}


