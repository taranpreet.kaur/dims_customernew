import 'dart:convert';
import 'package:customer/SignupDetailScreen.dart';
import 'package:customer/globals.dart';
import 'package:flutter/material.dart';
import 'package:customer/CustomerInfo.dart';
import 'package:customer/OtpPage.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'Constant.dart';
import 'form_model.dart';
import 'my_form_text_field.dart';

class SecondScreen extends StatefulWidget {
  @override
  State<SecondScreen> createState() => _SecondScreenState();
}

class _SecondScreenState extends State<SecondScreen> {
  TextEditingController name=new TextEditingController();

  TextEditingController email=new TextEditingController();

  TextEditingController companyName=new TextEditingController();

  TextEditingController phoneNumber=new TextEditingController();

  TextEditingController password=new TextEditingController();

  bool isLoading = false;

  var prefix;

  var parts;

  final _emailRegExp = RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+");

  final _formKey = GlobalKey<FormState>();

  final model = FormModel();

  dynamic selectedCompany = null;
  List<dynamic>   countryList = [];


  /*void getCountries() async{
    var response = await http.get("https://api.testwyre.com/v3/widget/supportedCountries");
    countryList = jsonDecode(response.body);
    selectedCompany = countryList[0];
    setState(() {
       model.companyName = selectedCompany;
    });

  }*/

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
   // getCountries();
  }

  @override
  Widget build(BuildContext context) {
    EasyLoading.dismiss(animation: false);

    Color color1 = _colorFromHex("#00ABC5");
    return FlutterEasyLoading(
        child:MaterialApp(
        home: SingleChildScrollView(
        padding: EdgeInsets.all(20.0),

          child: Container(


            child: Column(

              children: <Widget>[
                Form(
                  key: _formKey,
                  child: Column(

                    children: <Widget>[

                      TextFormField(

                        decoration: InputDecoration(
                          labelText: "Name",
                          hintText: "Name",
                        ),
                        keyboardType: TextInputType.text,
                        maxLength: 50,
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Please enter name';
                          }
                          return null;
                        },
                        onSaved: (value) {
                          model.name = value;
                        },
                      ),
                      TextFormField(
                      maxLength: 80,

                        decoration: InputDecoration(
                          labelText: "EmailAddress",
                          hintText: "me@abc.com",
                        ),

                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Please enter an email address';
                          }
                          else if (!_emailRegExp.hasMatch(value)) {
                            return 'Invalid email address!';
                          }
                          return null;
                        },
                        onSaved: (value) {
                          model.emailAddress = value;
                          // prefix: "date"
                          // var date = parts.sublist(1).join(':').trim();
                        },
                      ),
                      // company name


                      MyFormTextField(
                        isObscure: true,
                        decoration: InputDecoration(
                        labelText: "Password", hintText: "my password"),
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Please enter a password';
                          }
                          // else if(value.length>=10 && !value.contains(RegExp(r'\W')) && RegExp(r'\d+\w*\d+').hasMatch(value))
                          // {
                          //   EasyLoading.showToast("Valid PAssword");
                          // }
                          else {
                            EasyLoading.showToast("InValid PAssword");
                          }

                        },
                        onSaved: (value) {
                          model.password = value;
                        },
                      ),

                      TextFormField(
                        decoration: InputDecoration(
                          labelText: "Phone Number",
                          hintText: "1234567890",


                        ),
                        keyboardType: TextInputType.number,
                        maxLength: 10,

                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Please enter Phone Number';
                          }
                          return null;
                        },
                        onSaved: (value) {
                          model.phoneNumber = value;
                        },
                      ),

                      /*MyFormTextField(
                        isObscure: false,
                        decoration: InputDecoration(
                          labelText: "Domain Name",
                          enabled: false

                        ),

                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Please enter domain name';
                          }
                          return null;
                        },
                        onSaved: (value) {
                          var address = model.emailAddress.toString();

                          var parts = address.split('@');
                          var prefix = parts[1].trim();

                          model.companyName = value;
                        },
                      ),*/
                      Container(
                          child: Row(
                            children: <Widget>[

                              FlatButton(
                                textColor: color1,
                                child: Text(
                                  ' ',

                                ),
                                onPressed: () {

                                  // Navigator.push(
                                  //   context,
                                  //   MaterialPageRoute(builder: (context) => Dashboard()),
                                  // );

                                  //signup screen
                                },
                              )
                            ],
                            mainAxisAlignment: MainAxisAlignment.center,
                          )),
                      // FormSubmitButton(
                      Container(
                          height: 50
                          ,
                          width: 500,

                          margin: EdgeInsets.fromLTRB(10, 10, 10, 0),
                          child: RaisedButton(
                            textColor: Colors.white,
                            color: color1,
                            child: Text('SignUp'),
                            onPressed: () {
                              if (_formKey.currentState.validate()) {
                                _formKey.currentState.save();
                                print(model);

                                // Navigator.push(context,MaterialPageRoute(builder: (context) => DomainSelection()),);
                                var address = model.emailAddress.toString();

                                var parts = address.split('@');
                                var prefix = parts[1].trim();
                                print("==prefix"+prefix);
                              //  model.companyName = selectedCompany;
                                signUp(context,model.name.toString(),model.emailAddress.toString(),model.password.toString(),
                                    model.phoneNumber.toString(),model.companyName.toString());
    // if(validatePassword(model.password.toString())){
    //   signUp(context,model.name.toString(),model.emailAddress.toString(), model.password.toString(), model.phoneNumber.toString(),prefix);
    //
    // }
    // else {
    //   EasyLoading.showToast("Please Enter Correct format of password");
    // }
                                // EasyLoading.show(status: "Loading...");
                                 // Scaffold.of(_formKey.currentContext).showSnackBar(
                                //     SnackBar(content: Text('Processing Data')));
                              }
                            },
                          )),
                      /*Container(
                          height: 50
                          ,
                          width: 500,

                          margin: EdgeInsets.fromLTRB(10, 10, 10, 0),
                          child: RaisedButton(
                            textColor: Colors.white,
                            color: color1,
                            child: Text('Next'),
                            onPressed: () {
                              if (_formKey.currentState.validate()) {
                                _formKey.currentState.save();
                                print(model);

                                // Navigator.push(context,MaterialPageRoute(builder: (context) => DomainSelection()),);
                                var address = model.emailAddress.toString();

                                var parts = address.split('@');
                                var prefix = parts[1].trim();
                                print("==prefix"+prefix);
                              //  model.companyName = selectedCompany;
                                signUp(context,model.name.toString(),model.emailAddress.toString(),model.password.toString(),
                                    model.phoneNumber.toString(),model.companyName.toString());
    // if(validatePassword(model.password.toString())){
    //   signUp(context,model.name.toString(),model.emailAddress.toString(), model.password.toString(), model.phoneNumber.toString(),prefix);
    //
    // }
    // else {
    //   EasyLoading.showToast("Please Enter Correct format of password");
    // }
                                // EasyLoading.show(status: "Loading...");
                                 // Scaffold.of(_formKey.currentContext).showSnackBar(
                                //     SnackBar(content: Text('Processing Data')));
                              }
                            },
                          )),*/
                      // Validate returns true if the form is valid, otherwise false.
                    ],
                  ),
                )
              ],
            ),
          )),
    ),);
  }

  var nameList;

  var name1;

  bool validateStructure(String value){
    String  pattern = r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#\$&*~]).{8,}$';
    RegExp regExp = new RegExp(pattern);
    return regExp.hasMatch(value);
  }

  String validatePassword(String value) {
    Pattern pattern =
        r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#\$&*~]).{8,}$';
    RegExp regex = new RegExp(pattern);
    print(value);
    if (value.isEmpty) {
      return 'Please enter password';
    } else {
      if (!regex.hasMatch(value))
        return 'Enter valid password';
      else
        return null;
    }
  }

    signUp(BuildContext context,String name,email, pass,phoneNumber,companyName) async {
    EasyLoading.show(status:"Loading");
    print("==name"+name);
    print("==email"+email);
    print("==pass"+pass);
    print("==phoneNumber"+phoneNumber);
    print("==companyName"+companyName);
    var url=Constants.base_url+"api/cust/user";
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    var request =  http.MultipartRequest('POST',Uri.parse(url));
    // request.fields['name']='asdadaddsds';
    // request.fields['email']="joy2368@gmail.com";
    // request.fields['password']='1234567';
    // request.fields['phoneNumber']='935524623223';
    request.fields['name']=name;
    request.fields['email']=email;
    request.fields['password']=pass;
    request.fields['phoneNumber']=phoneNumber;
    request.fields['domain_name']= email.toString().split("@")[1];
    Globals.domainName = email.toString().split("@")[1];
    var response=await request.send();
    if(response.statusCode == 200) {
      EasyLoading.dismiss(animation: false);
      var result = await http.Response.fromStream(response);
      print("==result"+result.body.toString());
      var data = json.decode(result.body);
      var user=data['users'];
      var id=user['_id'];
     // var comapny=data['companies'];
     // var customerarray=comapny['customerarray']  as List;
     //  var list=customerarray.map<CustomerInfo>((json) => CustomerInfo.fromJson(json)).toList();
     //  for(int i=0;i<list.length;i++)
     //  {
     //      name1=list[i].name.toString();
     //      print("name"+ name);
     //      nameList=List<String>();
     //      nameList.add(name1);
     //  }
    //  Navigator.push(context, MaterialPageRoute(builder: (context) => OtpPage(email)),);
      /*Navigator.push(context, MaterialPageRoute(builder: (context){
        return SignUPOtherDetail();
      }));*/

      Navigator.pushAndRemoveUntil<dynamic>(
        context,
        MaterialPageRoute<dynamic>(
          builder: (BuildContext context) => OtpPage(email),
        ),
            (route) => false,//if you want to disable back feature set to false
      );
      var sharedPreferences    = await SharedPreferences.getInstance();
      sharedPreferences.setString("response", result.body.toString());
      sharedPreferences.setString("userid", id);
      sharedPreferences.setString('email', email);
      sharedPreferences.setString('domain_name', companyName);

     // print("The user name is" + companyName);
    }
    else {
      var result = await http.Response.fromStream(response);
      print("==result"+result.body.toString());
      var data = json.decode(result.body);
      var user=data['message'];

      EasyLoading.showError(user.toString());
      print("==response error"+response.toString());
    }
  }

    Color _colorFromHex(String hexColor) {
    final hexCode = hexColor.replaceAll('#', '');
    return Color(int.parse('FF$hexCode', radix: 16));
  }
}