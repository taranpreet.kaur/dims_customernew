// import 'package:flutter/cupertino.dart';
// import 'package:flutter/material.dart';
// //import 'TicketListing.dart';
// //void main() => runApp(EngineerEvaluationForm());
//
// class Notifications extends StatelessWidget {
//   // static const String _title = 'Flutter Code Sample';
//   // List<String> litems = ["New","Accepted","Onhold","Resolve","Part req"];
//   List<String> litems = ["Pending","On Hold","Received","Pending","On Hold","Received",
//     "Pending","On Hold","Received","Pending","On Hold","Received","Pending","On Hold","Received"];
//   @override
//   Widget build(BuildContext context) {
//     // Color color1 = _colorFromHex("#00ABC5");
//     return MaterialApp(
//         debugShowCheckedModeBanner: false,
//         home:Scaffold(appBar: AppBar(centerTitle: false,
//           //leading: Icon(Icons.arrow_back),
//           title: Text("Conveyance Payment"),
//           flexibleSpace: Image(
//             image: AssetImage('assets/image/appbar_background.png'),
//             fit: BoxFit.cover,
//           ),
//           backgroundColor: Colors.transparent,),
//           body: Column(
//             children: [
//               Container(
//                 child: Padding(
//                   padding: EdgeInsets.all(20.0),
//                   child: Row(
//                     children: [
//                       Expanded(
//                         flex: 1,
//                         child: Text("Date"),
//                       ),
//                       Expanded(
//                         flex: 1,
//                         child: Text("KM"),
//                       ),
//                       Expanded(
//                         flex: 1,
//                         child: Text("Amount"),
//                       ),
//                       Expanded(
//
//                         //flex: 1,
//
//
//                           child: Padding(
//                             padding: const EdgeInsets.only(right: 10),
//                             child: Text("Status"),
//                           )
//
//                       ),
//                     ],
//                   ),
//                 ),
//               ),
//               Container(
//                 height: 0.1,
//                 color: Colors.black,
//               ),
//               Expanded(
//                   child:
//                   ListView.separated(
//                     separatorBuilder: (context, index) => Divider(
//                       color: Colors.black,
//                     ),
//                     itemCount: this.litems.length,
//                     itemBuilder: (context, index) => Padding(
//                       padding: EdgeInsets.all(8.0),
//                       child: InkWell(
//                         onTap: (){
//                           // leading: Icon(Icons.arrow_back);
//                           //   Navigator.push(context,MaterialPageRoute(builder:(context)=> TicketListing()),);
//                         },
//                         child: Container(
//                           height: 30.0,
//                           child: Row(
//                             children: [
//                               Expanded(
//                                 flex: 1,
//                                 child: Text("12 Nov 2020"),
//                               ),
//                               Expanded(
//                                 flex: 1,
//                                 child: Text("150"),
//                               ),
//                               Expanded(
//                                 flex: 1,
//
//
//                                 child: Text("RS 1500"),
//                               ),
//                               Expanded(
//                                 flex: 1,
//                                 child: Text("${this.litems[index]}",style: TextStyle(
//                                     color:  "${this.litems[index]}" == "Pending" ? Colors.orange : "${this.litems[index]}" == "On Hold" ?
//                                     Colors.red :
//                                     "${this.litems[index]}" == "Received" ? Colors.green : ""
//                                         "${this.litems[index]}" == "Resolve" ? Colors.yellow :
//                                     "${this.litems[index]}" == "Part req" ? Colors.grey : Colors.grey
//                                   //(1==1)?Colors.blue:Colors.orange
//                                 ),),
//                               ),
//                             ],
//                           ),
//                         ),
//                       ),
//                     ),
//                   )
//               ),
//             ],
//
//           ),
//         ));
//     // for (var i = 0; i < 10; i++) {
//     // if (i==)
//     //children.add(new ListTile());
//     // }
//   }
// }

import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import 'NotificationResponse.dart';
//void main() => runApp(Notifications());
String token;
SharedPreferences sharedPreferences;
/// This Widget is the main application widget.
///

class Notifications extends StatefulWidget {
  NotificationsView createState()=> NotificationsView();

}
class NotificationsView extends State<Notifications> {

  Future<bool> _onWillPop() async {
    return (await showDialog(
      context: context,
      builder: (context) => new AlertDialog(
        title: new Text('Are you sure?'),
        content: new Text('Do you want to exit from App'),
        actions: <Widget>[
          FlatButton(
            onPressed: () => Navigator.of(context).pop(true),
            child: new Text('Yes'),
          ),
         FlatButton(
            onPressed: () => Navigator.of(context).pop(false),
            child: new Text('No'),
          ),
        ],
      ),
    )) ?? false;
  }



  @override
  Widget build(BuildContext context) {
    getData();
    return new WillPopScope(
      onWillPop: _onWillPop,
      child: MaterialApp(
        home: Scaffold(
          appBar: AppBar(title: Text('Notification')),
          backgroundColor: _colorFromHex("#F8FCFF"),

          body:FutureBuilder(
              future: getData(),
              builder: (context, snapshot) {
                return snapshot.data != null
                    ? listViewWidget(snapshot.data)
                    : Center(child: CircularProgressIndicator());
              }),
        ),
      ),
    );

  }

  // child: ListView.builder(
  //         itemCount: article.length,
  //
  //         padding: const EdgeInsets.all(2.0),
  //         itemBuilder: (context, position) {
  //           return Card(
  //             child: ListTile(
  //               title: Text(
  //                 '${article[position].notification_details.title}',
  //                 style: TextStyle(
  //                     fontSize: 18.0,
  //                     color: Colors.black,
  //                     fontWeight: FontWeight.normal),
  //               ),
  //               subtitle: Text('${article[position].notification_details.body}',style: TextStyle(fontSize: 16.0,color:Colors.black,fontWeight:FontWeight.normal),),
  //
  //
  //             ),
  //           );
  //         }),

  Widget listViewWidget(List<NotificationResponse> article) {
    // return Container(
    //   child: ListView.builder(
    //       itemCount: article.length,
    //
    //       padding: const EdgeInsets.all(2.0),
    //       itemBuilder: (context, position) {
    return Center(
        child: ListView.builder(
          // separatorBuilder: (context, index) => Divider(
          //   //color: Colors.white,
          // ),
            itemCount: article.length,
            itemBuilder: (context, index) => Padding(
                padding: EdgeInsets.fromLTRB(7, 1, 7, 4),
                child: InkWell(
                  onTap: () async {


                  },
//                           child: Container(
//                             height: 120,
//                             child: Card(
//
//                               // shape: RoundedRectangleBorder(
//                               //   borderRadius: BorderRadius.circular(1.0),
//                               // ),
//                               // color: Colors.white,
//                               //elevation: 3,
//                               child: Row(
//                                 mainAxisSize: MainAxisSize.min,
//                                 children: <Widget>[
//                                   Container(
//                                     height: 120,
//                                     width: 2 ,
// color: Colors.red,
//
//                                   ),
//                                   SizedBox(
//                                     width: 13,
//
//                                   ),
//                                   Container(
//                                     height: 40,
//                                     width: 60 ,
//
//                                     child: Text("26 Nov 2021"),
//                                   ),
//                                   SizedBox(
//                                     // width: ,
//
//                                   ),
//                                   Container(
//
//                                     height: 70,
//                                     width: 2,
//                                     color: Colors.black12,
//                                   ),
//                                   SizedBox(
//                                     width: 10,
//
//                                   ),
//                                   Container(
//                                     height: 100,
//
//                                     child: Column(
//                                         // mainAxisAlignment: MainAxisAlignment.start,
//                                         // mainAxisSize: MainAxisSize.min,
//                                         children: <Widget>[
//                                           // Align(
//                                           //
//                                           //     child: Text("Sector 18, Gurugram,\nHaryana 122022",
//                                           //       style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold,
//                                           //           color: Color(0xFF101010)),)
//                                           // ),
//                                           SizedBox(
//                                             height: 8,
//                                           ),
//
//
//                                          Padding(
//                                             padding: EdgeInsets.fromLTRB(3, 3, 0, 0),
//                                               child: Align(
//
//                                                  alignment: Alignment.bottomLeft,
//                                                child: Text(
//                                                  '${article[index].notification_details.title}',
//                                                  style: TextStyle(fontSize: 20.0,color: Colors.black),
//                                                textAlign: TextAlign.left ,
//                                                ),
//                                            ),
//                                             ),
//
//
//
//                                        SizedBox(
//                                          height: 10,
//                                        ),
//                                           Container(
//                                              width: 300,
//                                             child: Text(
//                                                 '${article[index].notification_details.body}'
//                                                 ,
//                                                 overflow: TextOverflow.ellipsis,
//                                                 maxLines: 5,
//
//                                                 style: TextStyle(fontSize: 15.0)
//                                             )
//                                           ),
//                                        ]
//                                     ),)
//
//
//
//                                 ],
//                               ),
//                             ),
//                           ),
                  child: Container(
                    height: 160,
                    child: Card(

                      // shape: RoundedRectangleBorder(
                      //   borderRadius: BorderRadius.circular(1.0),
                      // ),
                      // color: Colors.white,
                      //elevation: 3,
                      child: Row(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Container(
                            height: 160,
                            width: 2 ,

                            color: Colors.red,
                          ),
                          SizedBox(
                            width: 13,

                          ),
                          Container(
                            height: 60,
                            width: 50 ,

                            child: Text('${article[index].notification_details.date}'),
                          ),
                          SizedBox(
                            width: 10 ,

                          ),
                          Container(

                            height: 70,
                            width: 2,
                            color: Colors.black12,
                          ),
                          SizedBox(
                            width: 20,

                          ),
                          Container(
                            height: 120,

                            child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                // mainAxisAlignment: MainAxisAlignment.start,
                                // mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  // Align(
                                  //
                                  //     child: Text("Sector 18, Gurugram,\nHaryana 122022",
                                  //       style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold,
                                  //           color: Color(0xFF101010)),)
                                  // ),
                                  SizedBox(
                                    height: 8,
                                  ),


                                  Align(
                                    alignment: Alignment.centerLeft,
                                    child: Container(
                                      //color: Colors.blue,
                                      child: Text(
                                        '${article[index].notification_details.title}',
                                        style: TextStyle(fontSize: 20.0,color: Colors.black),
                                        //  textAlign: TextAlign.left ,
                                      ),
                                    ),
                                  ),



                                  SizedBox(
                                    height: 4,
                                  ),
                                  Container(
                                      width: 240,
                                      height:  80,
                                      child: Text(
                                          '${article[index].notification_details.body}'
                                          ,
                                          overflow: TextOverflow.ellipsis,
                                          maxLines: 6,

                                          style: TextStyle(fontSize: 15.0)
                                      )
                                  ),
                                ]
                            ),)



                        ],
                      ),
                    ),
                  ),

                )
            )
        )
    );
    //       }),
    // );
  }
}

Color _colorFromHex(String hexColor) {
  final hexCode = hexColor.replaceAll('#', '');
  return Color(int.parse('FF$hexCode', radix: 16));
}
// class MyCardWidget extends StatelessWidget {
//   MyCardWidget({Key key}) : super(key: key);
//   List<String> litems = ["New","Accepted","On Hold","Resolve","Part req","New","Accepted",
//     "On Hold"];
//   // List<String> litems = ["New","Accepted","Onhold","Resolve","Part req"];
//
//   @override
//   void initState()  {
//     getData();
//
//   }
//   @override
//   Widget build(BuildContext context) {
//
//     return Center(
//         child: ListView.separated(
//             separatorBuilder: (context, index) => Divider(
//               color: Colors.white,
//             ),
//             itemCount: litems.length,
//             itemBuilder: (context, index) => Padding(
//                 padding: EdgeInsets.all(8.0),
//                 child: InkWell(
//                   onTap: () async {
//
//                     // leading: Icon(Icons.arrow_back);
//                     // Navigator.push(context,MaterialPageRoute(builder:(context)=> TicketListing()),);
//                     // SharedPreferences pref=await SharedPreferences.getInstance();
//                     // pref.setString("ticketId", controller.result[index].sId);
//
//                   },
//                   child: Card(
//                     shape: RoundedRectangleBorder(
//                       borderRadius: BorderRadius.circular(15.0),
//                     ),
//                     color: Colors.grey,
//                     elevation: 10,
//                     child: Column(
//                       mainAxisSize: MainAxisSize.min,
//                       children: <Widget>[
//                         const ListTile(
//                           leading: Icon(Icons.album, size: 60),
//                           title: Text(
//                               'Sonu Nigam',
//                               style: TextStyle(fontSize: 20.0)
//                           ),
//                           subtitle: Text(
//                               'Best of Sonu Nigam Music.',
//                               style: TextStyle(fontSize: 15.0)
//                           ),
//                         ),
//
//                       ],
//                     ),
//                   ),
//                 )
//             )
//         )
//     );
//   }
// }
getSharedprefence ()async{
  SharedPreferences sharedPreferences ;
  sharedPreferences    = await SharedPreferences.getInstance();
  token = sharedPreferences.getString("token") ;
}


Map<String, String> get headers => {

  //"Content-Type": "application/json",
  "token": token,
  // "Authorization": "Bearer $_token",
};
getData() async {
  List<NotificationResponse> list;
  print("called get data");
  final sharedPreferences = await SharedPreferences.getInstance();
  var id=sharedPreferences.get("token");
  var idd=sharedPreferences.getString("token");

  print("==shared in notifo"+id+"hjhj"+idd);

  var request = http.MultipartRequest(
      'POST', Uri.parse("http://10.11.4.59:8080/api/extranet/get/notification"));
  print("request" + request.toString());



  request.fields['user_id'] = sharedPreferences.getString("userID");
  request.headers.addAll(headers);
  var response = await request.send();

  print(response.statusCode);

  // listen for response
  if (response.statusCode == 200) {
    var result = await http.Response.fromStream(response);
    var jsonResponse = json.decode(result.body);
    //print(jsonResponse);


    var texts =  jsonResponse["resp"];

    print( texts);

    list = texts.map<NotificationResponse>((json) => NotificationResponse.fromJson(json)).toList();
    //print("==result" + result.body.toString());
    return list;

  } else {
    print("==response error" + response.toString());
  }

//   var response = await request.send();
//   print(response);
//   if (response.statusCode == 200) {
//     var result = await http.Response.fromStream(response);
// print(result.toString());
//
//
//   } else {
//     print("==response error" + response.toString());
//   }
}

class NotificationResponse{
  Notify notification_details;

  NotificationResponse(
      {
        this.notification_details

      });
  factory NotificationResponse.fromJson(Map<String, dynamic> json) {
    return NotificationResponse(notification_details : Notify.fromJson(json["notification_details"]));
  }
}


class Notify {

  String title;
  String body;
  String ticket_id;
  String date;


  Notify(
      {
        this.title,
        this.body,
        this.ticket_id,
        this.date,
      });

  factory Notify.fromJson(Map<String, dynamic> json) {
    return Notify(

      title: json["title"],
      body: json["body"],
      ticket_id:json["ticket_id"],
      date:json["date"],
    );
  }

  static Map<String, dynamic> toMap(Notify music) => {
    'title': music.title,
    'body': music.body,
    'ticket_id':music.ticket_id,
    'date':music.date

  };


}