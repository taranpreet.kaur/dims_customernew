import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';

class MyFormTextField extends StatelessWidget {
  Function(String) onSaved;
  InputDecoration decoration;
  Function(String) validator;
  final bool isObscure;
  TextInputFormatter inputFormatter;

  MyFormTextField(
      {this.isObscure, this.decoration, this.validator, this.onSaved,this.inputFormatter});

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      //
    //  key: ValueKey("passwordTf"),
      obscureText: isObscure,
      decoration: decoration,
      validator: validator,
      onSaved: onSaved,
    );
  }
}
