import 'dart:async';
import 'dart:developer';
import 'package:connectivity/connectivity.dart';
import 'package:date_time_picker/date_time_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'Article.dart';
import 'Services.dart';
import 'package:intl/intl.dart';
import 'package:scroll_to_index/scroll_to_index.dart';
import 'Services1.dart';
import 'TicketDetails.dart';
import 'globals.dart';


class ResolveTickets extends StatefulWidget {
  ResolveTickets() : super();
  final String title = "Filter List Demo";

  @override
  ResolveTicketsState createState() => ResolveTicketsState();
}

class Debouncer {
  final int milliseconds;
  VoidCallback action;
  Timer _timer;
  Timer timer1;

  Debouncer({this.milliseconds});

  run(VoidCallback action) {
    if (null != _timer) {
      _timer.cancel();
    }
    _timer = Timer(Duration(milliseconds: milliseconds), action);
  }
}

class ResolveTicketsState extends State<ResolveTickets> {
  //
  final _debouncer = Debouncer(milliseconds: 500);
  List<Article> users = List();
  List<Article> filteredUsers = List();

  final newList = [];
  ConnectionState connectionState;
  var isLoading = false;
  GlobalKey globalKey = GlobalKey();
  String filterText = "";
  List<Article>  cancelledList = [];
  List<Article>  resolvedList = [];
  Timer timer1;
  bool isApiLoading = true;


  @override
  void initState() {
    super.initState();
    if (Globals.filterText.isNotEmpty) {
      filterText = Globals.filterText;
    } else {
      filterText = "";
    }
    controller = AutoScrollController(
        viewportBoundaryGetter: () =>
            Rect.fromLTRB(0, 0, 0, MediaQuery.of(context).padding.bottom),
        axis: scrollDirection);
    setState(() {
      isLoading = true;
      CircularProgressIndicator();
    });


   getAllResolvedUsers();
   // autoRefreshDetails();
  }

  final scrollDirection = Axis.vertical;

  AutoScrollController controller;

  void filterList() {
    Future.delayed(Duration.zero, () {
      if (filterText.isNotEmpty) {
        Globals.filterText = "";
        if (filterText == "TODAYRESOLVED") {
          buttonList([
            "PART_RESOLVED",
            "STR_RESOLVED",
            "RIM_RESOLVED",
            "ENG_RESOLVED",
            "ENG_RESO",
            "PENG_RESOLVED",
            "PENG_RESO",
            "IM_RESOLVED",
            "STR_SPR_DLVR-R",
            "STR_STDBY_PICK",
            "STR_REP_DLVRD_FP",
            "STR_REPD_NTBP",
          ]);
        }
      } else {
        print("Filter text null");
      }
    });
  }


  /// for refresh
  Widget _buildList() {
    return isLoading? Center(
      child: CircularProgressIndicator(),
    ):filteredUsers.length !=0
        ? RefreshIndicator(
      child: Card(
        child: Padding(
          padding: EdgeInsets.all(0.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                color: Color((0xFFF8FCFF)),
                padding: EdgeInsets.fromLTRB(0, 5, 0, 280),
                height: MediaQuery.of(context).size.height,
                width: MediaQuery.of(context).size.width,
                child: ListView.separated(
                  //padding: EdgeInsets.fromLTRB(20, 20, 20, 20),
                  physics: const BouncingScrollPhysics(parent: AlwaysScrollableScrollPhysics()),
                  separatorBuilder: (context, index) =>
                      Divider(
                        color: Colors.transparent,
                      ),
                  itemCount: filteredUsers.length,
                  itemBuilder: (context, index) => Padding(
                    padding: EdgeInsets.all(2.0),
                    child: InkWell(
                      onTap: () async {
                        // print("ticketid"+ticketId[index]);
                        //   Article aa=new Article();
                        // var id=aa.id;
                        // print("id search page"+id);

                        SharedPreferences sharedPreferences =
                        await SharedPreferences
                            .getInstance();
                        sharedPreferences.setString(
                            "ticket_id",
                            filteredUsers[index].id);
                        Globals.fromScreen = "resolved_tickets";

                        sharedPreferences.setString("status", _status(this.filteredUsers[index].status, this.filteredUsers[index].lastRemark, this.filteredUsers[index].isPartRequested));
                        // if(aa.assetType==true)
                        //   {
                        //
                        //    CupertinoColors.extraLightBackgroundGray;
                        //   }
                        // Navigator.push(context, MaterialPageRoute(builder: (context) => TicketDetails(filteredUsers[index].id)),);
                        Navigator.of(context,
                            rootNavigator: true)
                            .push(
                          MaterialPageRoute(
                            builder: (context) =>
                                TicketDetails(
                                    filteredUsers[index].id),
                          ),
                        );
                      },
                      // TicketDetails(filteredUsers[index].id))
                      child: Container(
                        height: 60.0,

                        padding:
                        EdgeInsets.fromLTRB(0, 5, 5, 5),
                        child: Column(children: <Widget>[
                          Row(
                           // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Expanded(
                                  flex: 1,
                                  child: Padding(
                                    padding:
                                    EdgeInsets.fromLTRB(
                                        1, 1, 1, 1),

                                    child: Text(
                                      "${this.filteredUsers[index].ticketSerialNo}",
                                      textAlign:
                                      TextAlign.left,

                                      // textAlign: TextAlign.right,
                                      style: TextStyle(
                                        fontSize: 12.0,
                                        // color:  "${this.dogsBreedList[index]}" == "New" ? Colors.orange : "${this.dogsBreedList[index]}" == "On Hold" ?
                                        // Colors.red :
                                        // "${this.dogsBreedList[index]}" == "Except" ? Colors.green : ""
                                        //     "${this.dogsBreedList[index]}" == "Resolve" ? Colors.yellow :
                                        // "${this.dogsBreedList[index]}" == "Hold On" ? Colors.grey : Colors.grey
                                        //(1==1)?Colors.blue:Colors.orange
                                      ),
                                    ),

                                    // this doesn't work for top and bottom
                                  )),
                              // SizedBox(width: 5),
                              Expanded(
                                flex: 1,
                                // var at = toBeginningOfSentenceCase(this.filteredUsers[index].assetType);
                                child: Text(
                                  assetType(this
                                      .filteredUsers[index]
                                      .assetType),
                                  textAlign: TextAlign.left,
                                  style: TextStyle(
                                    fontSize: 12.0,
                                    // color:  "${this.dogsBreedList[index]}" == "New" ? Colors.orange : "${this.dogsBreedList[index]}" == "On Hold" ?
                                    // Colors.red :
                                    // "${this.dogsBreedList[index]}" == "Except" ? Colors.green : ""
                                    //     "${this.dogsBreedList[index]}" == "Resolve" ? Colors.yellow :
                                    // "${this.dogsBreedList[index]}" == "Hold On" ? Colors.grey : Colors.grey
                                    //(1==1)?Colors.blue:Colors.orange
                                  ),
                                ),
                              ),

                              // DateTime dateTime = DateTime.parse(this.filteredUsers[index].ticketDate);
                              // SizedBox(width: 20),
                              Expanded(
                                flex: 1,
                                child: Text(
                                  "${this.filteredUsers[index].ticketDate}",
                                  textAlign: TextAlign.left,
                                  style: TextStyle(
                                    fontSize: 12.0,
                                    // color:  "${this.dogsBreedList[index]}" == "New" ? Colors.orange : "${this.dogsBreedList[index]}" == "On Hold" ?
                                    // Colors.red :
                                    // "${this.dogsBreedList[index]}" == "Except" ? Colors.green : ""
                                    //     "${this.dogsBreedList[index]}" == "Resolve" ? Colors.yellow :
                                    // "${this.dogsBreedList[index]}" == "Hold On" ? Colors.grey : Colors.grey
                                    //(1==1)?Colors.blue:Colors.orange
                                  ),
                                ),
                              ),

                              // SizedBox(width: 10),

                              Expanded(
                                flex: 1,
                                child: Text(
                                  "${_status(this.filteredUsers[index].status, this.filteredUsers[index].lastRemark, this.filteredUsers[index].isPartRequested)}",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      fontSize: 12.0),
                                ),
                              ),
                              Expanded(
                                flex: 1,
                                child: Align(
                                  alignment: Alignment.centerRight,
                                  child: Text(

                                      dateCreation(filteredUsers[index].status == "TICKET_CANCELLED" ? filteredUsers[index].ticketCancelDate :
                                      filteredUsers[index].ticketResolvedDate,filteredUsers[index].status),
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                      fontSize: 12.0
                                    ),
                                  ),
                                ),
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              Container(
                                padding: EdgeInsets.fromLTRB(
                                    1, 1, 1, 1),
                                height: 0.1,
                                color: Colors.black,
                              )
                            ],
                          ),
                        ]),
                        // ),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
      onRefresh: _getData,
    )
        : Center(child: Padding(
          padding: const EdgeInsets.all(20.0),
          child: Container(child: Text("No data found",
            style: TextStyle(fontWeight: FontWeight.bold),),),
        ));
  }

  /// resolved api function
  Future<void> getAllResolvedUsers() async{
    resolvedList.clear();
    cancelledList.clear();
    filteredUsers.clear();
   isApiLoading = true;
    await Services1.getUsers1().then((usersFromServer)
    {
      setState(() {
        isApiLoading = false;
        isLoading = false;
        // if(connectionState==ConnectionState.waiting)
        // {
        //   loadingView();
        // }
        // else if(connectionState==ConnectionState.active){
        //
        // }

        users = usersFromServer.cast<Article>();
        // here we create two separate lists for cancelled and resolved status and add item to it
        users.forEach((element) {
          if(element.status == "TICKET_CANCELLED"){
            cancelledList.add(element);
          }else{
            resolvedList.add(element);
          }

        });
        // here we are sorting the both list according the date
        resolvedList.sort((a,b) {
          final DateFormat formatter = DateFormat('dd-MM-yyyy HH:mm:ss a');
          DateTime aDateTime = formatter.parse(a.ticketDate.toUpperCase());
          DateTime bDateTime = formatter.parse(b.ticketDate.toUpperCase());
          return bDateTime.compareTo(aDateTime);
        });

        cancelledList.sort((a,b) {
          final DateFormat formatter = DateFormat('dd-MM-yyyy HH:mm:ss a');
          DateTime aDateTime = formatter.parse(a.ticketDate.toUpperCase());
          DateTime bDateTime = formatter.parse(b.ticketDate.toUpperCase());
          return bDateTime.compareTo(aDateTime);
        });


        Future.delayed(Duration.zero, () {
          //  filteredUsers.addAll(users);
          // here we are ading those both list one by one first resolved and ater that cancelled
          filteredUsers.addAll(resolvedList);
          filteredUsers.addAll(cancelledList);

          setState(() {});
          if (Globals.filterText.isNotEmpty) {
            filterList();
          }
        });
      });
    }
    );
  }

  // for pull to refresh


  Future<void> _getData() async {
    print("data is pull to refresh");
    setState(() {
      getAllResolvedUsers();
    });
  }

  void autoRefreshDetails() {
    timer1 = Timer.periodic(Duration(seconds: 30), (Timer t) {
      if(!isApiLoading){
      //  getAllResolvedUsers();
      }
    });
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    // this will stop api calling
    if (timer1 != null) {
      timer1.cancel();
    }
  }



  @override
  Widget build(BuildContext context) {
    Color color1 = _colorFromHex("#00ABC5");

    return new MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        key: globalKey,
        resizeToAvoidBottomInset: false,
        // resizeToAvoidBottomPadding: false,
        // sir can i have your number
        body: WillPopScope(
          onWillPop: _onBackPressed,
          child: RefreshIndicator(
            onRefresh: _getData,
            child: SingleChildScrollView(
              child: Column(children: <Widget>[
                SizedBox(
                  height: 10,
                ),
                Container(
                  height: 50.0,
                  alignment: Alignment.centerLeft,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Align(
                        alignment: Alignment.centerLeft,
                        child: Container(
                          width: MediaQuery.of(context).size.width * 0.80,
                          height: 80,
                          child: TextField(
                            inputFormatters: <TextInputFormatter>[
                              FilteringTextInputFormatter.allow(
                                  RegExp("[0-9a-zA-Z]")),
                            ],
                            // maxLength: 15,
                            decoration: InputDecoration(
                                contentPadding: EdgeInsets.all(15.0),
                                hintStyle: TextStyle(fontSize: 14.0),
                                hintText: 'Search by Ticket Id & Asset Type',
                                prefixIcon: Icon(Icons.search),
                                border: OutlineInputBorder(
                                  borderRadius:
                                  BorderRadius.all(Radius.circular(25.0)),
                                )),

                            onChanged: (string) {
                              _debouncer.run(() {
                                setState(() {
                                  filteredUsers = users
                                      .where((u) => (u.assetType
                                      .toLowerCase()
                                      .contains(string.toLowerCase()) ||
                                      u.ticketSerialNo
                                          .toLowerCase()
                                          .contains(string.toLowerCase())))
                                      .toList();
                                });
                              });
                            },
                          ),
                        ),
                      ),
                      /*InkWell(
                         child: Ink(
                           height: 40.0,
                           width: 50.0,
                           child: Icon(Icons.filter_alt_rounded,size: 30.0,),


                         ),

                         onTap: (){
                           showFilterDialog();
                         },
                         // child:
                         // shape: CircleBorder(side: BorderSide(color: Colors.transparent)),
                       ),*/
                      Flexible(
                        child: SizedBox(
                          width: MediaQuery.of(context).size.width * 0.15,
                          height: 80.0,
                          child: IconButton(
                            icon: Container(
                              padding: EdgeInsets.only(bottom: 30.0),
                              child: Icon(Icons.filter_alt_rounded),
                            ),
                            // textColor: Colors.white,
                            onPressed: () {
                              showFilterDialog();
                            },
                            // child:
                            // shape: CircleBorder(side: BorderSide(color: Colors.transparent)),
                          ),
                        ),
                      ),
                    ],
                  ),
                  padding: EdgeInsets.symmetric(horizontal: 10.0),
                ),
                Container(
                    padding: EdgeInsets.fromLTRB(0, 20, 0, 0),
                    child: Row(
                      mainAxisSize: MainAxisSize.min,
                    //  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Expanded(
                          flex: 1,
                          child: Text("Ticket Id.",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  fontSize: 12.0,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.black)),
                        ),

                        // SizedBox(width: 8,),
                        Expanded(
                            flex: 1,
                            child: Text("Asset Type",
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    fontSize: 12.0,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.black))),

                        // SizedBox(width: 20,),
                        Expanded(
                          flex: 1,
                          child: Text("Created Date",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  fontSize: 12.0,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.black)),
                        ),

                        Expanded(
                          flex: 1,
                          child: Align(
                            child: Text("Status",
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    fontSize: 12.0,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.black)),
                          ),
                        ),
                        Expanded(
                          flex: 1,
                          child: Align(
                            child: Text("resolve Date",
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    fontSize: 12.0,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.black)),
                          ),
                        ),
                      ],
                    )),
                _buildList(),
               /* Expanded(
                  // child: _buildList(),
                  child: isLoading
                      ? Center(
                    child: CircularProgressIndicator(),
                  )
                      : filteredUsers.length > 0 ?
                  RefreshIndicator(

                    child: ListView.builder(
                        scrollDirection: scrollDirection,
                        physics: AlwaysScrollableScrollPhysics(),
                        controller: controller,
                        shrinkWrap: true,
                        itemCount: filteredUsers.length,
                        padding: EdgeInsets.only(bottom: 100.0,top: 0.0),
                        itemBuilder: (BuildContext context, int index) {
                          return Card(
                            child: Padding(
                              padding: EdgeInsets.all(0.0),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Container(
                                    color: Color((0xFFF8FCFF)),
                                    padding: EdgeInsets.fromLTRB(5, 5, 5, 280),
                                    height: MediaQuery.of(context).size.height,
                                    width: MediaQuery.of(context).size.width,
                                    child: ListView.separated(
                                      //padding: EdgeInsets.fromLTRB(20, 20, 20, 20),
                                      separatorBuilder: (context, index) =>
                                          Divider(
                                            color: Colors.transparent,
                                          ),
                                      itemCount: filteredUsers.length,
                                      itemBuilder: (context, index) => Padding(
                                        padding: EdgeInsets.all(2.0),
                                        child: InkWell(
                                          onTap: () async {
                                            // print("ticketid"+ticketId[index]);
                                            //   Article aa=new Article();
                                            // var id=aa.id;
                                            // print("id search page"+id);

                                            SharedPreferences sharedPreferences =
                                            await SharedPreferences
                                                .getInstance();
                                            sharedPreferences.setString(
                                                "ticket_id",
                                                filteredUsers[index].id);
                                            // if(aa.assetType==true)
                                            //   {
                                            //
                                            //    CupertinoColors.extraLightBackgroundGray;
                                            //   }
                                            // Navigator.push(context, MaterialPageRoute(builder: (context) => TicketDetails(filteredUsers[index].id)),);
                                            Navigator.of(context,
                                                rootNavigator: true)
                                                .push(
                                              MaterialPageRoute(
                                                builder: (context) =>
                                                    TicketDetails(
                                                        filteredUsers[index].id),
                                              ),
                                            );
                                          },
                                          // TicketDetails(filteredUsers[index].id))
                                          child: Container(
                                            height: 40.0,

                                            padding:
                                            EdgeInsets.fromLTRB(5, 5, 5, 5),
                                            child: Column(children: <Widget>[
                                              Row(
                                                mainAxisAlignment:
                                                MainAxisAlignment
                                                    .spaceBetween,
                                                children: [
                                                  Expanded(
                                                      flex: 1,
                                                      child: Padding(
                                                        padding:
                                                        EdgeInsets.fromLTRB(
                                                            1, 1, 1, 1),

                                                        child: Text(
                                                          "${this.filteredUsers[index].ticketSerialNo}",
                                                          textAlign:
                                                          TextAlign.center,

                                                          // textAlign: TextAlign.right,
                                                          style: TextStyle(
                                                            fontSize: 12.0,
                                                            // color:  "${this.dogsBreedList[index]}" == "New" ? Colors.orange : "${this.dogsBreedList[index]}" == "On Hold" ?
                                                            // Colors.red :
                                                            // "${this.dogsBreedList[index]}" == "Except" ? Colors.green : ""
                                                            //     "${this.dogsBreedList[index]}" == "Resolve" ? Colors.yellow :
                                                            // "${this.dogsBreedList[index]}" == "Hold On" ? Colors.grey : Colors.grey
                                                            //(1==1)?Colors.blue:Colors.orange
                                                          ),
                                                        ),

                                                        // this doesn't work for top and bottom
                                                      )),
                                                  // SizedBox(width: 5),
                                                  Expanded(
                                                    flex: 1,
                                                    // var at = toBeginningOfSentenceCase(this.filteredUsers[index].assetType);
                                                    child: Text(
                                                      assetType(this
                                                          .filteredUsers[index]
                                                          .assetType),
                                                      textAlign: TextAlign.center,
                                                      style: TextStyle(
                                                        fontSize: 12.0,
                                                        // color:  "${this.dogsBreedList[index]}" == "New" ? Colors.orange : "${this.dogsBreedList[index]}" == "On Hold" ?
                                                        // Colors.red :
                                                        // "${this.dogsBreedList[index]}" == "Except" ? Colors.green : ""
                                                        //     "${this.dogsBreedList[index]}" == "Resolve" ? Colors.yellow :
                                                        // "${this.dogsBreedList[index]}" == "Hold On" ? Colors.grey : Colors.grey
                                                        //(1==1)?Colors.blue:Colors.orange
                                                      ),
                                                    ),
                                                  ),

                                                  // DateTime dateTime = DateTime.parse(this.filteredUsers[index].ticketDate);
                                                  // SizedBox(width: 20),
                                                  Expanded(
                                                    flex: 1,
                                                    child: Text(
                                                      "${this.filteredUsers[index].ticketDate}",
                                                      textAlign: TextAlign.center,
                                                      style: TextStyle(
                                                        fontSize: 12.0,
                                                        // color:  "${this.dogsBreedList[index]}" == "New" ? Colors.orange : "${this.dogsBreedList[index]}" == "On Hold" ?
                                                        // Colors.red :
                                                        // "${this.dogsBreedList[index]}" == "Except" ? Colors.green : ""
                                                        //     "${this.dogsBreedList[index]}" == "Resolve" ? Colors.yellow :
                                                        // "${this.dogsBreedList[index]}" == "Hold On" ? Colors.grey : Colors.grey
                                                        //(1==1)?Colors.blue:Colors.orange
                                                      ),
                                                    ),
                                                  ),

                                                  // SizedBox(width: 10),

                                                  Expanded(
                                                    flex: 1,
                                                    child: Text(
                                                      "${_status(this.filteredUsers[index].status, this.filteredUsers[index].lastRemark, this.filteredUsers[index].isPartRequested)}",
                                                      textAlign: TextAlign.center,
                                                      style: TextStyle(
                                                          fontSize: 12.0),
                                                    ),
                                                  ),
                                                  SizedBox(
                                                    height: 10,
                                                  ),
                                                  Container(
                                                    padding: EdgeInsets.fromLTRB(
                                                        1, 1, 1, 1),
                                                    height: 0.1,
                                                    color: Colors.black,
                                                  )
                                                ],
                                              ),
                                            ]),
                                            // ),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          );
                        }),
                    onRefresh: _getData,
                  ) : Center(child: Container(child: Text("No data found"),)),
                ),*/
              ]),
            ),
          ),
        ),
      ),
    );
  }
//// conver date from utc

  dateCreation(date,status) {

    try {
      var dateTime = DateFormat("yyyy-MM-ddTHH:mm:ssZ").parse(date, true);
      var dateLocal = dateTime.toLocal();


      DateTime parseDatess =
      new DateFormat("yyyy-MM-ddTHH:mm:ssZ").parse(date);
      String formattedDate = DateFormat('yy-MM-dd hh:mm:ss aaa').format(dateLocal);
      print("print Date"+formattedDate);


      var newFormat = DateFormat("dd-MM-yyyy hh:mm:ss aaa");
      String updatedDtss = newFormat.format(dateLocal);
      return updatedDtss;
    } on Exception catch (e) {
      print("Status Exception => "+status.toString());
      if(date != null){
        print("DateFormat => "+date);
        return "NA";
      }else{
        return "Not Available";
      }

      // TODO
    }
  }
  String formatDate(String date) {
    DateTime tempDate = new DateFormat("yyyy-MM-dd hh:mm:ss").parse(date);
  }

  assetType(assettype) {
    var at = toBeginningOfSentenceCase(assettype);
    return at;
  }

  date(date) {
    var newDate = DateFormat('dd-MM-yyyy').format(date);
    return newDate;
  }

  _status(status, lastRemark, isPartRequested) {
    if (status == "RIM_NEW") {
      if (isPartRequested == "1") {
        return 'WIP';
      } else {
        return 'NEW';
      }
      // _getColorByEvent('New');

    } else if (status == "RIM_ACCEPT") {
      // _getColorByEvent('WIP');
      return 'WIP';
    } else if (status == "RIM_WIP") {
      // _getColorByEvent('WIP');
      return "WIP";
    } else if (status == "RIM_UOB") {
      //_getColorByEvent('Under Observation');
      return "Under Observation";
    } else if (status == "RIM_PA") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "RIM_PDC") {
      //_getColorByEvent('HOLD');
      return "On HOLD";
    } else if (status == "RIM_FAULTPRST") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "RIM_RESOLVED") {
      //_getColorByEvent('RESOLVED');
      return "RESOLVED";
    } else if (status == "RIM_TRNSIM") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "RIM_TOE") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "RIM_FAULTPR") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "RIM_UNDER_OBSERVATION") {
      return "Under Observation";
    } else if (status == "RIM_TIM") {
      return "WIP";
    } else if (status == "RIM_TENG") {
      return "WIP";
    } else if (status == "RIM_PART_AUTH") {
      return "WIP";
    } else if (status == "RIM_FPR") {
      return "WIP";
    } else if (status == "RIM_FPR_SR") {
      return "WIP";
    } else if (status == "RIM_RTN_ENG") {
      return "WIP";
    } else if (status == "RIM_STD_REQ") {
      return "WIP";
    } else if (status == "RIM_FPR_STD_REQ") {
      return "WIP";
    }
    if (status == "ENG_NEW") {
      //_getColorByEvent('New');
      return 'NEW';
    } else if (status == "ENG_ACCEPT") {
      //_getColorByEvent('Assign to Aggent');
      return 'Assigned to Engineer';
    } else if (status == "ENG_REJECT") {
      //_getColorByEvent('New');
      return "NEW";
    } else if (status == "ENG_PFS") {
      //_getColorByEvent('Aggent on the way');
      return "Engineer on the way";
    } else if (status == "ENG_RAS") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "ENG_RESOLVED") {
      //_getColorByEvent('RESOLVED');

      return "RESOLVED";
    } else if (status == "ENG_UOB") {
      //_getColorByEvent('WIP');
      return "Under Observation";
    } else if (status == "ENG_SPAREQ") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "ENG_SPR_REQ") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "ENG_WIP") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "ENG_PDC") {
      //_getColorByEvent('WIP');
      return "On Hold";
    } else if (status == "ENG_RESO") {
      //_getColorByEvent('WIP');

      return "RESOLVED";
    } else if (status == "ENG_TRANSIM") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "ENG_TRANS_IM") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "ENG_TRNS_IM") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "ENG_WPS") {
      //_getColorByEvent('WIP');
      return "WIP";
    }
    if (status == "PENG_NEW") {
      //_getColorByEvent('New');
      return 'NEW';
    } else if (status == "PENG_ACCEPT") {
      //_getColorByEvent('Assign to Aggent');
      return 'Assign to Engineer';
    } else if (status == "PENG_REJECT") {
      //_getColorByEvent('New');
      return "NEW";
    } else if (status == "PENG_PFS") {
      //_getColorByEvent('Aggent on the way');
      return "Engineer on the way";
    } else if (status == "PENG_RAS") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "PENG_RESOLVED") {
      //_getColorByEvent('RESOLVED');

      return "RESOLVED";
    } else if (status == "PENG_UOB") {
      //_getColorByEvent('WIP');
      return "Under Observation";
    } else if (status == "PENG_SPAREQ") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "PENG_TRNS_PART") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "PENG_WPS") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "PENG_SPR_REQ") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "PENG_WIP") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "ENG_STDBY_INSTL") {
      //_getColorByEvent('WIP');
      return "Standby Provided";
    } else if (status == "PENG_PDC") {
      //_getColorByEvent('WIP');
      return "On Hold";
    } else if (status == "PENG_RESO") {
      //_getColorByEvent('WIP');

      return "RESOLVED";
    } else if (status == "PENG_STDBY_INSTL") {
      //_getColorByEvent('WIP');
      return "Standby Provided";
    } else if (status == "PENG_TRANSIM") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "PENG_TRANS_IM") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "PENG_TRNS_IM") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "Standby Delivered") {
      //_getColorByEvent('WIP');
      return "Standby Given";
    } else if (status == "Standby Delivered and Faulty Picked") {
      //_getColorByEvent('WIP');
      return "Standby Given";
    } else if (status == "IM_NEW") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "RIM_TIM") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "IM_WIP") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "IM_ASGND_PRTNR") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "IM_ASGND_ENGNR") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "IM_PDC") {
      //_getColorByEvent('WIP');
      return "On Hold";
    } else if (status == "IM_OBS") {
      //_getColorByEvent('WIP');
      return "Under Observation";
    } else if (status == "IM_RESOLVED") {
      //_getColorByEvent('WIP');

      return "RESOLVED";
    } else if (status == "IM_SPART_REQ") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "IM_TRANSFER_RIM") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "IM_WPS") {
      //_getColorByEvent('WIP');
      return "WIP";
    }

    if (status == "STR_NEW") {
      //_getColorByEvent('WIP');
      return 'WIP';
    }
    if (status == "STR_UP") {
      //_getColorByEvent('WIP');
      return 'WIP';
    } else if (status == "STR_WIP") {
      //_getColorByEvent('WIP');
      return 'WIP';
    } else if (status == "STR_SPR_BP") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "STR_SPR_DSPTH") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "STR_SPR_DLVR-R") {
      //_getColorByEvent('WIP');

      return "RESOLVED";
    } else if (status == "STR_SPR_DSPTH-ER") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "STR_STDBY_DSPTH") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "STR_STDBY_DLVR") {
      //_getColorByEvent('WIP');
      return "Standby Given";
    } else if (status == "STR_STDBY_PICK") {
      //_getColorByEvent('WIP');

      return "RESOLVED";
    } else if (status == "STR_FLTY_PFR") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "STR_STDBY-DLVR_FP") {
      //_getColorByEvent('WIP');
      return "Standby Given";
    } else if (status == "STR_REP_DLVRD_FP") {
      //_getColorByEvent('WIP');
      return "RESOLVED";
    } else if (status == "STR_REPD_NTBP") {
      //_getColorByEvent('WIP');
      return "RESOLVED";
    } else if (status == "STR_TRNS_IM") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "STR_TRNS_TRC_RPR") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "STR_TRNS_PRTNR_RPR") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "STR_FLTY_NTBP") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "STR_TBD") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "STR_PKUP") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "STR_PDC") {
      //_getColorByEvent('WIP');
      return "On Hold";
    } else if (status == "STR_DEL_ENR") {
      if (lastRemark == "Spare Delivered Faulty Picked up") {
        return "WIP";
      } else if (lastRemark == "Spare Delivered Faulty pending to pick") {
        return "WIP";
      } else if (lastRemark == "Standby Delivered Faulty Picked up") {
        return "Standby Provided";
      } else if (lastRemark == "Standby Delivered Faulty pending to pick") {
        return "Standby Provided";
      } else if (lastRemark == "Unit replacement Delivered Faulty Picked up") {
        return "WIP";
      } else if (lastRemark ==
          "Unit replacement Delivered Faulty pending to pick") {
        return "WIP";
      } else if (lastRemark == "Spare delivered after repair") {
        return "WIP";
      } else if (lastRemark == "Full unit delivered after repair") {
        return "WIP";
      }
      //_getColorByEvent('WIP');

    } else if (status == "STR_DEL_ER") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "STR_RESOLVED") {
      //_getColorByEvent('WIP');

      return "Resolved";
    } else if (status == "STR_HOVR_TRC_RPR") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "STR_HOVR_PTR_RPR") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "STR_REP") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "STR_UREP") {
      //_getColorByEvent('WIP');
      return "WIP";
    }

    if (status == "PART_NEW") {
      //_getColorByEvent('WIP');
      return 'WIP';
    }
    if (status == "PART_ACCEPT") {
      //_getColorByEvent('WIP');
      return 'WIP';
    }
    if (status == "PART_REJECT") {
      //_getColorByEvent('WIP');
      return 'WIP';
    } else if (status == "PART_WIP") {
      //_getColorByEvent('WIP');
      return 'WIP';
    } else if (status == "PART_ASGN_PRTNR") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "PART_ASGN_PENGNR") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "PART_SPR_REQ") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "PART_ASGN_ENGNR") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "PART_PDC") {
      //_getColorByEvent('WIP');
      return "On Hold";
    } else if (status == "PART_RESOLVED") {
      //_getColorByEvent('WIP'); // TICKET_CANCELLED
      //
      return "RESOLVED";
    } else if (status == "TICKET_CANCELLED") {
      //_getColorByEvent('WIP');
      return "Ticket Cancelled";
    } else if (status == "PART_WPS") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "PART_UOB") {
      //_getColorByEvent('WIP');
      return "Under Observation";
    } else if (status == "TRC_NEW") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "TRC_WIP") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "TRC_REPAIRED") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "TRC_NOT_REPAIRABLE") {
      //_getColorByEvent('WIP');
      return "WIP";
    }
  }


  //// To show selective list on filter button click

  buttonList(List<String> statuses) {
  //  print("SSSS => "+(filteredUsers ?? "Not Found"));
    filteredUsers.clear();
    Future.delayed(Duration.zero, () {
      if (filterText.isNotEmpty && filterText == "TODAYRESOLVED") {
        users.forEach((element) {
          // getting current date time
          String parsedResolvedDate = dateCreation(element.ticketResolvedDate, "");
          DateTime currentDateTime = DateTime.now();
          String cDateTime = DateFormat("dd-MM-yyyy HH:mm:ss a").format(currentDateTime);
          // splitting ticket datetime to compare with current one
          print("SSSS => "+parsedResolvedDate);
          List<String>  splitList = parsedResolvedDate.split(" ");
          List<String>   datesList = splitList[0].split("-");
          //splitting current date time
          List<String>  cSplitList = cDateTime.split(" ");
          List<String>   cDatesList = cSplitList[0].split("-");
          print("dateslist[0] => "+datesList[0]+" == "+cDatesList[0]);
          // here we are comparing that day,month and year are same for both dates

          if (datesList[0] == cDatesList[0] &&
              datesList[1] == cDatesList[1] &&
              datesList[2] == cDatesList[2]) {
            filteredUsers.add(element);
          }
        });
      } else if (statuses[0] == "ALL") {
        filteredUsers.addAll(users);
      } else {
        users.forEach((element) {
          if (statuses.contains(element.status)) {
            filteredUsers.add(element);
          }
        });
      }
      print("Resolved length => " + filteredUsers.length.toString());
      setState(() {});
    });
  }

  Future<bool> _onBackPressed() {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('Are you sure?'),
            content: Text('Do you want to exit from App!!'),
            actions: <Widget>[
              FlatButton(
                child: Text('YES'),
                onPressed: () {
                  Navigator.of(context).pop(true);
                },
              ),
              FlatButton(
                child: Text('NO'),
                onPressed: () {
                  Navigator.of(context).pop(false);
                },
              ),
            ],
          );
        });
  }

  void showFilterDialog() {
    filterText = "";
    showDialog(
      context: context,
      builder: (context) {
        return Dialog(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
          elevation: 16,
          child: Container(
            width: 300,
            height: 200,
            padding: EdgeInsets.fromLTRB(10, 20, 10, 10),
            alignment: Alignment.centerLeft,
            child: Column(
              children: [
                Align(
                    alignment: Alignment.center,
                    child: Text(
                      "Status",
                      style: TextStyle(
                          fontSize: 17.0,
                          fontWeight: FontWeight.bold,
                          color: Color(0xFF919191)),
                    )),
                new Container(
                  child: Column(
                    children: [
                      Container(
                        child: InkWell(
                          onTap: () {
                            print("ALL");

                            buttonList(["ALL"]);

                            Navigator.of(context).pop();
                          },
                          child: Text('All Tickets'),
                        ),
                        padding: EdgeInsets.only(top: 20.0),
                      ),
                      Container(
                        child: InkWell(
                          onTap: () {
                            print("ENG_RESO");

                            buttonList([
                              "PART_RESOLVED",
                              "STR_RESOLVED",
                              "RIM_RESOLVED",
                              "ENG_RESOLVED",
                              "ENG_RESO",
                              "PENG_RESOLVED",
                              "PENG_RESO",
                              "IM_RESOLVED",
                              "STR_SPR_DLVR-R",
                              "STR_STDBY_PICK",
                              "STR_REP_DLVRD_FP",
                              "STR_REPD_NTBP",
                            ]);

                            // you can add yor ownyes sir ok sir

                            Navigator.of(context).pop();
                          },
                          child: Text('Resolved'),
                        ),
                        padding: EdgeInsets.only(top: 20.0),
                      ),
                      Container(
                        child: InkWell(
                          onTap: () {
                            buttonList(["TICKET_CANCELLED"]);
                            Navigator.of(context).pop();
                          }, // Handle your callback
                          child: Text('Ticket Cancelled'),
                        ),
                        padding: EdgeInsets.only(top: 20.0),
                      ),
                    ],
                  ),

                  //   ],
                  // ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}

Color _colorFromHex(String hexColor) {
  final hexCode = hexColor.replaceAll('#', '');
  return Color(int.parse('FF$hexCode', radix: 16));
}

Future<bool> isConnected() async {
  var connectivityResult = await (Connectivity().checkConnectivity());
  if (connectivityResult == ConnectivityResult.mobile) {
    return true;
  } else if (connectivityResult == ConnectivityResult.wifi) {
    return true;
  }
  return false;
}

Color checkStatus(String st) {
  Color a;
  if (st == "OK") a = Colors.red;
}

// Widget loadingView() => Center(
//       child: CircularProgressIndicator(
//         backgroundColor: Colors.red,
//       ),
//     );
