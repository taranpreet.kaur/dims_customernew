import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:customer/Constant.dart';
import 'package:flutter/services.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'Dashboard.dart';
import 'form_model.dart';


class EditProfile extends StatefulWidget {
  EditProfile():super();

  @override
  EditProfileState createState() => EditProfileState();
}

class EditProfileState extends State<EditProfile>{
  var _controller = TextEditingController(text: 'name');
  var email_controller=TextEditingController(text:'emailId');
  var phone_controller=TextEditingController(text:'PhoneNumber');
  var companyName_controller=TextEditingController(text:'companyName');
  var contract=TextEditingController(text:'Contract');
  var userId;
  final _formKey = GlobalKey<FormState>();
  final model = FormModel();
   var isLoading = false;
@override
  void initState() {
  super.initState();

  prefs();
  }
  @override
  Widget build(BuildContext context) {
    Color color1 = _colorFromHex("#00ABC5");


    return FlutterEasyLoading(
        child:Scaffold(
       appBar: AppBar(title: Text("EditProfile"),
         flexibleSpace: Image(
           image: AssetImage('assets/appbar_background.png'),
           fit: BoxFit.cover,
         ),),
            body:
            Container(

                padding: EdgeInsets.all(10),
                child: ListView(
                  children: <Widget>[
                    Container(
                      alignment: Alignment.center,
                      padding: EdgeInsets.all(10),
                    ),
                    Container(
                      padding: EdgeInsets.all(10),
                      child: Text(
                        'Name',
                        textAlign: TextAlign.left,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(fontWeight: FontWeight.normal),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.all(10),
                      child: Form(
                        child: TextField(

                          controller: _controller,
                          enabled: false,

                          textAlign: TextAlign.left,

                          style: TextStyle(fontWeight: FontWeight.bold),
                          // readOnly: true,

                        ),

                      ),
                    ),

                    Container(
                      padding: EdgeInsets.all(10),
                      child: Text(

                        'Email',
                        textAlign: TextAlign.left,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(fontWeight: FontWeight.normal),

                      ),
                    ),

                    Container(
                      padding: EdgeInsets.all(10),


                      child: TextField(
                        enableInteractiveSelection: false,
                        enabled: false,
                        readOnly: true,
                        controller: email_controller,
                        textAlign: TextAlign.left,
                        style: TextStyle(fontWeight: FontWeight.bold),

                        // readOnly: true,
                      ),


                    ),
                    // company name controller
                    /*Container(
                      padding: EdgeInsets.all(10),
                      child: Text(
                        'Company Name',
                        textAlign: TextAlign.left,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(fontWeight: FontWeight.normal),
                      ),
                    ),
                    // company name controller
                    Container(
                      padding: EdgeInsets.all(10),
                      child:TextField(

                        controller: companyName_controller ?? "",

                        textAlign: TextAlign.left,

                        style: TextStyle(fontWeight: FontWeight.bold),
                        readOnly: true,

                      ),
                    ),*/


                    Container(
                      padding: EdgeInsets.all(10),

                      child: Text(

                        'Mobile Number',
                        textAlign: TextAlign.left,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(fontWeight: FontWeight.normal),

                      ),
                    ),

                    Container(
                      padding: EdgeInsets.all(10),
                      child: Form(
                        key: _formKey,

                        child: TextField(
                          controller: phone_controller,
                          maxLength: 10,
                          inputFormatters: [FilteringTextInputFormatter.allow(RegExp('[0-9]')),],
                          textAlign: TextAlign.left,
                          style: TextStyle(fontWeight: FontWeight.bold),

                          // readOnly: true,

                        ),


                      ),
                    ),

                    Container(
                      padding: EdgeInsets.all(10),
                      child: Text(
                        'Customer Type',
                        textAlign: TextAlign.left,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(fontWeight: FontWeight.normal),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.all(10),
                      child: TextField(

                        controller : contract,
                        textAlign: TextAlign.left,
                        // overflow: TextOverflow.ellipsis,
                        style: TextStyle(fontWeight: FontWeight.bold),
                        readOnly: true,
                      ),
                    ),
                    Container(
                        height: 60,
                        width: 250,
                        padding: EdgeInsets.fromLTRB(10, 10, 10, 0),
                        margin: EdgeInsets.fromLTRB(10, 10, 10, 0),
                        child: RaisedButton(
                          textColor: Colors.white,
                          color: color1,
                          child: Text('Save'),
                          onPressed: () {



                            setState(() {
                              isLoading=true;
                              CircularProgressIndicator();

                              if (phone_controller.text == '' )
                              {
                                // Put your code here which you want to execute when Text Field is Empty.
                                print('Please Fill the mobile number');
                                showDialog(
                                    context: context,
                                    builder: (context) => CupertinoAlertDialog(
                                      title: Text("Alert!"),
                                      actions: [
                                        FlatButton(
                                          onPressed: () {
                                            Navigator.pop(context,);
                                            },
                                          child: Text("Cancel"),
                                        ),
                                        FlatButton(
                                          onPressed: () {
                                            Navigator.pop(
                                              context,

                                            );
                                          },
                                          child: Text("Ok"),
                                        )
                                      ],
                                      content: Text("Mobile number is empty."),

                                    ));
                                //  EasyLoading.showError('Failed with Error');
                              }

                              else if ( phone_controller.text.length != 10)
                              {
                                print('Mobile number is empty');
                                showDialog(
                                    context: context,
                                    builder: (context) => CupertinoAlertDialog(
                                      title: Text("Alert!"),
                                      actions: [
                                        FlatButton(
                                          onPressed: () {

                                            Navigator.pop(
                                              context,

                                            );

                                          },
                                          child: Text("Cancel"),
                                        ),
                                        FlatButton(
                                          onPressed: () {
                                            Navigator.pop(
                                              context,

                                            );
                                          },
                                          child: Text("OK"),
                                        )
                                      ],
                                      content: Text("Mobile number should be 10 digit"),

                                    ));
                                // EasyLoading.showError('Failed with Error');

                              }


                              else if (_formKey.currentState.validate()) {
                                _formKey.currentState.save();
                                // isLoading=true;
                                // CircularProgressIndicator();

                                updateProfile(email_controller.text,_controller.text ,phone_controller.text ,
                                    companyName_controller.text,context);

                              }
                            });


                          },
                        )),

                  ],
                ))));
  }

  String validateMobile(String value) {
    String pattern = r'(^(?:[+0]9)?[0-9]{10,12}$)';
    RegExp regExp = new RegExp(pattern);
    if (value.length == 0) {
      return 'Please enter mobile number';
    }
    else if (!regExp.hasMatch(value)) {
      return 'Please enter valid mobile number';
    }
    return null;
  }


  Color _colorFromHex(String hexColor) {
    final hexCode = hexColor.replaceAll('#', '');
    return Color(int.parse('FF$hexCode', radix: 16));
  }
  Future<String> prefs()
  async {
    final prefs = await SharedPreferences.getInstance();
    var n=prefs.get("name_profile");
    var email=prefs.get("email_profile");
    var phone=prefs.get("number_profile");
    var companyname=prefs.get("companyname_profile");
    userId=prefs.get("userId");

    print("==profile name"+n +email +phone +companyname);

     _controller.text=n;
     email_controller.text=email;
     phone_controller.text=phone;
     companyName_controller.text = companyname;

  }
   Future<String> updateProfile(String email, name,phoneNumber,companyName,BuildContext context) async {
     EasyLoading.show(status: "Loading...");
    print("==email :"+userId);
    print("==email1 :"+email);
    print("==email2 :"+name);
    print("==email3 :"+phoneNumber);
     print("==Compnayname is  :"+companyName);
// setState(() {
//   isLoading =true;
//   CircularProgressIndicator();
// });
     var url=Constants.base_url+"api/cust/user/editProfile";

    var request =  http.MultipartRequest('POST',Uri.parse(url));
    print("request"+request.toString());
    request.fields['userId']=userId;
    request.fields['email']=email;
    request.fields['name']=name;
    request.fields['phoneNumber']=phoneNumber;
    request.fields['domain_name'] = companyName;
    var response=await request.send();
    SnackBar(content: Text('Processing Data'));

    if(response.statusCode == 200) {
      EasyLoading.dismiss(animation: false);
      var result = await http.Response.fromStream(response);

        var  jsonResponse = json.decode(result.body);
        print("==edit profile"+jsonResponse.toString());
          Navigator.push(context,MaterialPageRoute(builder: (context) => Dashboard()),);

    }
    else {
      EasyLoading.showError("Response Error");
      print("==response error"+response.toString());
    }
  }
}

