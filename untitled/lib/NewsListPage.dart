import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'Article.dart';

class NewsListPage extends StatefulWidget {
  List<Article> list1;
  List<String> dogsBreedList = List<String>();
  List<String> tempList = List<String>();
  bool isLoading = false;
  @override
  _NewsListPageState createState() => _NewsListPageState();
}

class _NewsListPageState extends State<NewsListPage> {
  var tempList = List<String>();
  Future<List<Article>> getData() async {
    List<Article> list;
    var jj;
    SharedPreferences sharedPreferences    = await SharedPreferences.getInstance();
      String token=  sharedPreferences.getString("token");
    String link = "http://10.11.4.59:8080/api/ticket/index";
    var sdr = Uri.parse(link);
    var res = await http
        .get(sdr, headers: {"token": token});
    var rest;
    print(res.body);
    if (res.statusCode == 200) {
      var data = json.decode(res.body);
      data['result'].forEach((assetType){
        tempList.add(assetType.toString().toUpperCase());
      });
      print(rest);


      list = rest.map<Article>((json) => Article.fromJson(json)).toList();
            // jj=jsonEncode(list);
           // print("==jjj"+jj);
    }
    SharedPreferences pref = await SharedPreferences.getInstance();
    print("==List Size: $list");
    return list;
  }

  Widget listViewWidget(List<Article> article) {
    return Container(
      child: ListView.builder(
          itemCount: article.length,
          padding: const EdgeInsets.all(2.0),
          itemBuilder: (context, position) {
            return Card(
              child: ListTile(
                title: Text(
                  '${article[position].assetType}',
                  style: TextStyle(
                      fontSize: 18.0,
                      color: Colors.black,
                      fontWeight: FontWeight.normal),
                ),
                subtitle: Text('${article[position].assetId}',style: TextStyle(fontSize: 16.0,color:Colors.black,fontWeight:FontWeight.normal),),
                // leading: Padding(
                //   padding: const EdgeInsets.all(8.0),
                //   child: SizedBox(
                //     child: article[position].urlToImage == null
                //         ? Image(
                //       image: AssetImage('images/no_image_available.png'),
                //     )
                //         : Image.network('${article[position].urlToImage}'),
                //     height: 100.0,
                //     width: 100.0,
                //   ),
                // ),
                onTap: () => _onTapItem(context, article[position]),
              ),
            );
          }),
    );
  }

  void _onTapItem(BuildContext context, Article article) {
    // Navigator.of(context).push(MaterialPageRoute(
    //     builder: (BuildContext context) => NewsDetails(article, widget.title)));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(actions: <Widget>[IconButton(onPressed:() {

        showSearch(context: context, delegate: Search());
      }, icon: Icon(Icons.search),)],centerTitle: true,title: Text('Search Bar'),),

      body: FutureBuilder(
          future: getData(),
          builder: (context, snapshot) {
            return snapshot.data != null
                ? listViewWidget(snapshot.data)
                : Center(child: CircularProgressIndicator());
          }),
    );
  }
}

class Search extends SearchDelegate{
  List<Article> article;

  @override
  List<Widget> buildActions(BuildContext context) {
    // TODO: implement buildActions
    throw UnimplementedError();
  }
  @override
  Widget buildLeading(BuildContext context) {
    // TODO: implement buildLeading
    throw UnimplementedError();
  }
  @override
  Widget buildResults(BuildContext context) {
    // TODO: implement buildResults
    throw UnimplementedError();
  }
  @override
  Widget buildSuggestions(BuildContext context) {
  }



}


