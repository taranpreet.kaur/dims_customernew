import 'dart:convert';
import 'dart:developer';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:customer/Article.dart';
import 'package:customer/Constant.dart';
import 'package:customer/SearchListViewExample.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:http/http.dart' as http;
import 'package:loading/indicator/ball_pulse_indicator.dart';
import 'package:loading/loading.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Services {


  // static const String url = 'http://10.11.4.59:8080/api/cust/user/geTickets';
  static Future<List<Article>> getUsers() async {


    var url=Constants.base_url+"api/cust/user/geTickets";
    try {
      SharedPreferences sharedPreferences    = await SharedPreferences.getInstance();
      // final response = await http.get(url);
      var request =  http.MultipartRequest('POST',Uri.parse(url));
//     print("request"+request.toString());
      request.fields['customerID']=sharedPreferences.getString("userId");

      var response=await request.send();

      if (response.statusCode == 200) {

        var result1 = await http.Response.fromStream(response);
       final jsonResponse = jsonDecode(result1.body);
       print("==result"+jsonResponse.toString());


       var result=jsonResponse['result'] as List;

        List<Article> list = parseUsers(result);
        var distinctIds = list.toSet().toList();
        return distinctIds;

      }
      else {
        EasyLoading.showError("Error");
        throw Exception("Error");
      }
    } catch (e) {
      throw Exception(e.toString());
    }
  }

  static List<Article> parseUsers(List result) {
    // final parsed = json.decode(responseBody).cast<Map<String, dynamic>>();
    return result.map<Article>((json) => Article.fromJson(json)).toList();
  }


  Future<bool> isConnected() async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile) {
      return true;
    } else if (connectivityResult == ConnectivityResult.wifi) {
      return true;
    }
    return false;
  }

   static Widget loadingView() => Center(
    child: CircularProgressIndicator(
      backgroundColor: Colors.red,
    ),
  );

  static Future<List<dynamic>> getTicketHistoryDetails({assetsId}) async {
    var url=Constants.base_url+"api/ticket/ticketHistory";
  ////  var url="http://35.200.255.51:8080/api/ticket/ticketHistory";
    print("url: ${url}");
    try {
      var request =  http.MultipartRequest('POST',Uri.parse(url));
      request.fields['assetId']=assetsId.toString().toLowerCase() ?? "siu-77149";

      var response=await request.send();
      if (response.statusCode == 200) {
        var result1 = await http.Response.fromStream(response);

        print("Ticket Details Response ${result1.statusCode}"+ result1.body);
        final jsonResponse = jsonDecode(result1.body);
        List<dynamic> result=jsonResponse['data'] as List;
        return result;
      } else {
        EasyLoading.showError("Error");
        throw Exception("Error");
      }
    } catch (e) {
      throw Exception(e.toString());
    }
  }

  static Future<List<dynamic>> getTicketHistorylist({assetsId, createdAt,assetSerial,serviceItemNo}) async {
    var url=Constants.base_url+"api/ticket/ticketHistory";

    print("url: ${url}");
    try {
      var request =  http.MultipartRequest('POST',Uri.parse(url));
      if(assetsId != null)
      request.fields['assetId']=assetsId.toString();
      request.fields['createdAt'] = createdAt;
      request.fields['assetSerial'] = /*assetSerial*/" ";
      request.fields['serviceItemNo'] = serviceItemNo.toString();
      print("Params =. "+jsonEncode(request.fields));

      var response=await request.send();
      print("=Status code "+response.statusCode.toString());
      // var result2 = await http.Response.fromStream(response);
      // print("Response =>  "+result2.body);
      if (response.statusCode == 200) {
        var result1 = await http.Response.fromStream(response);
        print(" Ticket new Details Response${result1.statusCode}"+ result1.body);
        final jsonResponse = jsonDecode(result1.body);
        List<dynamic> result=jsonResponse['result'] as List;
        return result;
      } else {
        EasyLoading.showError("Error");
        throw Exception("Error");
      }
    } catch (e) {
      throw Exception(e.toString());
    }
  }


}
