
class Article {
  String assetType;
  String assetId;
  String id;
  String status;
  String ticketSerialNo;
  String ticketDate;
  String ticketResolvedDate;
  String ticketCancelDate;
  String lastRemark;
  String lastStatus;
  bool isNewTicket;
  String isPartRequested;
  String isStandByInstall;


  Article({
    this.assetType,
    this.assetId,
    this.id,
    this.status,
    this.ticketSerialNo,
    this.ticketDate,
    this.ticketResolvedDate,
    this.ticketCancelDate,
    this.lastRemark,
    this.lastStatus,
    this.isNewTicket,
    this.isPartRequested,
    this.isStandByInstall

  });

  factory Article.fromJson(Map<String, dynamic> json) {
    return Article(
      assetType: json["assetType"] as String,
      assetId: json["assetId"] as String,
      id: json["_id"] as String,
      status: json["status"] as String,
      ticketSerialNo: json["ticketSerialNo"] as String,
      ticketDate: json["ticketDate"] as String,
      ticketResolvedDate: json["ticketResolvedDate"] as String,
      ticketCancelDate: json["ticketCancelDate"] as String,
      lastRemark: json["lastRemark"] as String,
      lastStatus: json["lastStatus"] as String,
     // isNewTicket = json['isNewTicket'],
        isNewTicket: json["isNewTicket"] as bool,
      isPartRequested: json["isPartRequested"].toString() as String,
      isStandByInstall: json["isStandByInstall"].toString() as String,

    );
  }

  static Map<String, dynamic> toMap(Article music) =>
      {
        'assetType': music.assetType,
        'assetId': music.assetId,
        '_id': music.id,
        'status': music.status,
        'ticketSerialNo':music.ticketSerialNo,
        'ticketDate':music.ticketDate,
        'ticketResolvedDate':music.ticketResolvedDate,
        'ticketCancelDate' :music.ticketCancelDate,
        'lastRemark':music.lastRemark,
        'lastStatus':music.lastStatus,
        'isNewTicket':music.isNewTicket,
        'isPartRequested':music.isPartRequested,
        'isStandByInstall':music.isStandByInstall,

      };
}
