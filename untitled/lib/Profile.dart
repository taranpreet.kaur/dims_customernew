import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:customer/Constant.dart';
import 'package:customer/LoginTab.dart';
import 'package:customer/main.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'EditProfile.dart';
import 'dart:async';
import 'package:intl/intl.dart';

class Profile extends StatefulWidget {
  Profile() : super();
  @override
  ProfileState createState() => ProfileState();
}

class ProfileState extends State<Profile> {
  var _controller = TextEditingController(text: 'name');
  var email_controller=TextEditingController(text:'emailId');
  var companyName_controller=TextEditingController(text:'companyName');
  var phone_controller=TextEditingController(text:'phoneNumber');
  var userId;
  var isLoading =false;


  @override
  void initState() {
    super.initState();
    setState(() {
      isLoading =true;
      CircularProgressIndicator();
      prefs();
    });

  }

  // Future<bool> _onBackPressed() {
  //   return showDialog(
  //       context: context,
  //       builder: (BuildContext context) {
  //         return AlertDialog(
  //           title: Text('Are you sure?'),
  //           content: Text('Do you want to exit from App!!'),
  //           actions: <Widget>[
  //
  //             FlatButton(
  //               child: Text('YES'),
  //               onPressed: () {
  //                 Navigator.of(context).pop(true);
  //               },
  //             ),
  //             FlatButton(
  //               child: Text('NO'),
  //               onPressed: () {
  //                 Navigator.of(context).pop(false);
  //               },
  //             ),
  //           ],
  //         );
  //       });
  // }

  @override
  build(BuildContext context) {
    Color color1 = _colorFromHex("#00ABC5");
    return FlutterEasyLoading(child: Scaffold(
          appBar: AppBar(
            title: const Text('Profile'),
            flexibleSpace: Image(
              image: AssetImage('assets/appbar_background.png'),
              fit: BoxFit.cover,
            ),

            actions: <Widget>[
              IconButton(
                icon: Icon(
                  Icons.edit,
                  color: Colors.white,
                ),
                onPressed: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context) => EditProfile()),);

                },
              )
            ],
          ),
          body:

    isLoading
    ? Center(
    child: CircularProgressIndicator(),
    )
        :
    Center(

        child: Padding(

            padding: EdgeInsets.all(10),
            child: ListView(
              children: <Widget>[
                Container(
                  alignment: Alignment.center,
                  padding: EdgeInsets.all(10),
                ),
                Container(

                  padding: EdgeInsets.all(10),
                  child: Text(
                    'Name',
                    textAlign: TextAlign.left,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(fontWeight: FontWeight.normal),
                  ),
                ),
                Container(

                  padding: EdgeInsets.all(10),
                  child: TextField(

                    controller: _controller,
                    enabled: false,
                    textAlign: TextAlign.left,

                    style: TextStyle(fontWeight: FontWeight.bold),
                    readOnly: true,

                  ),

                ),

                Container(
                  padding: EdgeInsets.all(10),
                  child: Text(
                    'Email',
                    textAlign: TextAlign.left,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(fontWeight: FontWeight.normal),
                  ),
                ),
                Container(
                  padding: EdgeInsets.all(10),
                  child:TextField(

                    controller: email_controller,

                    textAlign: TextAlign.left,

                    style: TextStyle(fontWeight: FontWeight.bold),
                    readOnly: true,

                  ),
                ),
                // company name
                /*Container(
                  padding: EdgeInsets.all(10),
                  child: Text(
                    'Company Name',
                    textAlign: TextAlign.left,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(fontWeight: FontWeight.normal),
                  ),
                ),
                // company name
                Container(
                  padding: EdgeInsets.all(10),
                  child:TextField(

                    controller: companyName_controller ?? "",

                    textAlign: TextAlign.left,

                    style: TextStyle(fontWeight: FontWeight.bold),
                    readOnly: true,

                  ),
                ),*/


                Container(
                  padding: EdgeInsets.all(10),
                  child: Text(

                    'Mobile Number',
                    textAlign: TextAlign.left,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(fontWeight: FontWeight.normal),

                  ),
                ),
                Container(
                  padding: EdgeInsets.all(10),
                  child: TextField(

                    controller:phone_controller,

                    textAlign: TextAlign.left,

                    style: TextStyle(fontWeight: FontWeight.bold),
                    readOnly: true,

                  ),
                ),

                Container(
                  padding: EdgeInsets.all(10),
                  child: Text(
                    'Customer Type',
                    textAlign: TextAlign.left,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(fontWeight: FontWeight.normal),
                  ),
                ),
                Container(
                  padding: EdgeInsets.all(10),
                  child: Text(
                    'Contractual',
                    textAlign: TextAlign.left,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                ),
                Container(
                    height: 50,
                    padding: EdgeInsets.fromLTRB(10, 10, 10, 0),
                    margin: EdgeInsets.fromLTRB(10, 10, 10, 0),
                    child: RaisedButton(
                      textColor: Colors.white,
                      color: color1,
                      child: Text('Logout'),
                      onPressed: () async {
                        final pref = await SharedPreferences.getInstance();
                        await pref.clear();
                        // Navigator.of(context).pushReplacementNamed('/HomePage');

                        Navigator.pushAndRemoveUntil<dynamic>(
                          context,
                          MaterialPageRoute<dynamic>(
                            builder: (BuildContext context) => LoginTab(),
                          ),
                              (route) => false,//if you want to disable back feature set to false
                        );


                        // Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (BuildContext context) => SplashScreen()));                    // signUp(name.text,email.text, password.text,phoneNumber.text);
                      },
                    )),
              ],
            ))),),);

  }


  Future<String> prefs()
  async {
    final prefs = await SharedPreferences.getInstance();

    userId=prefs.get('userId');

    print("==profile name"+userId);

    getProfile(userId); // function where you call your api


  }
  SharedPreferences sharedPreferences ;

  Future<String> getProfile(String userId)  async {

    print("==email1 :"+userId);
    var url=Constants.base_url+"api/cust/user/profileData";

    var request =  http.MultipartRequest('POST',Uri.parse(url));
    print("request"+request.toString());
    request.fields['userId']=userId;
    var response=await request.send();
    // EasyLoading.show(status:"loading....");
    //  SnackBar(content: Text('Processing Data'));
    if(response.statusCode == 200) {
       setState(() {
         isLoading =false;
       });
        var result = await http.Response.fromStream(response);
        var  jsonResponse = jsonDecode(result.body);
        var user=jsonResponse['user'];
        var email=user['email'];
        var id=user['_id'];
        var name=user['name'];
       var sentence = toBeginningOfSentenceCase(name); // This is a string

       var phoneNumber=user['phoneNumber'];
       var companyName=user['domain_name'];
        _controller.text=sentence;
        email_controller.text=email;
        phone_controller.text=phoneNumber.toString();

        print("==edit profile"+user.toString());
        sharedPreferences= await SharedPreferences.getInstance();
        sharedPreferences.setString("email_profile", email);
       companyName = sharedPreferences.getString('domain_name');
       sharedPreferences.setString("companyname_profile", companyName);
       companyName_controller.text = companyName;
     //  sharedPreferences.getString(companyName);
       print("==companyprofile"+companyName.toString());
       print("==emailprofile"+email.toString());
        sharedPreferences.setString("name_profile", sentence);
        sharedPreferences.setString("number_profile", phoneNumber.toString());
      // sharedPreferences.getString("companyName_profile", companyName);



      // EasyLoading.dismiss(animation: false);

    }
    else {
      print("==response error"+response.toString());
    }
  }




}



  //   Future<bool> _onWillPop() {
  //   return showDialog(
  //     context: context,
  //     builder: (context) => AlertDialog(
  //       title: Text('Are you sure?'),
  //       content: Text('Do you want to exit an App'),
  //       actions: <Widget>[
  //         FlatButton(
  //           onPressed: () => Navigator.of(context).pop(false),
  //           child: Text('No'),
  //         ),
  //         FlatButton(
  //           onPressed: () => exit(0),
  //           /*Navigator.of(context).pop(true)*/
  //           child: Text('Yes'),
  //         ),
  //       ],
  //     ),
  //   ) ??
  //       false;
  // }

  // BuildContext context;
    Color _colorFromHex(String hexColor) {
    final hexCode = hexColor.replaceAll('#', '');
    return Color(int.parse('FF$hexCode', radix: 16));
  }



