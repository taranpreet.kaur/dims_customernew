import 'package:customer/LoginTab.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'SliderModel.dart';

class WelcomeScreen extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    Color color1 = _colorFromHex("#00ABC5");

    return Scaffold(
      resizeToAvoidBottomInset: true,

      body: Home(),
    );
  }
  Color _colorFromHex(String hexColor) {
    final hexCode = hexColor.replaceAll('#', '');
    return Color(int.parse('FF$hexCode', radix: 16));
  }
}

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {


  List<SliderModel> mySLides = new List<SliderModel>();
  int slideIndex = 0;
  PageController controller;


  Widget _buildPageIndicator(bool isCurrentPage){
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 2.0),
      height: isCurrentPage ? 10.0 : 10.0,
      width: isCurrentPage ? 10.0 : 10.0,
      decoration: BoxDecoration(
        color: isCurrentPage ? Color(0xFF54BC86) : Colors.grey[300],
        borderRadius: BorderRadius.circular(20),
      ),
    );
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    mySLides = getSlides();
    controller = new PageController();
  }

  @override
  Widget build(BuildContext context) {

    return Container(
      color: Color(0xFFF8FCFF),
      child: Scaffold(
          body: Container(
            color: Color(0xFFF8FCFF),
            alignment: Alignment.center,

            child: PageView(
              pageSnapping: true,

              controller: controller,
              onPageChanged: (index) {
                setState(() {
                  slideIndex = index;
                });
              },
              children: <Widget>[

                SlideTile(

                  imagePath: mySLides[0].getImageAssetPath(),
                  title: mySLides[0].getTitle(),
                   desc: mySLides[0].getDesc(),
                ),
                SlideTile(

                  imagePath: mySLides[1].getImageAssetPath(),
                  title: mySLides[1].getTitle(),
                  desc: mySLides[1].getDesc(),
                ),
                SlideTile(
                  imagePath: mySLides[2].getImageAssetPath(),
                  title: mySLides[2].getTitle(),
                   desc: mySLides[2].getDesc(),
                )
              ],
            ),
          ),
          bottomSheet: Container(
            color: Color(0xFFF8FCFF),
            height: 170,
            padding: EdgeInsets.symmetric(vertical: 10),
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    SizedBox(
                      width: 100,
                    ),
                    Container(
                      color: Color(0xFFF8FCFF),
                      child: Row(
                        children: [
                          for (int i = 0; i < 3 ; i++) i == slideIndex ? _buildPageIndicator(true): _buildPageIndicator(false),
                        ],),
                    ),
                    SizedBox(
                      width: 100,
                    ),
                  ],
                ),
                Container(
                  color: Color(0xFFF8FCFF),
                  margin: EdgeInsets.symmetric(vertical: 16),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      _showNeedHelpButton(context,"BACK",Color(0xFFD2D4D8),controller,slideIndex,true),
                      _showNeedHelpButton(context,slideIndex<2?"NEXT":"LET’S START",Color(0xFF00ABC5),controller,slideIndex,false),
                    ],
                  ),
                )
              ],

            ),
          )
      ),
    );
  }
}

Widget _showNeedHelpButton(BuildContext context,String text, Color color,PageController controller,int slideIndex ,bool isBack) {

  return Padding(

    padding: EdgeInsets.all(16),
    child: Material(  //Wrap with Material

      shape: RoundedRectangleBorder(borderRadius:BorderRadius.circular(10.0) ),
      elevation: 18.0,
      color: color,
      clipBehavior: Clip.antiAlias,
      // Add This
      child: MaterialButton(
        minWidth: 150.0,
        height: 55,
        color: color,
        child: new Text(text,
            style: new TextStyle(fontSize: 16.0, color: Colors.white)),
            onPressed: () {
          // mainlogin:var context= MainloginScreen();
          // '/XDdetectingproblems14' :(context) => XDdetectingproblems14(),
          if(isBack){
            if(slideIndex>0){
              controller.animateToPage(slideIndex - 1, duration: Duration(milliseconds: 400), curve: Curves.linear);
            }
          }
          else{
            if(slideIndex==2){
              //var context;
              Navigator.pushAndRemoveUntil<dynamic>(
                context,
                MaterialPageRoute<dynamic>(
                  builder: (BuildContext context) => LoginTab(),
                ),
                    (route) => false,//if you want to disable back feature set to false
              );
              print("Tapping");
            }
            else{
              controller.animateToPage(slideIndex + 1, duration: Duration(milliseconds: 400), curve: Curves.linear);
            }
          }
        },
      ),
    ),
  );
}

class SlideTile extends StatelessWidget {
  final String imagePath, title, desc;
  SlideTile({this.imagePath, this.title, this.desc});
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Color(0xFFF8FCFF),
      padding: EdgeInsets.symmetric(horizontal: 20),
      alignment: Alignment.center,
      child: Column(
        children: <Widget>[
          SizedBox(
            height: 35,
          ),
          Align(
            alignment: Alignment.topRight,
            child: FlatButton(child: new Text(
              'Skip',
            ),
                onPressed: () {
                  Navigator.pushAndRemoveUntil<dynamic>(
                    context,
                    MaterialPageRoute<dynamic>(
                      builder: (BuildContext context) => LoginTab(),
                    ),
                        (route) => false,//if you want to disable back feature set to false
                  );
                  print('Clicked');
                }),
          ),
          SizedBox(
              width: 200,
              height: 200,
              child: Image.asset("assets/image/tc-logonew.png")),
          SizedBox(
            height: 50,
          ),
          SizedBox(
              width: 200,
              height: 200,
              child: Image.asset(imagePath)),
          SizedBox(
            height: 40,
          ),
          Text(title, textAlign: TextAlign.center,style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 36
          ),),
          SizedBox(
            height: 20,
          ),
          Text(desc, textAlign: TextAlign.center,style: TextStyle(
              fontWeight: FontWeight.w500,
              fontSize: 14),
          ),
        ],
      ),
    );
  }
}


