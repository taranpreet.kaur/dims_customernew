import 'package:flutter/material.dart';
import 'package:customer/JsonSpinner.dart';

import 'SecondScreen.dart';
import 'myform.dart';

class LoginTab extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
  Color color1 = _colorFromHex("#00ABC5");
  return MaterialApp(

    //     home: DefaultTabController(
    //         length: 2,
    //         child:Scaffold(appBar: PreferredSize(
    //     preferredSize: Size.fromHeight(100.0),
    //     child:AppBar(
    //
    //       title: Image.asset('assets/tclogo.png',height: 100, width: 130,), centerTitle: true,
    //       flexibleSpace: Image(
    //         image: AssetImage('assets/appbar_background.png'),
    //         fit: BoxFit.cover,
    //       ),
    //       backgroundColor: Colors.transparent,
    //
    //       bottom: TabBar(
    //         indicatorColor: Color(0xFFE4CE37),
    //
    //         tabs: [
    //           Tab(text: "SIGN UP",),
    //           Tab(text: "SIGN IN",),
    //           TabBarView(children: [MyForm(),SecondScreen()],)
    //         ],
    //       ),)),
    // ),),);
      home: DefaultTabController(length: 2,child: Scaffold(
        appBar: AppBar(title: Text(""),centerTitle: true,

        flexibleSpace: Image(
          image: AssetImage('assets/appbar_background.png'),
          fit: BoxFit.cover,
        ),
        backgroundColor: Colors.transparent,
       bottom:TabBar(indicatorColor:Colors.white,tabs: [Tab(text: 'SignIn'),Tab(text: 'SignUp',)],),),
      body:
        TabBarView(children: [MyForm(),SecondScreen()],),))
    );
  }

  Color _colorFromHex(String hexColor) {
    final hexCode = hexColor.replaceAll('#', '');
    return Color(int.parse('FF$hexCode', radix: 16));
  }
}