import 'dart:convert';
import 'dart:developer';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:customer/Article.dart';
import 'package:customer/Constant.dart';
import 'package:customer/SearchListViewExample.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:http/http.dart' as http;
import 'package:loading/indicator/ball_pulse_indicator.dart';
import 'package:loading/loading.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Services1 {

  // static const String url = 'http://10.11.4.59:8080/api/cust/user/geTickets';
  static Future<List<Article>> getUsers1() async {

    SharedPreferences sharedPreferences    = await SharedPreferences.getInstance();
    var id=sharedPreferences.getString("userId");

    var url=Constants.base_url+"api/cust/customerResolvedTicket/"+id;
    try {
      SharedPreferences sharedPreferences= await SharedPreferences.getInstance();
      // final response = await http.get(url);
      var request =  http.MultipartRequest('GET',Uri.parse(url));
      var response=await request.send();
      if (response.statusCode == 200) {
        var result1 = await http.Response.fromStream(response);
        final jsonResponse = jsonDecode(result1.body);

        print("Result1555");
     //   print("Result1 "+jsonResponse['data'][0]['ticketResolvedDate']);

        if(jsonResponse['data'] != null) {
          var result=jsonResponse['data'] as List;
          List<Article> list = parseUsers(result);
          list.forEach((element) {
            if(element.status.toLowerCase() == "ticket cancelled"){
              print(" ${element.ticketSerialNo} == "+element.ticketCancelDate.toLowerCase());
            }
          });
          var distinctIds = list.toSet().toList();
          return distinctIds;
        }

        return [];



      } else {
        EasyLoading.showError("Error");
        throw Exception("Error");
      }
    } catch (e) {
      EasyLoading.showError("Error");
      throw Exception(e.toString());

    }
  }

  static List<Article> parseUsers(List result) {
    // final parsed = json.decode(responseBody).cast<Map<String, dynamic>>();
    return result.map<Article>((json) => Article.fromJson(json)).toList();
  }


  Future<bool> isConnected() async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile) {
      return true;
    } else if (connectivityResult == ConnectivityResult.wifi) {
      return true;
    }
    return false;
  }

  static Widget loadingView() => Center(
    child: CircularProgressIndicator(
      backgroundColor: Colors.red,
    ),
  );


}
