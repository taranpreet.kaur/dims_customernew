// class EarlierCallHistory {
//   bool? success;
//   String message;
//   List<Data>? data;
//
//   EarlierCallHistory({this.success, this.message, this.data});
//
//   EarlierCallHistory.fromJson(Map<String, dynamic> json) {
//     success = json['success'];
//     message = json['message'];
//     if (json['data'] != null) {
//       data = <Data>[];
//       json['data'].forEach((v) { data!.add(new Data.fromJson(v)); });
//     }
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data['success'] = this.success;
//     data['message'] = this.message;
//     if (this.data != null) {
//       data['data'] = this.data!.map((v) => v.toJson()).toList();
//     }
//     return data;
//   }
// }
//
// class Data {
//   Location? location;
//   Address? address;
//   bool? erpUpdate;
//   List<Null> lastEngineer;
//   List<String> deptHistory;
//   List<StatusHistory> statusHistory;
//   List<String>? ticketattachments;
//   List<Null>? auditTrail;
//   List<String>? partReq;
//   int? slaPercentage;
//   int? slaResponsePercent;
//   int? slaResponseGiven;
//   int? onHold;
//   bool? reopenStatus;
//   int? reOpenCount;
//   String isPdcStCheck;
//   String isDTC;
//   List<Null>? reopenAttachments;
//   List<Null>? cancelledAttachments;
//   List<Null>? csrAttachments;
//   String sId;
//   int? slaRemaningSeconds;
//   double? slaResponseRemaningSeconds;
//   List<Null>? comments;
//   List<AcceptInfo>? acceptInfo;
//   String userName;
//   String assetId;
//   int? storeMpsUser;
//   String serviceItemNo;
//   String assetType;
//   String callMode;
//   String callType;
//   String source;
//   String customer;
//   String custName;
//   String contactPerson;
//   String assestWarranty;
//   String serviceType;
//   String severity;
//   String accountManager;
//   String contactno;
//   String contractNo;
//   String scheduledTime;
//   String scheduledDate;
//   String issue;
//   String issueCategory;
//   String issueSubCategory;
//   String typeOfRequest;
//   String assetMake;
//   String assetModel;
//   String assetSerial;
//   String contractType;
//   String contractEnddate;
//   String contractExpired;
//   String contractStartdate;
//   bool? isCurrentOwner;
//   String sun;
//   String mon;
//   String tue;
//   String wed;
//   String thu;
//   String fri;
//   String sat;
//   String serviceCount;
//   String custCode;
//   String postalcode;
//   int? responsetimeHours;
//   int? resolutiontimeHours;
//   int? secNdslaresolutionHours;
//   String swStarttime;
//   String swEndtime;
//   int? phoneNo;
//   String priority;
//   String lastStatus;
//   String oemId;
//   String assginStatus;
//   String department;
//   String createdBy;
//   int? isPartRequested;
//   int? holdDuration;
//   bool? isNewTicket;
//   String ticketDate;
//   String lastRemark;
//   String lastRemarkDate;
//   String status;
//   String ticketCategory;
//   String clusterId;
//   String ticketSerialNo;
//   String createdAt;
//   String updatedAt;
//   int? iV;
//   String adminId;
//   String contractCalllimit;
//   String contractlineno;
//   Null? phoneNoext;
//   bool? isVIP;
//   String currentOwner;
//   String assignedAdmin;
//   String dateTm;
//   Null? holdStartTime;
//   String isAccepted;
//   String ticketResolvedDate;
//   String engAssigndate;
//   String engineer;
//   String pendingComment;
//
//   Data({this.location, this.address, this.erpUpdate, this.lastEngineer, this.deptHistory, this.statusHistory, this.ticketattachments, this.auditTrail, this.partReq, this.slaPercentage, this.slaResponsePercent, this.slaResponseGiven, this.onHold, this.reopenStatus, this.reOpenCount, this.isPdcStCheck, this.isDTC, this.reopenAttachments, this.cancelledAttachments, this.csrAttachments, this.sId, this.slaRemaningSeconds, this.slaResponseRemaningSeconds, this.comments, this.acceptInfo, this.userName, this.assetId, this.storeMpsUser, this.serviceItemNo, this.assetType, this.callMode, this.callType, this.source, this.customer, this.custName, this.contactPerson, this.assestWarranty, this.serviceType, this.severity, this.accountManager, this.contactno, this.contractNo, this.scheduledTime, this.scheduledDate, this.issue, this.issueCategory, this.issueSubCategory, this.typeOfRequest, this.assetMake, this.assetModel, this.assetSerial, this.contractType, this.contractEnddate, this.contractExpired, this.contractStartdate, this.isCurrentOwner, this.sun, this.mon, this.tue, this.wed, this.thu, this.fri, this.sat, this.serviceCount, this.custCode, this.postalcode, this.responsetimeHours, this.resolutiontimeHours, this.secNdslaresolutionHours, this.swStarttime, this.swEndtime, this.phoneNo, this.priority, this.lastStatus, this.oemId, this.assginStatus, this.department, this.createdBy, this.isPartRequested, this.holdDuration, this.isNewTicket, this.ticketDate, this.lastRemark, this.lastRemarkDate, this.status, this.ticketCategory, this.clusterId, this.ticketSerialNo, this.createdAt, this.updatedAt, this.iV, this.adminId, this.contractCalllimit, this.contractlineno, this.phoneNoext, this.isVIP, this.currentOwner, this.assignedAdmin, this.dateTm, this.holdStartTime, this.isAccepted, this.ticketResolvedDate, this.engAssigndate, this.engineer, this.pendingComment});
//
//   Data.fromJson(Map<String, dynamic> json) {
//     location = json['location'] != null ? new Location.fromJson(json['location']) : null;
//     address = json['address'] != null ? new Address.fromJson(json['address']) : null;
//     erpUpdate = json['erpUpdate'];
//     if (json['lastEngineer'] != null) {
//       lastEngineer = <Null>[];
//       json['lastEngineer'].forEach((v) { lastEngineer!.add(new Null.fromJson(v)); });
//     }
//     deptHistory = json['dept_history'].cast<String>();
//     if (json['statusHistory'] != null) {
//       statusHistory = <StatusHistory>[];
//       json['statusHistory'].forEach((v) { statusHistory!.add(new StatusHistory.fromJson(v)); });
//     }
//     ticketattachments = json['ticketattachments'].cast<String>();
//     if (json['auditTrail'] != null) {
//       auditTrail = <Null>[];
//       json['auditTrail'].forEach((v) { auditTrail!.add(new Null.fromJson(v)); });
//     }
//     partReq = json['partReq'].cast<String>();
//     slaPercentage = json['sla_percentage'];
//     slaResponsePercent = json['sla_response_percent'];
//     slaResponseGiven = json['sla_response_given'];
//     onHold = json['on_hold'];
//     reopenStatus = json['reopenStatus'];
//     reOpenCount = json['reOpenCount'];
//     isPdcStCheck = json['isPdcStCheck'];
//     isDTC = json['isDTC'];
//     if (json['reopenAttachments'] != null) {
//       reopenAttachments = <Null>[];
//       json['reopenAttachments'].forEach((v) { reopenAttachments!.add(new Null.fromJson(v)); });
//     }
//     if (json['cancelledAttachments'] != null) {
//       cancelledAttachments = <Null>[];
//       json['cancelledAttachments'].forEach((v) { cancelledAttachments!.add(new Null.fromJson(v)); });
//     }
//     if (json['csrAttachments'] != null) {
//       csrAttachments = <Null>[];
//       json['csrAttachments'].forEach((v) { csrAttachments!.add(new Null.fromJson(v)); });
//     }
//     sId = json['_id'];
//     slaRemaningSeconds = json['sla_remaning_seconds'];
//     slaResponseRemaningSeconds = json['sla_response_remaning_seconds'];
//     if (json['comments'] != null) {
//       comments = <Null>[];
//       json['comments'].forEach((v) { comments!.add(new Null.fromJson(v)); });
//     }
//     if (json['acceptInfo'] != null) {
//       acceptInfo = <AcceptInfo>[];
//       json['acceptInfo'].forEach((v) { acceptInfo!.add(new AcceptInfo.fromJson(v)); });
//     }
//     userName = json['userName'];
//     assetId = json['assetId'];
//     storeMpsUser = json['storeMpsUser'];
//     serviceItemNo = json['serviceItemNo'];
//     assetType = json['assetType'];
//     callMode = json['callMode'];
//     callType = json['callType'];
//     source = json['source'];
//     customer = json['customer'];
//     custName = json['custName'];
//     contactPerson = json['contactPerson'];
//     assestWarranty = json['assestWarranty'];
//     serviceType = json['serviceType'];
//     severity = json['severity'];
//     accountManager = json['accountManager'];
//     contactno = json['contactno'];
//     contractNo = json['contractNo'];
//     scheduledTime = json['scheduledTime'];
//     scheduledDate = json['scheduledDate'];
//     issue = json['issue'];
//     issueCategory = json['issueCategory'];
//     issueSubCategory = json['issueSubCategory'];
//     typeOfRequest = json['typeOfRequest'];
//     assetMake = json['assetMake'];
//     assetModel = json['assetModel'];
//     assetSerial = json['assetSerial'];
//     contractType = json['contractType'];
//     contractEnddate = json['contractEnddate'];
//     contractExpired = json['contractExpired'];
//     contractStartdate = json['contractStartdate'];
//     isCurrentOwner = json['isCurrentOwner'];
//     sun = json['sun'];
//     mon = json['mon'];
//     tue = json['tue'];
//     wed = json['wed'];
//     thu = json['thu'];
//     fri = json['fri'];
//     sat = json['sat'];
//     serviceCount = json['serviceCount'];
//     custCode = json['custCode'];
//     postalcode = json['postalcode'];
//     responsetimeHours = json['responsetime_Hours'];
//     resolutiontimeHours = json['resolutiontime_Hours'];
//     secNdslaresolutionHours = json['secNdslaresolution_Hours'];
//     swStarttime = json['swStarttime'];
//     swEndtime = json['swEndtime'];
//     phoneNo = json['phoneNo'];
//     priority = json['priority'];
//     lastStatus = json['lastStatus'];
//     oemId = json['oemId'];
//     assginStatus = json['assginStatus'];
//     department = json['department'];
//     createdBy = json['createdBy'];
//     isPartRequested = json['isPartRequested'];
//     holdDuration = json['hold_duration'];
//     isNewTicket = json['isNewTicket'];
//     ticketDate = json['ticketDate'];
//     lastRemark = json['lastRemark'];
//     lastRemarkDate = json['lastRemarkDate'];
//     status = json['status'];
//     ticketCategory = json['ticketCategory'];
//     clusterId = json['clusterId'];
//     ticketSerialNo = json['ticketSerialNo'];
//     createdAt = json['createdAt'];
//     updatedAt = json['updatedAt'];
//     iV = json['__v'];
//     adminId = json['adminId'];
//     contractCalllimit = json['contractCalllimit'];
//     contractlineno = json['contractlineno'];
//     phoneNoext = json['phoneNoext'];
//     isVIP = json['isVIP'];
//     currentOwner = json['currentOwner'];
//     assignedAdmin = json['assignedAdmin'];
//     dateTm = json['dateTm'];
//     holdStartTime = json['hold_start_time'];
//     isAccepted = json['isAccepted'];
//     ticketResolvedDate = json['ticketResolvedDate'];
//     engAssigndate = json['engAssigndate'];
//     engineer = json['engineer'];
//     pendingComment = json['pendingComment'];
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     if (this.location != null) {
//       data['location'] = this.location!.toJson();
//     }
//     if (this.address != null) {
//       data['address'] = this.address!.toJson();
//     }
//     data['erpUpdate'] = this.erpUpdate;
//     if (this.lastEngineer != null) {
//       data['lastEngineer'] = this.lastEngineer!.map((v) => v.toJson()).toList();
//     }
//     data['dept_history'] = this.deptHistory;
//     if (this.statusHistory != null) {
//       data['statusHistory'] = this.statusHistory!.map((v) => v.toJson()).toList();
//     }
//     data['ticketattachments'] = this.ticketattachments;
//     if (this.auditTrail != null) {
//       data['auditTrail'] = this.auditTrail!.map((v) => v.toJson()).toList();
//     }
//     data['partReq'] = this.partReq;
//     data['sla_percentage'] = this.slaPercentage;
//     data['sla_response_percent'] = this.slaResponsePercent;
//     data['sla_response_given'] = this.slaResponseGiven;
//     data['on_hold'] = this.onHold;
//     data['reopenStatus'] = this.reopenStatus;
//     data['reOpenCount'] = this.reOpenCount;
//     data['isPdcStCheck'] = this.isPdcStCheck;
//     data['isDTC'] = this.isDTC;
//     if (this.reopenAttachments != null) {
//       data['reopenAttachments'] = this.reopenAttachments!.map((v) => v.toJson()).toList();
//     }
//     if (this.cancelledAttachments != null) {
//       data['cancelledAttachments'] = this.cancelledAttachments!.map((v) => v.toJson()).toList();
//     }
//     if (this.csrAttachments != null) {
//       data['csrAttachments'] = this.csrAttachments!.map((v) => v.toJson()).toList();
//     }
//     data['_id'] = this.sId;
//     data['sla_remaning_seconds'] = this.slaRemaningSeconds;
//     data['sla_response_remaning_seconds'] = this.slaResponseRemaningSeconds;
//     if (this.comments != null) {
//       data['comments'] = this.comments!.map((v) => v.toJson()).toList();
//     }
//     if (this.acceptInfo != null) {
//       data['acceptInfo'] = this.acceptInfo!.map((v) => v.toJson()).toList();
//     }
//     data['userName'] = this.userName;
//     data['assetId'] = this.assetId;
//     data['storeMpsUser'] = this.storeMpsUser;
//     data['serviceItemNo'] = this.serviceItemNo;
//     data['assetType'] = this.assetType;
//     data['callMode'] = this.callMode;
//     data['callType'] = this.callType;
//     data['source'] = this.source;
//     data['customer'] = this.customer;
//     data['custName'] = this.custName;
//     data['contactPerson'] = this.contactPerson;
//     data['assestWarranty'] = this.assestWarranty;
//     data['serviceType'] = this.serviceType;
//     data['severity'] = this.severity;
//     data['accountManager'] = this.accountManager;
//     data['contactno'] = this.contactno;
//     data['contractNo'] = this.contractNo;
//     data['scheduledTime'] = this.scheduledTime;
//     data['scheduledDate'] = this.scheduledDate;
//     data['issue'] = this.issue;
//     data['issueCategory'] = this.issueCategory;
//     data['issueSubCategory'] = this.issueSubCategory;
//     data['typeOfRequest'] = this.typeOfRequest;
//     data['assetMake'] = this.assetMake;
//     data['assetModel'] = this.assetModel;
//     data['assetSerial'] = this.assetSerial;
//     data['contractType'] = this.contractType;
//     data['contractEnddate'] = this.contractEnddate;
//     data['contractExpired'] = this.contractExpired;
//     data['contractStartdate'] = this.contractStartdate;
//     data['isCurrentOwner'] = this.isCurrentOwner;
//     data['sun'] = this.sun;
//     data['mon'] = this.mon;
//     data['tue'] = this.tue;
//     data['wed'] = this.wed;
//     data['thu'] = this.thu;
//     data['fri'] = this.fri;
//     data['sat'] = this.sat;
//     data['serviceCount'] = this.serviceCount;
//     data['custCode'] = this.custCode;
//     data['postalcode'] = this.postalcode;
//     data['responsetime_Hours'] = this.responsetimeHours;
//     data['resolutiontime_Hours'] = this.resolutiontimeHours;
//     data['secNdslaresolution_Hours'] = this.secNdslaresolutionHours;
//     data['swStarttime'] = this.swStarttime;
//     data['swEndtime'] = this.swEndtime;
//     data['phoneNo'] = this.phoneNo;
//     data['priority'] = this.priority;
//     data['lastStatus'] = this.lastStatus;
//     data['oemId'] = this.oemId;
//     data['assginStatus'] = this.assginStatus;
//     data['department'] = this.department;
//     data['createdBy'] = this.createdBy;
//     data['isPartRequested'] = this.isPartRequested;
//     data['hold_duration'] = this.holdDuration;
//     data['isNewTicket'] = this.isNewTicket;
//     data['ticketDate'] = this.ticketDate;
//     data['lastRemark'] = this.lastRemark;
//     data['lastRemarkDate'] = this.lastRemarkDate;
//     data['status'] = this.status;
//     data['ticketCategory'] = this.ticketCategory;
//     data['clusterId'] = this.clusterId;
//     data['ticketSerialNo'] = this.ticketSerialNo;
//     data['createdAt'] = this.createdAt;
//     data['updatedAt'] = this.updatedAt;
//     data['__v'] = this.iV;
//     data['adminId'] = this.adminId;
//     data['contractCalllimit'] = this.contractCalllimit;
//     data['contractlineno'] = this.contractlineno;
//     data['phoneNoext'] = this.phoneNoext;
//     data['isVIP'] = this.isVIP;
//     data['currentOwner'] = this.currentOwner;
//     data['assignedAdmin'] = this.assignedAdmin;
//     data['dateTm'] = this.dateTm;
//     data['hold_start_time'] = this.holdStartTime;
//     data['isAccepted'] = this.isAccepted;
//     data['ticketResolvedDate'] = this.ticketResolvedDate;
//     data['engAssigndate'] = this.engAssigndate;
//     data['engineer'] = this.engineer;
//     data['pendingComment'] = this.pendingComment;
//     return data;
//   }
// }
//
// class Location {
//   List<double>? coordinates;
//   String type;
//
//   Location({this.coordinates, this.type});
//
//   Location.fromJson(Map<String, dynamic> json) {
//     coordinates = json['coordinates'].cast<double>();
//     type = json['type'];
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data['coordinates'] = this.coordinates;
//     data['type'] = this.type;
//     return data;
//   }
// }
//
// class Address {
//   String line1;
//   String line2;
//   String city;
//   String state;
//   String postalcode;
//
//   Address({this.line1, this.line2, this.city, this.state, this.postalcode});
//
//   Address.fromJson(Map<String, dynamic> json) {
//     line1 = json['line1'];
//     line2 = json['line2'];
//     city = json['city'];
//     state = json['state'];
//     postalcode = json['postalcode'];
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data['line1'] = this.line1;
//     data['line2'] = this.line2;
//     data['city'] = this.city;
//     data['state'] = this.state;
//     data['postalcode'] = this.postalcode;
//     return data;
//   }
// }
//
// class LastEngineer {
//
//
//   LastEngineer({});
//
// LastEngineer.fromJson(Map<String, dynamic> json) {
// }
//
// Map<String, dynamic> toJson() {
//   final Map<String, dynamic> data = new Map<String, dynamic>();
//   return data;
// }
// }
//
// class StatusHistory {
//   String status;
//   String updatedTime;
//
//   StatusHistory({this.status, this.updatedTime});
//
//   StatusHistory.fromJson(Map<String, dynamic> json) {
//     status = json['status'];
//     updatedTime = json['updatedTime'];
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data['status'] = this.status;
//     data['updatedTime'] = this.updatedTime;
//     return data;
//   }
// }
//
// class AcceptInfo {
//   String sId;
//   String comment;
//   String dateTime;
//   String userName;
//   String userId;
//
//   AcceptInfo({this.sId, this.comment, this.dateTime, this.userName, this.userId});
//
//   AcceptInfo.fromJson(Map<String, dynamic> json) {
//     sId = json['_id'];
//     comment = json['comment'];
//     dateTime = json['dateTime'];
//     userName = json['userName'];
//     userId = json['userId'];
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data['_id'] = this.sId;
//     data['comment'] = this.comment;
//     data['dateTime'] = this.dateTime;
//     data['userName'] = this.userName;
//     data['userId'] = this.userId;
//     return data;
//   }
// }
