import 'dart:async';
import 'dart:convert';
import 'dart:typed_data';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
//import 'package:flutter_animarker/lat_lng_interpolation.dart';
//import 'package:flutter_animarker/models/lat_lng_delta.dart';
import 'package:customer/Dashboard.dart';
import 'package:customer/pin_pill_info.dart';
import 'package:flutter_polyline_points/flutter_polyline_points.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:socket_io_client/socket_io_client.dart';
import 'package:http/http.dart' as http;


const double CAMERA_ZOOM = 16;
const double CAMERA_TILT = 80;
const double CAMERA_BEARING = 30;
PinInformation sourcePinInfo;
//LatLngInterpolationStream _latLngStream = LatLngInterpolationStream();
//StreamSubscription<LatLngDelta> subscription;
var _markerIdCounter=0;
PinInformation currentlySelectedPin = PinInformation(
    pinPath: '',
    avatarPath: '',
    location: LatLng(0, 0),
    locationName: '',
    labelColor: Colors.grey);

class MyApp1 extends StatefulWidget {
  String id;
  var ticket_lat,ticket_lng,eng_lat,eng_lng;
  MyApp1(this.id,this.ticket_lat,this.ticket_lng,this.eng_lat,this.eng_lng);
  @override
  MapScreen createState() => MapScreen(id,ticket_lat,ticket_lng,eng_lat,eng_lng);
}


class MapScreen extends State<MyApp1> {
  String id;
  var ticket_lat,ticket_lng,eng_lat,eng_lng;
  MapScreen(this.id,this.ticket_lat,this.ticket_lng,this.eng_lat,this.eng_lng);
  GoogleMapController myController;

  // double _destLatitude;
  // // = 28.6623,
  //     double _destLongitude ;
  // // = 77.1411;
  Map<MarkerId, Marker> markers = {};
  Map<PolylineId, Polyline> polylines = {};
  List<LatLng> polylineCoordinates = [];
  PolylinePoints polylinePoints = PolylinePoints();
  String apiKey = "AIzaSyAWgUYmMPpD3mcpEBjz2FOB94PNqKGc-N0";
  Completer<GoogleMapController> _controller = Completer();
  Completer<GoogleMapController> _mapController = Completer();

  Set<Marker> _markers = Set<Marker>();
  LatLng SOURCE_LOCATION = LatLng(28.57061, 77.18792);
  double pinPillPosition = -100;
  var eta_controller = TextEditingController();
  var timeTaken;
  final LatLng _center = const LatLng(28.5679, 77.1881);

  var _originLatitude, _originLongitude,dest_latitude,dest_longitude;

  void _onMapCreated(GoogleMapController controller) {
    _controller.complete(controller);
    // if ([SOURCE_LOCATION] != null) {
    MarkerId markerId = MarkerId(_markerIdVal());
    // LatLng position = [SOURCE_LOCATION] as LatLng;
    Marker marker = Marker(
      markerId: markerId,
      position: LatLng(ticket_lat,ticket_lng),
      draggable: false,
    );
    setState(() {
      markers[markerId] = marker;
    });

    Future.delayed(Duration(seconds: 1), () async {
      GoogleMapController controller = await _controller.future;
      controller.animateCamera(
        CameraUpdate.newCameraPosition(
          CameraPosition(
            target: LatLng(ticket_lat,ticket_lng),

            zoom: 12.0,
          ),
        ),
      );
    });
  }



  @override
  Widget build(BuildContext context) {

    // print("Map Screen id is: "+id);
    Color color1 = _colorFromHex("#00ABC5");
    CameraPosition initialCameraPosition = CameraPosition(
      zoom: CAMERA_ZOOM,
      tilt: CAMERA_TILT,
      bearing: CAMERA_BEARING,

      target: LatLng(eng_lat, eng_lng),);

    // if (currentLocation != null) {


    return WillPopScope(
      onWillPop: _onBackPressed,
      child:MaterialApp(
      home: Scaffold(
        appBar: AppBar(title: Text("Tracking"),  flexibleSpace: Image(
          image: AssetImage('assets/appbar_background.png'),
          fit: BoxFit.cover,
        ),
          backgroundColor: Colors.transparent,),
          body:
           Stack(
          children: <Widget>[
            Container(margin: EdgeInsets.fromLTRB(25, 30, 20, 30),
              child: Text("Estimated Time of Arrival"),),
            Container(
              padding: EdgeInsets.all(20),
              margin: EdgeInsets.fromLTRB(10, 20, 10, 5),
              child: TextField(
                controller: eta_controller??"Eta",
                textAlign: TextAlign.left,
                style: TextStyle(fontWeight: FontWeight.bold),
                readOnly: true,

              ),

            ),


            Container(margin: EdgeInsets.fromLTRB(10, 150, 10, 10),
              child: GoogleMap(
                myLocationEnabled: true,
                compassEnabled: true,
                tiltGesturesEnabled: false,
                markers: Set<Marker>.of(markers.values),
                polylines:Set<Polyline>.of(polylines.values),
                mapType: MapType.normal,
                onMapCreated: _onMapCreated,
                initialCameraPosition: initialCameraPosition,
                onTap: (LatLng loc) {
                },
              ),


            ),



            // Padding(
            //   padding: const EdgeInsets.all(14.0),
            //   child: Align(
            //     alignment: Alignment.topRight,
            //     child: FloatingActionButton(
            //       onPressed: () => print('You have pressed the button'),
            //       materialTapTargetSize: MaterialTapTargetSize.padded,
            //       backgroundColor: color1,
            //       child: const Icon(Icons.map, size: 30.0),
            //     ),
            //   ),
            // ),
          ],
        ),
      ),
      ),);
  }

  Color _colorFromHex(String hexColor) {
    final hexCode = hexColor.replaceAll('#', '');
    return Color(int.parse('FF$hexCode', radix: 16));
  }

  Future<bool> _onBackPressed() {

    print("===trackkkkkkkkk");

    Navigator.pushAndRemoveUntil<dynamic>(
      context,
      MaterialPageRoute<dynamic>(
        builder: (BuildContext context) => Dashboard(),
      ),
          (route) => false,//if you want to disable back feature set to false
    );
  }


    void getTime(String cuttentlat, currentlog, destlat, destlong) async {
    print("called"+cuttentlat+currentlog+destlat+destlong);
    var urlString = "https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=" +cuttentlat + "," + currentlog + "&destinations=" +destlat+","+destlong + "&key=AIzaSyAWgUYmMPpD3mcpEBjz2FOB94PNqKGc-N0";
    print("currenturl" + urlString);
    // 'https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=29.4727,77.7085&destinations=28.9845,77.7064&key=AIzaSyAWgUYmMPpD3mcpEBjz2FOB94PNqKGc-N0'
    final response =
    await http.get(Uri.parse(urlString));
    if (response.statusCode == 200) {
      // var result = await http.Response.fromStream(response);
      var jsonResponse = json.decode(response.body);
      if (jsonResponse['rows'][0]['elements'][0]['status'] == "ZERO_RESULTS"){
        setState(() {
          timeTaken = "Undefined Time";
        });
      }else{
        var respo = jsonResponse['rows'][0]['elements'][0]['duration']['text'];
        print("respo " + respo);

        setState(() {
          timeTaken = respo;
          eta_controller.text=timeTaken;
        });
      }
      print("==result get APi" + response.body.toString());
    } else {
      print("==response error" + response.toString());
    }
    // if (response.statusCode == 200) {
    //   // If the server did return a 200 OK response,
    //   // then parse the JSON.
    //   return Album.fromJson(jsonDecode(response.body));
    // } else {
    //   // If the server did not return a 200 OK response,
    //   // then throw an exception.
    //   throw Exception('Failed to load album');
    // }
    // var request = http.get(Uri.parse("https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=29.4727,77.7085&destinations=28.9845,77.7064&key=AIzaSyAWgUYmMPpD3mcpEBjz2FOB94PNqKGc-N0"));
    //       print("request" + request.toString());

    //
    // var response = await request.send();
  }

  _addMarker(LatLng position, String id, BitmapDescriptor descriptor) {
    MarkerId markerId = MarkerId(id);
    Marker marker =
    Marker(markerId: markerId, icon: descriptor, position: position);
    markers[markerId] = marker;

    sourcePinInfo = PinInformation(
        locationName: "Start Location",
        location: SOURCE_LOCATION,
        pinPath: "assets/delivery1.png",
        avatarPath: "assets/delivery1.png",
        labelColor: Colors.blueAccent);
  }


  // updateMarker(id){
  //
  //   final marker = markers.values.toList().firstWhere((item) => item.markerId == id);
  //
  //   Marker _marker = Marker(
  //     markerId: marker.markerId,
  //     onTap: () {
  //       print("tapped");
  //     },
  //     position: LatLng(marker.position.latitude, marker.position.longitude),
  //     icon: marker.icon,
  //     infoWindow: InfoWindow(title: 'my new Title'),
  //   );
  //
  //   setState(() {
  //     //the marker is identified by the markerId and not with the index of the list
  //     markers[id] = _marker;
  //   });
  // }


  _addPolyLine() {
    PolylineId id = PolylineId("poly");
    Polyline polyline = Polyline(
        polylineId: id, color: Colors.blue, points: polylineCoordinates);
    polylines[id] = polyline;

    setState(() {
      // create a Polyline instance
      // with an id, an RGB color and the list of LatLng pairs
      // create a Polyline instance
      // with an id, an RGB color and the list of LatLng pairs
      // add the constructed polyline as a set of points
      // to the polyline set, which will eventually
      // end up showing up on the map
    });
  }

  _getPolyline() async {
    PolylineResult result = await polylinePoints.getRouteBetweenCoordinates(
      apiKey,
      PointLatLng(eng_lat, eng_lng),
      PointLatLng(ticket_lat, ticket_lng),
      travelMode: TravelMode.driving,

    );
    if (result.points.isNotEmpty) {
      print(result.points);
      result.points.forEach((PointLatLng point) {
        polylineCoordinates.add(LatLng(point.latitude, point.longitude));
      });
    }
    _addPolyLine();


    // Dio dio = new Dio();
    // Response response=await dio.get("https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=$eng_lat,$eng_lng&destinations=$lat%2C,$lng&key=AIzaSyAWgUYmMPpD3mcpEBjz2FOB94PNqKGc-N0");
    // print("==ETA"+response.data.toString());

  }

  @override
  void initState() {

   // subscription= _latLngStream .getLatLngInterpolation().listen((LatLngDelta delta) {
   //   LatLng from = delta.from;
   //   LatLng to = delta.to;
   // });
    super.initState();
    // compute(socket(),null);
    initPlatformState();

    // getTime(ticket_lat.toString(),ticket_lng.toString(),eng_lat.toString(),eng_lng.toString());
    // origin marker

      BitmapDescriptor myIconss;
      BitmapDescriptor.fromAssetImage(
        ImageConfiguration(size:  Size(48,48)),'assets/delivery1.png')
    .then((onValue){
      myIconss = onValue;
  });

    _addMarker(LatLng(eng_lat, eng_lng), "sourcePin",myIconss);
    // destination marker
    _addMarker(LatLng(ticket_lat, ticket_lng), "destination",
        BitmapDescriptor.defaultMarkerWithHue(90));
    _getPolyline();
  }

    double latitude;
    double longitude;
    updateMarker( lat1, lng1)  {
      String id='sourcePin';
    print("==udate marker"+lat1.toString()+lng1.toString());

      setState(() async {
        var pinPosition = LatLng(lat1, lng1);
        final GoogleMapController controller = await _controller.future;
        controller.moveCamera(CameraUpdate.newCameraPosition(CameraPosition(target: pinPosition, zoom: 16)),);


      // _addMarker(LatLng(lat1,lng1), "sourcePin",
      //   BitmapDescriptor.fromAsset('assets/delivery1.png',),);

    });

  }

  socket() {
    // 10.11.4.59:8080
    Socket socket1 = io ('http://34.93.27.23:8080', <String, dynamic>{
      'transports': ['websocket'],
      'autoConnect': true,
      // 'extraHeaders': {'foo': 'bar'} // optional
    });
    socket1.connect();
    print("==hello server");
    // getTime(latitude.toString(), longitude.toString(), ticket_lat.toString(), ticket_lng.toString());
    socket1.on(id, (data) {
      print("socket"+data);
      // compute(  latitude=data['long'],longitude=data['lat']);
       var hjhj=jsonDecode(data);
       var  latitude=hjhj['lat'];
       var  longitude=hjhj['lng'];
       if(latitude!=null && longitude!=null){
      updateMarker (double.parse(latitude), double.parse(longitude));
      }
    });

  }

  Future<void> initPlatformState() async {
    // Configure BackgroundFetch.


      socket();
      getTime(eng_lat.toString(), eng_lng.toString(), ticket_lat.toString(), ticket_lng.toString());






  }

}


String _markerIdVal({bool increment = false}) {
  String val = 'marker_id_$_markerIdCounter';
  if (increment) _markerIdCounter++;
  return val;
}


@override
void dispose() {
 // subscription.cancel();
 // _latLngStream.cancel();
}


class Utils {
  static String mapStyles = '''[
  {
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#f5f5f5"
      }
    ]
  },
  {
    "elementType": "labels.icon",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#616161"
      }
    ]
  },
  {
    "elementType": "labels.text.stroke",
    "stylers": [
      {
        "color": "#f5f5f5"
      }
    ]
  },
  {
    "featureType": "administrative.land_parcel",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#bdbdbd"
      }
    ]
  },
  {
    "featureType": "poi",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#eeeeee"
      }
    ]
  },
  {
    "featureType": "poi",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#757575"
      }
    ]
  },
  {
    "featureType": "poi.park",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#e5e5e5"
      }
    ]
  },
  {
    "featureType": "poi.park",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#9e9e9e"
      }
    ]
  },
  {
    "featureType": "road",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#ffffff"
      }
    ]
  },
  {
    "featureType": "road.arterial",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#757575"
      }
    ]
  },
  {
    "featureType": "road.highway",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#dadada"
      }
    ]
  },
  {
    "featureType": "road.highway",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#616161"
      }
    ]
  },
  {
    "featureType": "road.local",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#9e9e9e"
      }
    ]
  },
  {
    "featureType": "transit.line",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#e5e5e5"
      }
    ]
  },
  {
    "featureType": "transit.station",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#eeeeee"
      }
    ]
  },
  {
    "featureType": "water",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#c9c9c9"
      }
    ]
  },
  {
    "featureType": "water",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#9e9e9e"
      }
    ]
  }
]''';

}
