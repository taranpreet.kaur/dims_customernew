import 'package:customer/Dashboard.dart';
import 'package:customer/TicketDetails.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class TestWidget extends StatefulWidget {


  @override
  _TestWidgetState createState() => _TestWidgetState();
}

class _TestWidgetState extends State<TestWidget> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: InkWell(
          onTap: (){
            Navigator.push(context,CupertinoPageRoute(builder: (context){
              return Dashboard(selectedIndex: 1,);
            }));
          },
          child: Container(
            height: 40.0,
            width: 200.0,
            color: Colors.black,
          ),
        ),
      ),
    );
  }
}
