import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:customer/LoginTab.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:http/http.dart' as http;

import 'Constant.dart';
import 'Dashboard.dart';
import 'form_model.dart';
import 'my_form_text_field.dart';

class ResetPassword extends StatefulWidget{
  String email;
  ResetPassword(this.email);


  @override
  ResetPasswordState createState() => ResetPasswordState(email);

}

class ResetPasswordState extends State<ResetPassword> {
  String email;
  String password;
  ResetPasswordState(this.email);
  bool _obscureText = true;
  // Toggles the password show status
  void _togglePasswordStatus() {
    setState(() {
      _obscureText = !_obscureText;
    });
  }
  final _formKey = GlobalKey<FormState>();
  // holds the form data for access
  final model = FormModel();
  TextEditingController passwordController=TextEditingController();
  TextEditingController email1=TextEditingController();
  @override
  Widget build(BuildContext context) {
    Color color1 = _colorFromHex("#00ABC5");
    return FlutterEasyLoading(
      child:Scaffold(
        appBar: AppBar(
        title: Text("Reset Password"),

    flexibleSpace: Image(
    image: AssetImage('assets/appbar_background.png'),
    fit: BoxFit.cover,
    ),),
        body: SingleChildScrollView(
            padding: EdgeInsets.all(20.0),
            child: Container(
              child: Column(

                children: <Widget>[
                  Form(
                    key: _formKey,
                    child: Column(
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.fromLTRB(10, 10, 10, 10),
                          child: TextFormField(
                            readOnly: true,
                            controller: email1,
                            decoration: InputDecoration(
                              border: OutlineInputBorder(),
                              hintText: 'Password',),

                          ),),

                          Container(
                          margin: EdgeInsets.fromLTRB(10, 10, 10, 10),
                          child: TextFormField(
                          controller: passwordController,
                          decoration: InputDecoration(
                           border: OutlineInputBorder(),
                            hintText: 'Password',
                            suffixIcon:  IconButton(
                              icon:Icon(_obscureText ? Icons.visibility_off:Icons.visibility),
                              onPressed: _togglePasswordStatus,
                              color: Colors.black,
                            ),
                          ),
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Please enter Password';
                            }
                            return null;
                          },
                          obscureText: _obscureText,
                          onChanged: (val){
                            setState(() {
                              password = val.trim();
                            });
                          },
                        ),),




                        Container(

                            height: 50,
                            width: 500,
                            padding: EdgeInsets.fromLTRB(10, 10, 10, 0),
                            margin: EdgeInsets.fromLTRB(10, 10, 10, 0),

                                child: FlatButton(
                                  textColor: Colors.white,
                                  color: color1,
                                  child: Text(
                                    'Submit',
                                  ),
                                  onPressed: () {
    if (_formKey.currentState.validate()) {
    _formKey.currentState.save();
    resetPassword(email, passwordController.text, context);
    }
                                    print("==passwordreset"+passwordController.text+email);
                                    // Navigator.push(
                                    //   context,
                                    //   MaterialPageRoute(builder: (context) => LoginTab()),
                                    // );
                                  },
                                )


                            ),
                        // FormSubmitButton(


                      ],
                    ),
                  )
                ],
              ),
            )),
      ),);
  }


  Color _colorFromHex(String hexColor) {
    final hexCode = hexColor.replaceAll('#', '');
    return Color(int.parse('FF$hexCode', radix: 16));
  }


  resetPassword(String email,password,BuildContext context) async {
    print("==email"+email);
    print("==password"+password);
    // EasyLoading.show(status:"Loading");
    // Navigator.push(context,MaterialPageRoute(builder: (context) => ForgotOtp("email")),);

    EasyLoading.show(status:"Loading");
    // http://192.168.49.1:8080/api/ticket/index
    var url=Constants.base_url+"api/cust/resetPassword";
    var request =  http.MultipartRequest('POST',Uri.parse(url));
    print("request"+request.toString());
    request.fields['email']=email;
    request.fields['password']=password;

    var response=await request.send();
    SnackBar(content: Text('Processing Data'));
    if(response.statusCode == 200) {
      EasyLoading.dismiss(animation: false);
      EasyLoading.showToast("Success");
      Navigator.push(context,MaterialPageRoute(builder: (context) => LoginTab()));

    }
    else if(response.statusCode == 400) {
      EasyLoading.showToast("Email is Incorrect");

    }









  }
  @override
  void initState() {
  super.initState();
  email1.text=email;


  }



}