import 'dart:async';
import 'dart:convert';
import 'dart:developer';
import 'package:customer/Constant.dart';
import 'package:customer/myform.dart';
import 'package:date_time_picker/date_time_picker.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'Article.dart';
import 'package:intl/intl.dart';
import 'NotificationResponse.dart';
import 'Notify.dart';

class NotificationList extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _NotificationListState();
  }
}

class _NotificationListState extends State<NotificationList> {
  String _message = '';
  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging.instance;
  // chnged list based on query
  List<NotificationResponse> filteredList = [];
  // to store all notification list
  List<NotificationResponse> allNotificationList = [];
  bool isApiLoading = true;
  Timer timer1;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getMessage();
    // called this api from future builder
    getData();
   // autoRefreshDetails();
  }

  void getMessage() {
    FirebaseMessaging.onMessage.listen((RemoteMessage message) {
      print('Got a message whilst in the foreground!');
      print('Message data: ${message.data}');
      _message = message.data["notification"]["title"];


      if (message.notification != null) {
        print('Message also contained a notification: ${message.notification}');
      }
    });
    // _firebaseMessaging.configure(
    //     onMessage: (Map<String, dynamic> message) async {
    //   print('on message $message');
    //   setState(() => _message = message["notification"]["title"]);
    // },
    //     // onBackgroundMessage: myBackgroundMessageHandler,
    //     onResume: (Map<String, dynamic> message) async {
    //   print('on resume $message');
    //   setState(() => _message = message["notification"]["title"]);
    // }, onLaunch: (Map<String, dynamic> message) async {
    //   print('on launch $message');
    //   setState(() => _message = message["notification"]["title"]);
    // });
  }

  var tempList = List<String>();

  // void autoRefreshDetails() {
  //   timer1 = Timer.periodic(Duration(seconds: 20), (Timer t) {
  //     if(!isApiLoading){
  //     //  getData();
  //     }
  //   });
  // }
  //
  // @override
  // void dispose() {
  //   // TODO: implement dispose
  //   super.dispose();
  //   // this will stop api calling
  //   if (timer1 != null) {
  //     timer1.cancel();
  //   }
  // }

  @override
  Widget build(BuildContext context) {
    Color color1 = _colorFromHex("#00ABC5");
    // TODO: implement build
    return FlutterEasyLoading(
      child: Scaffold(
        backgroundColor: Colors.transparent,
        appBar: AppBar(
          flexibleSpace: Image(
            image: AssetImage('assets/appbar_background.png'),
            fit: BoxFit.cover,
          ),
          title: Text("Notification List"),
        ),
        body: Column(
          children: [
            Container(
              height: 50.0,
              alignment: Alignment.centerLeft,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Align(
                  alignment: Alignment.centerLeft,
                  child: Container(
                    width: MediaQuery.of(context).size.width * 0.80,
                    height: 80,
                    child: TextField(
                      decoration: InputDecoration(
                          contentPadding: EdgeInsets.all(15.0),
                          hintStyle: TextStyle(fontSize: 14.0),
                          hintText: 'Search by Ticket Id & Asset Type',
                          prefixIcon: Icon(Icons.search),
                          border: OutlineInputBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(25.0)),
                          )),
                      onChanged: (query) {
                        filteredList.clear();
                        if (query.isEmpty) {
                              //  restore all list when text field empty
                          Future.delayed(Duration.zero, () {
                            filteredList.addAll(allNotificationList);
                          });
                        } else if (query.length > 1) {
                          // filtered list based on query
                          Future.delayed(Duration.zero, () {
                            allNotificationList.forEach((element) {
                              if (element.notification_details.title.toLowerCase()
                                      .contains(query.trim().toLowerCase()) || element.notification_details.body.toLowerCase()
                                  .contains(query.trim().toLowerCase())) {
                                 print(query+" ==== "+element.notification_details.title);
                                filteredList.add(element);
                              }
                            });
                            setState(() {});
                          });
                        }
                      },
                    ),
                  ),
                ),
              ),
              padding: EdgeInsets.symmetric(horizontal: 10.0),
            ),
            SizedBox(
              height: 10,
            ),
            // Flexible(
            //     child: FutureBuilder(
            //         future: getData(),
            //         builder: (context, snapshot) {
            //           return snapshot.data != null
            //               ? listViewWidget1(filteredList)
            //               : Center(child: CircularProgressIndicator());
            //         })),
            Expanded(child:isApiLoading ? Center(child: CircularProgressIndicator()) :listViewWidget1(filteredList))
          ],
        ),
      ),
    );
  }

  // Widget listViewWidget(List<NotificationResponse> article) {
  //   return Container(
  //     child: ListView.builder(
  //         itemCount: article.length,
  //         padding: const EdgeInsets.all(2.0),
  //         itemBuilder: (context, position) {
  //           return Card(
  //             child: ListTile(
  //               title: Text(
  //                 '${article[position].notification_details.title}',
  //                 style: TextStyle(
  //                     fontSize: 18.0,
  //                     color: Colors.black,
  //                     fontWeight: FontWeight.normal),
  //               ),
  //               subtitle: Text(
  //                 '${article[position].notification_details.body}',
  //                 style: TextStyle(
  //                     fontSize: 16.0,
  //                     color: Colors.black,
  //                     fontWeight: FontWeight.normal),
  //               ),
  //             ),
  //           );
  //         }),
  //   );
  // }

  Widget listViewWidget1(List<NotificationResponse> article) {
    return Center(
        child: article.length > 0
            ? RefreshIndicator(
          onRefresh: (){
            getData();
          },
              child: ListView.builder(
                  // separatorBuilder: (context, index) => Divider(
                  //   //color: Colors.white,
                  // ),
                  itemCount: article.length,
                  itemBuilder: (context, index) {
                    String formatted = "";

                    try {
                      final DateFormat formatter =
                          DateFormat('MM-dd-yyyy HH:mm:ss a');
                      DateTime dateTime = formatter
                          .parse(article[index].notification_details.date);
                      final DateFormat newFormat = DateFormat('dd-MM-yy');
                      formatted = newFormat.format(dateTime);
                    } catch (e) {
                      // final DateFormat formatter = DateFormat('MM-dd-yyyy');
                      // DateTime dateTime = formatter.parse(article[index].notification_details.date);
                      // final DateFormat newFormat = DateFormat('dd-MM-yy');
                      // formatted = newFormat.format(dateTime);
                      //  formatted = "Invalid Date";
                      print("Notification Date " +
                          article[index].notification_details.date);
                    }
                    print("New Date " + formatted);
                    return Padding(
                        padding: EdgeInsets.fromLTRB(7, 1, 7, 4),
                        child: InkWell(
                          onTap: () async {},
//                           child: Container(
//                             height: 120,
//                             child: Card(
//
//                               // shape: RoundedRectangleBorder(
//                               //   borderRadius: BorderRadius.circular(1.0),
//                               // ),
//                               // color: Colors.white,
//                               //elevation: 3,
//                               child: Row(
//                                 mainAxisSize: MainAxisSize.min,
//                                 children: <Widget>[
//                                   Container(
//                                     height: 120,
//                                     width: 2 ,
// color: Colors.red,
//
//                                   ),
//                                   SizedBox(
//                                     width: 13,
//
//                                   ),
//                                   Container(
//                                     height: 40,
//                                     width: 60 ,
//
//                                     child: Text("26 Nov 2021"),
//                                   ),
//                                   SizedBox(
//                                     // width: ,
//
//                                   ),
//                                   Container(
//
//                                     height: 70,
//                                     width: 2,
//                                     color: Colors.black12,
//                                   ),
//                                   SizedBox(
//                                     width: 10,
//
//                                   ),
//                                   Container(
//                                     height: 100,
//
//                                     child: Column(
//                                         // mainAxisAlignment: MainAxisAlignment.start,
//                                         // mainAxisSize: MainAxisSize.min,
//                                         children: <Widget>[
//                                           // Align(
//                                           //
//                                           //     child: Text("Sector 18, Gurugram,\nHaryana 122022",
//                                           //       style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold,
//                                           //           color: Color(0xFF101010)),)
//                                           // ),
//                                           SizedBox(
//                                             height: 8,
//                                           ),
//
//
//                                          Padding(
//                                             padding: EdgeInsets.fromLTRB(3, 3, 0, 0),
//                                               child: Align(
//
//                                                  alignment: Alignment.bottomLeft,
//                                                child: Text(
//                                                  '${article[index].notification_details.title}',
//                                                  style: TextStyle(fontSize: 20.0,color: Colors.black),
//                                                textAlign: TextAlign.left ,
//                                                ),
//                                            ),
//                                             ),
//
//
//
//                                        SizedBox(
//                                          height: 10,
//                                        ),
//                                           Container(
//                                              width: 300,
//                                             child: Text(
//                                                 '${article[index].notification_details.body}'
//                                                 ,
//                                                 overflow: TextOverflow.ellipsis,
//                                                 maxLines: 5,
//
//                                                 style: TextStyle(fontSize: 15.0)
//                                             )
//                                           ),
//                                        ]
//                                     ),)
//
//
//
//                                 ],
//                               ),
//                             ),
//                           ),
                          child: Container(
                            height: 100,
                            child: Card(
                              // shape: RoundedRectangleBorder(
                              //   borderRadius: BorderRadius.circular(1.0),
                              // ),
                              // color: Colors.white,
                              //elevation: 3,
                              child: Row(
                                // mainAxisAlignment: MainAxisAlignment.start,
                                mainAxisSize: MainAxisSize.min,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  Container(
                                    height: 100,
                                    width: 2,
                                    color: Colors.red,
                                  ),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  Container(
                                    height: 60,
                                    width: 90,

                                    child: Text(
                                        '${article[index].notification_details.date}'),
                                    // ${article[index].notification_details.body}
                                  ),
                                  SizedBox(
                                    width: 2,
                                  ),
                                  Container(
                                    height: 70,
                                    width: 2,
                                    color: Colors.black12,
                                  ),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  Flexible(
                                      child: Container(
                                    height: 80,
                                    child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        // mainAxisAlignment: MainAxisAlignment.start,
                                        // mainAxisSize: MainAxisSize.min,
                                        children: <Widget>[
                                          // Align(
                                          //
                                          //     child: Text("Sector 18, Gurugram,\nHaryana 122022",
                                          //       style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold,
                                          //           color: Color(0xFF101010)),)
                                          // ),
                                          SizedBox(
                                            height: 4,
                                          ),

                                          Align(
                                            alignment: Alignment.centerLeft,
                                            child: Container(
                                              //color: Colors.blue,
                                              child: Text(
                                                '${article[index].notification_details.title}',
                                                style: TextStyle(
                                                    fontSize: 15.0,
                                                    fontWeight: FontWeight.bold,
                                                    color: Color(0xFF696969)),
                                                //  textAlign: TextAlign.left ,
                                              ),
                                            ),
                                          ),

                                          SizedBox(
                                            height: 2,
                                          ),
                                          Flexible(
                                            child: Container(
                                                width: 260,
                                                height: 50,
                                                child: Text(

                                                    // "nstance of 'TicketResponse', Instance of 'TicketResponse', Instance of 'TicketResponse', Instance of 'TicketResponse', Instance of 'TicketResponse', Instance of 'TicketResponse', Instance of 'TicketResponse', Instance of 'TicketResponse', Instance of 'TicketResponse']"
                                                    '${article[index].notification_details.body}',
                                                    overflow: TextOverflow.fade,
                                                    maxLines: 7,
                                                    //  softWrap: false,
                                                    //overflow: TextOverflow.fade,

                                                    style: TextStyle(
                                                        fontSize: 13.0))),
                                          ),
                                        ]),
                                  ))
                                ],
                              ),
                            ),
                          ),
                        ));
                  }),
            )
            : Center(
                child: Container(
                child: Text("No data found"),
              )));
  }

  Future<bool> _onBackPressed() {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('Are you sure?'),
            content: Text('Do you want to exit from App!!'),
            actions: <Widget>[
              FlatButton(
                child: Text('YES'),
                onPressed: () {
                  Navigator.of(context).pop(true);
                },
              ),
              FlatButton(
                child: Text('NO'),
                onPressed: () {
                  Navigator.of(context).pop(false);
                },
              ),
            ],
          );
        });
  }

  Future<List<NotificationResponse>> getData() async {
    // EasyLoading.show(status: "Loading..");
    filteredList.clear();
    allNotificationList.clear();
    List<NotificationResponse> list;
    isApiLoading = true;
    setState(() {

    });
    final prefs = await SharedPreferences.getInstance();
    var userId = prefs.getString("userId");
    tempList = List<String>();
    var url = Constants.base_url + "api/extranet/get/notification";

    var request = http.MultipartRequest('POST', Uri.parse(url));
    print("request is" + request.toString());
    request.fields['user_id'] = userId;
    var response = await request.send();
    if (response.statusCode == 200) {
      EasyLoading.dismiss(animation: false);
      var result = await http.Response.fromStream(response);
      var data = json.decode(result.body);

      var rest = data["result"] as List ;

      print("New response is that" + rest.toString());

      list = rest
          .map<NotificationResponse>(
              (json) => NotificationResponse.fromJson(json))
          .toList();
      allNotificationList.addAll(list);
      filteredList.addAll(list);

    } else {
      // EasyLoading.showError("Error");

    }
    print("List Size: ${list.length}");
    isApiLoading = false;
    setState(() {

    });
    return list;
  }

  Color _colorFromHex(String hexColor) {
    final hexCode = hexColor.replaceAll('#', '');
    return Color(int.parse('FF$hexCode', radix: 16));
  }
}
