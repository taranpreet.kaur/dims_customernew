 class User{
  final int id;
  String name;
  String email;
  String phoneNumber;
  String companyName;

  User({this.id, this.name, this.email,this.phoneNumber,this.companyName});

  @override
  String toString() {
    return 'User{id: $id, name: $name, email: $email, phoneNumber: $phoneNumber, companyName: $companyName}';
  }

  factory User.fromJson(Map<String, dynamic> json) {
    return User(
        id: json['_id'],
        name: json['name'].toString(),
        email: json['email'].toString(),
        companyName: json['companyName'].toString(),
      phoneNumber: json['phoneNumber'].toString()
    );
  }
}

