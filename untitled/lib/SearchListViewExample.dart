import 'dart:async';
import 'dart:developer';
import 'package:connectivity/connectivity.dart';
import 'package:date_time_picker/date_time_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'Article.dart';
import 'Services.dart';
import 'package:intl/intl.dart';
import 'package:scroll_to_index/scroll_to_index.dart';
import 'TicketDetails.dart';
import 'globals.dart';

class UserFilterDemo extends StatefulWidget {
  UserFilterDemo() : super();

  // final String title = "Filter List Demo";
  @override
  UserFilterDemoState createState() => UserFilterDemoState();
}

class Debouncer {
  final int milliseconds;
  VoidCallback action;
  Timer _timer;

  Debouncer({this.milliseconds});

  run(VoidCallback action) {
    if (null != _timer) {
      _timer.cancel();
    }
    _timer = Timer(Duration(milliseconds: milliseconds), action);
  }
}

class UserFilterDemoState extends State<UserFilterDemo> {
  //
  final _debouncer = Debouncer(milliseconds: 500);
  List<Article> users = List();
  List<Article> filteredUsers = List();
  final newList = [];
  ConnectionState connectionState;
  var isLoading = false;
  GlobalKey globalKey = GlobalKey();
  String filterText = "";
  Timer timer;

  @override
  void initState() {
    super.initState();
    print("Gloabls Filtertext => " + Globals.filterText);
    if (Globals.filterText.isNotEmpty) {
      filterText = Globals.filterText;
    } else {
      filterText = "";
    }
    controller = AutoScrollController(
        viewportBoundaryGetter: () =>
            Rect.fromLTRB(0, 0, 0, MediaQuery.of(context).padding.bottom),
        axis: scrollDirection);
    setState(() {
      isLoading = true;
      CircularProgressIndicator();
    });
    getAllUsers();
    autoRefreshDetails();
  }

  Future<void> getAllUsers() async {
    print("api called");
    await Services.getUsers().then((usersFromServer) {
      filteredUsers.clear();
      setState(() {
        isLoading = false;
        print("check data");
        // if(connectionState==ConnectionState.waiting)
        // {
        //   loadingView();
        // }
        // else if(connectionState==ConnectionState.active){
        //
        // }
        users = usersFromServer.cast<Article>();

        Future.delayed(Duration.zero, () {
          filteredUsers.addAll(users);

          setState(() {});
          // dont delete
          // debugger();
          if (Globals.filterText.isNotEmpty) {
            print("filtering text");
            filterList();
          }
        });
        // filteredUsers = users;
        // // print("status"+users[0].status);
      });
    });
  }

  void filterList() {
    Future.delayed(Duration.zero, () {
      if (filterText.isNotEmpty) {
        Globals.filterText = "";
        if (filterText == "WIP") {
          buttonList([
            "RIM_ACCEPT",
            "RIM_NEW",
            "RIM_WIP",
            "RIM_PA",
            "RIM_PDC",
            "RIM_FAULTPRST",
            "RIM_TRNSIM",
            "RIM_TOE",
            "RIM_FAULTPR",
            "RIM_TIM",
            "RIM_TENG",
            "RIM_PART_AUTH",
            "RIM_FPR",
            "RIM_FPR_SR",
            "RIM_RTN_ENG",
            "RIM_STD_REQ",
            "RIM_FPR_STD_REQ",
            "ENG_RAS",
           // "ENG_PFS",
            "ENG_SPAREQ",
            "ENG_SPR_REQ",
            "ENG_NEW",
            "ENG_PDC",
            "ENG_ACCEPT",
            "ENG_WIP",
            "ENG_TRANSIM",
            "ENG_TRANS_IM",
            "ENG_TRNS_IM",
            "ENG_WPS",
            "PENG_RAS",
            "PENG_NEW",
           // "PENG_PFS",
            "PENG_PDC",
            "PENG_ACCEPT",
            "PENG_SPAREQ",
            "PENG_TRNS_PART",
            "PENG_WPS",
            "PENG_SPR_REQ",
            "PENG_WIP",
            "PENG_TRANSIM",
            "PENG_TRANS_IM",
            "PENG_TRNS_IM",
            "TRC_NEW",
            "TRC_WIP",
            "TRC_REPAIRED",
            "TRC_NOT_REPAIRABLE",
            "PART_WPS",
            "PART_ASGN_ENGNR",
            "PART_SPR_REQ",
            "PART_ASGN_PENGNR",
            "PART_ASGN_PRTNR",
            "PART_WIP",
            "PART_REJECT",
            "PART_ACCEPT",
            "PART_NEW",
            "PART_PDC",
          //   "PART_UOB",
             "IM_NEW",
            "IM_PDC",
            "RIM_TIM",
            "IM_WIP",
            "IM_ASGND_PRTNR",
            "IM_ASGND_ENGNR",
            "IM_SPART_REQ",
            "IM_TRANSFER_RIM",
            "IM_WPS",
            "STR_NEW",
            "STR_UP",
            "STR_WIP",
            "STR_PDC",
            "STR_SPR_BP",
            "STR_SPR_BP",
            "STR_SPR_DSPTH",
            "STR_SPR_DSPTH-ER",
            "STR_STDBY_DSPTH",
            "STR_FLTY_PFR",
            "STR_TRNS_IM",
            "STR_TRNS_TRC_RPR",
            "STR_TRNS_PRTNR_RPR",
            "STR_FLTY_NTBP",
            "STR_TBD",
            "STR_PKUP",
            "STR_HOVR_PTR_RPR",
            "STR_HOVR_TRC_RPR",
            "STR_RESOLVED",
            "STR_DEL_ER",
           // "STR_DEL_ENR",
            "STR_REP",
            "STR_UREP",
            "STR_REP",
            "STR_UREP"
          ], filterType: "WIP");
        } else if (filterText == "UNASSIGNED") {
          buttonList([
            "RIM_NEW",
            "IM_NEW",
           // "STR_NEW",
            "ENG_NEW",
          ]);
        } else if (filterText == "ONHOLD") {
          buttonList([
            "RIM_PDC",
            "ENG_PDC",
            "PENG_PDC",
            "IM_PDC",
            "STR_PDC",
            "PART_PDC"
          ]);
        } else if (filterText == "INJOURNEY") {

          buttonList(["ENG_PFS", "PENG_PFS"]);
        }

        else if (filterText == "UNDEROBSERVATION") {
          print("here......");
          buttonList([
            "RIM_UOB",
            "RIM_UNDER_OBSERVATION",
            "ENG_UOB",
            "PENG_UOB",
            "IM_OBS",
            "PART_UOB"
                     ]);
        }
        else if (filterText == "STANDBYPROVIDED") {
          buttonList([
            "ENG_STDBY_INSTL",
            "PENG_STDBY_INSTL",
             "STR_DEL_ENR",
            "Standby Delivered Faulty Picked up",
            "Standby Delivered Faulty pending to pick",
            "Standby Delivered",
            "STR_STDBY_DLVR",
            "STR_NEW",
           // "STR_STDBY-DLVR_FP",
            "Standby Delivered and Faulty Picked"
          ]);
        }
      } else {
        print("Filter text null");
      }
    });
  }

  final scrollDirection = Axis.vertical;

  AutoScrollController controller;

  //// To show selective list on filter button click

  buttonList(List<String> statusList, {String filterType}) {
    print("SSSS => "+(filterType ?? "Not Found"));
    filteredUsers.clear();
    Future.delayed(Duration.zero, () {
      if (statusList[0] == "ALL") {
        filteredUsers.addAll(users);
      } else {
        users.forEach((element) {

          if (statusList.contains(element.status)) {
            // // if we get status WIP fr _status method and filter type WIP than it will be added

            if (_status(element.status,element.isStandByInstall,element.lastRemark,element.isPartRequested,element.isNewTicket) == "WIP") {

              if(filterType == "WIP"){
                filteredUsers.add(element);
              }

            } else {
              if(filterType == "WIP"){
                // if(element.status == "ENG_PFS"){
                //   filteredUsers.add(element);
                // }
               print("elementStatus WIP=> "+element.status + " "+element.isNewTicket.toString()+" "+element.ticketSerialNo);
              }else{
                print("elementStatus => "+element.status + " "+element.isNewTicket.toString()+" "+element.ticketSerialNo);

                filteredUsers.add(element);
             }
            }
          }
        });
      }

      // debugger();
      print("length => " + filteredUsers.length.toString());

      setState(() {});
    });
  }

  Future<void> _pullRefresh() async {
    print("pull to resfreshing");
    //  fluxstore
    setState(() {
      getAllUsers();
    });
  }

  void autoRefreshDetails() {
    timer = Timer.periodic(Duration(seconds: 20), (Timer t) {
        getAllUsers();
    });
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    // this will stop api calling
    if (timer != null) {
      timer.cancel();
    }
  }

  Widget _buildList() {
    return isLoading
        ? Center(
            child: CircularProgressIndicator(),
          )
        : filteredUsers.length != 0
            ? RefreshIndicator(
                child: Card(
                  child: Padding(
                    padding: EdgeInsets.all(0.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          color: Color((0xFFF8FCFF)),
                          padding: EdgeInsets.fromLTRB(5, 5, 5, 280),
                          height: MediaQuery.of(context).size.height,
                          width: MediaQuery.of(context).size.width,
                          child: ListView.separated(
                            //padding: EdgeInsets.fromLTRB(20, 20, 20, 20),
                            physics: const BouncingScrollPhysics(
                                parent: AlwaysScrollableScrollPhysics()),
                            separatorBuilder: (context, index) => Divider(
                              color: Colors.transparent,
                            ),
                            itemCount: filteredUsers.length,
                            itemBuilder: (context, index) => Padding(
                              padding: EdgeInsets.all(2.0),
                              child: InkWell(
                                onTap: () async {
                                  // print("ticketid"+ticketId[index]);
                                  //   Article aa=new Article();
                                  // var id=aa.id;
                                  // print("id search page"+id);

                                  SharedPreferences sharedPreferences =
                                      await SharedPreferences.getInstance();
                                  sharedPreferences.setString(
                                      "ticket_id", filteredUsers[index].id);
                                  sharedPreferences.setString("createdAt", filteredUsers[index].ticketDate);
                                  sharedPreferences.setString("assetType", filteredUsers[index].assetType);
                                  sharedPreferences.setString("status", _status(this.filteredUsers[index].status, this.filteredUsers[index].isStandByInstall,this.filteredUsers[index].lastRemark,
                                      this.filteredUsers[index].isPartRequested, this.filteredUsers[index].isNewTicket));
                                  sharedPreferences.setString("ticketid", filteredUsers[index].ticketSerialNo);

                                  // if(aa.assetType==true)
                                  //   {
                                  //
                                  //    CupertinoColors.extraLightBackgroundGray;
                                  //   }
                                  //  Navigator.push(context, MaterialPageRoute(builder: (context) => TicketDetails(filteredUsers[index].id)),);
                                  Navigator.of(context, rootNavigator: true)
                                      .push(
                                    MaterialPageRoute(
                                      builder: (context) => TicketDetails(
                                          filteredUsers[index].id,),
                                    ),
                                  );
                                },
                                // TicketDetails(filteredUsers[index].id))
                                child: Container(
                                  height: 40.0,

                                  padding: EdgeInsets.fromLTRB(5, 5, 5, 5),
                                  child: Column(children: <Widget>[
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Expanded(
                                            flex: 1,
                                            child: Padding(
                                              padding: EdgeInsets.fromLTRB(
                                                  1, 1, 1, 1),

                                              child: Text(
                                                "${this.filteredUsers[index].ticketSerialNo}",
                                                textAlign: TextAlign.center,

                                                // textAlign: TextAlign.right,
                                                style: TextStyle(
                                                  fontSize: 12.0,
                                                  // color:  "${this.dogsBreedList[index]}" == "New" ? Colors.orange : "${this.dogsBreedList[index]}" == "On Hold" ?
                                                  // Colors.red :
                                                  // "${this.dogsBreedList[index]}" == "Except" ? Colors.green : ""
                                                  //     "${this.dogsBreedList[index]}" == "Resolve" ? Colors.yellow :
                                                  // "${this.dogsBreedList[index]}" == "Hold On" ? Colors.grey : Colors.grey
                                                  //(1==1)?Colors.blue:Colors.orange
                                                ),
                                              ),

                                              // this doesn't work for top and bottom
                                            )
                                        ),
                                        // SizedBox(width: 5),
                                        Expanded(
                                          flex: 1,
                                          // var at = toBeginningOfSentenceCase(this.filteredUsers[index].assetType);
                                          child: Text(
                                            assetType(this
                                                .filteredUsers[index]
                                                .assetType),
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                              fontSize: 12.0,
                                              // color:  "${this.dogsBreedList[index]}" == "New" ? Colors.orange : "${this.dogsBreedList[index]}" == "On Hold" ?
                                              // Colors.red :
                                              // "${this.dogsBreedList[index]}" == "Except" ? Colors.green : ""
                                              //     "${this.dogsBreedList[index]}" == "Resolve" ? Colors.yellow :
                                              // "${this.dogsBreedList[index]}" == "Hold On" ? Colors.grey : Colors.grey
                                              //(1==1)?Colors.blue:Colors.orange
                                            ),
                                          ),
                                        ),

                                        // DateTime dateTime = DateTime.parse(this.filteredUsers[index].ticketDate);
                                        // SizedBox(width: 20),
                                        Expanded(
                                          flex: 1,
                                          child: Text(
                                            "${this.filteredUsers[index].ticketDate}",
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                              fontSize: 12.0,
                                              // color:  "${this.dogsBreedList[index]}" == "New" ? Colors.orange : "${this.dogsBreedList[index]}" == "On Hold" ?
                                              // Colors.red :
                                              // "${this.dogsBreedList[index]}" == "Except" ? Colors.green : ""
                                              //     "${this.dogsBreedList[index]}" == "Resolve" ? Colors.yellow :
                                              // "${this.dogsBreedList[index]}" == "Hold On" ? Colors.grey : Colors.grey
                                              //(1==1)?Colors.blue:Colors.orange
                                            ),
                                          ),
                                        ),

                                        // SizedBox(width: 10),

                                        Expanded(
                                          flex: 1,
                                          child: Text(
                                            "${_status(this.filteredUsers[index].status, this.filteredUsers[index].isStandByInstall,this.filteredUsers[index].lastRemark,
                                                this.filteredUsers[index].isPartRequested, this.filteredUsers[index].isNewTicket)}",
                                            textAlign: TextAlign.center,
                                            style: TextStyle(fontSize: 12.0),
                                          ),
                                        ),
                                        SizedBox(
                                          height: 10,
                                        ),
                                        Container(
                                          padding:
                                              EdgeInsets.fromLTRB(1, 1, 1, 1),
                                          height: 0.1,
                                          color: Colors.black,
                                        )
                                      ],
                                    ),
                                  ]),
                                  // ),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                onRefresh: _pullRefresh,
              )
            : Center(
                child: Padding(
                padding: const EdgeInsets.all(20.0),
                child: Container(
                  child: Text(
                    "No data found",
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                ),
              ));
  }

  void showFilterDialog() {
    filterText = "";
    showDialog(
      context: context,
      builder: (context) {
        return Dialog(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
          elevation: 16,
          child: Container(
            width: 300,
            height: 500,
            padding: EdgeInsets.fromLTRB(10, 20, 10, 10),
            alignment: Alignment.centerLeft,
            child: Column(
              children: [
                Align(
                    alignment: Alignment.center,
                    child: Text(
                      "Status",
                      style: TextStyle(
                          fontSize: 17.0,
                          fontWeight: FontWeight.bold,
                          color: Color(0xFF919191)),
                    )),
                new Container(
                  child: Column(
                    children: [
                      Container(
                        child: InkWell(
                          onTap: () {
                            print("ALL");

                            buttonList(["ALL"]);

                            Navigator.of(context).pop();
                          },
                          child: Text('All Tickets'),
                        ),
                        padding: EdgeInsets.only(top: 20.0),
                      ),
                      Container(
                        child: InkWell(
                          onTap: () {
                            print("New Ticket");

                            buttonList([
                              "ENG_NEW",
                              "ENG_REJECT",
                              "RIM_NEW",
                              "IM_NEW",
                              "PENG_NEW",
                              "PENG_REJECT",
                            ]);

                            Navigator.of(context).pop();
                          },
                          child: Text('New'),
                        ),
                        padding: EdgeInsets.only(top: 20.0),
                      ),
                      Container(
                        child: InkWell(
                          onTap: () {
                            buttonList([
                              "RIM_ACCEPT",
                               "RIM_NEW",
                              "RIM_WIP",
                              "RIM_PDC",
                              "RIM_PA",
                              "RIM_FAULTPRST",
                              "RIM_TRNSIM",
                              "RIM_TOE",
                              "RIM_FAULTPR",
                              "RIM_TIM",
                              "RIM_TENG",
                              "RIM_PART_AUTH",
                              "RIM_FPR",
                              "RIM_FPR_SR",
                              "RIM_RTN_ENG",
                              "RIM_STD_REQ",
                              "RIM_FPR_STD_REQ",
                              "ENG_RAS",
                              "ENG_NEW",
                              "ENG_PFS",
                              "ENG_ACCEPT",
                              "ENG_SPAREQ",
                              "ENG_SPR_REQ",
                              "ENG_WIP",
                              "ENG_PDC",
                              "ENG_TRANSIM",
                              "ENG_TRANS_IM",
                              "ENG_TRNS_IM",
                              "ENG_WPS",
                              "PENG_RAS",
                              "PENG_SPAREQ",
                              "PENG_TRNS_PART",
                              "PENG_WPS",
                              "PENG_SPR_REQ",
                              "PENG_WIP",
                              "PENG_NEW",
                              "PENG_PDC"
                              "PENG_PFS",
                              "PENG_ACCEPT",
                              "PENG_TRANSIM",
                              "PENG_TRANS_IM",
                              "PENG_TRNS_IM",
                              "TRC_NEW",
                              "TRC_WIP",
                              "TRC_REPAIRED",
                              "TRC_NOT_REPAIRABLE",
                              "PART_WPS",
                              "PART_ASGN_ENGNR",
                              "PART_SPR_REQ",
                              "PART_ASGN_PENGNR",
                              "PART_ASGN_PRTNR",
                              "PART_WIP",
                              "PART_REJECT",
                              "PART_ACCEPT",
                              "PART_NEW",
                              "PART_PDC",
                                "IM_NEW",
                              "RIM_TIM",
                              "IM_WIP",
                              "IM_PDC",
                              "IM_ASGND_PRTNR",
                              "IM_ASGND_ENGNR",
                              "IM_SPART_REQ",
                              "IM_TRANSFER_RIM",
                              "IM_WPS",
                              "STR_NEW",
                              "STR_PDC",
                              "STR_UP",
                              "STR_WIP",
                              "STR_SPR_BP",
                              "STR_SPR_BP",
                              "STR_SPR_DSPTH",
                              "STR_SPR_DSPTH-ER",
                              "STR_STDBY_DSPTH",
                              "STR_FLTY_PFR",
                              "STR_TRNS_IM",
                              "STR_TRNS_TRC_RPR",
                              "STR_TRNS_PRTNR_RPR",
                              "STR_FLTY_NTBP",
                              "STR_TBD",
                              "STR_PKUP",
                              "STR_HOVR_PTR_RPR",
                              "STR_HOVR_TRC_RPR",
                              "STR_RESOLVED",
                              "STR_DEL_ER",
                              "STR_DEL_ENR",
                              "STR_REP",
                              "STR_UREP",
                              "STR_REP",
                              "STR_UREP"
                            ], filterType: "WIP");
                            Navigator.of(context).pop();
                          }, // Handle your callback
                          child: Text('WIP'),
                        ),
                        padding: EdgeInsets.only(top: 20.0),
                      ),
                      Container(
                        child: InkWell(
                          onTap: () {
                            buttonList(["ENG_PFS", "PENG_PFS"]);
                            Navigator.of(context).pop();
                          }, // Handle your callback
                          child: Text('Engineer on the way'),
                        ),
                        padding: EdgeInsets.only(top: 20.0),
                      ),
                      Container(
                        child: InkWell(
                          onTap: () {
                            buttonList(["ENG_ACCEPT", "PENG_ACCEPT"]);
                            Navigator.of(context).pop();
                          }, // Handle your callback
                          child: Text('Assigned To Engineer'),
                        ),
                        padding: EdgeInsets.only(top: 20.0),
                      ),
                      Container(
                        child: InkWell(
                          onTap: () {
                            print("OnHold Ticket");
                            buttonList([
                              "RIM_PDC",
                              "ENG_PDC",
                              "PENG_PDC",
                              "IM_PDC",
                              "STR_PDC",
                              "PART_PDC"
                            ]);
                            Navigator.of(context).pop();
                          }, // Handle your callback
                          child: Text('OnHold'),
                        ),
                        padding: EdgeInsets.only(top: 20.0),
                      ),
                      Container(
                        child: InkWell(
                          onTap: () {
                            print("Under Observation Ticket");

                            buttonList([
                              "RIM_UOB",
                              "RIM_UNDER_OBSERVATION",
                              "ENG_UOB",
                              "PENG_UOB",
                              "IM_OBS",
                              "PART_UOB"
                            ]);
                            Navigator.of(context).pop();
                          }, // Handle your callback
                          child: Text('Under Observation'),
                        ),
                        padding: EdgeInsets.only(top: 20.0),
                      ),
                      Container(
                        child: InkWell(
                          onTap: () {
                            print("Standby Provided Ticket");

                            buttonList([
                              "ENG_STDBY_INSTL",
                              "PENG_STDBY_INSTL",
                              "STR_NEW",
                              "Standby Delivered Faulty Picked up",
                              "Standby Delivered Faulty pending to pick",
                              "Standby Delivered",
                              "STR_STDBY_DLVR",
                              "STR_STDBY-DLVR_FP",
                              "Standby Delivered and Faulty Picked"
                            ]);
                            Navigator.of(context).pop();
                          }, // Handle your callback
                          child: Text('Standby Provided'),
                        ),
                        padding: EdgeInsets.only(top: 20.0),
                      ),
                      /*Container(
                        child: InkWell(
                          onTap: () {
                            print("Standby Given Ticket");

                            buttonList([

                            ]);
                            Navigator.of(context).pop();
                          }, // Handle your callback
                          child: Text('Standby Given'),
                        ),
                        padding: EdgeInsets.only(top: 20.0),
                      ),*/
                    ],
                  ),

                  //   ],
                  // ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    Color color1 = _colorFromHex("#00ABC5");

    return Scaffold(
      key: globalKey,
      resizeToAvoidBottomInset: false,
      // resizeToAvoidBottomPadding: false,
      // appBar: AppBar(centerTitle: false,
      //   title: Text("My Tickets Listing"),
      //   actions: <Widget>[
      //     FlatButton(
      //       textColor: Colors.white,
      //       onPressed: () {
      //         showDialog(
      //           context: context,
      //           builder: (context) {
      //             return Dialog(
      //               shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
      //               elevation: 16,
      //               child: Container(
      //                 width: 300,
      //                 height: 350,
      //                 padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
      //                 alignment: Alignment.centerLeft,
      //                 child: Column(
      //                   children: [
      //                     Align(
      //                         alignment: Alignment.center,
      //                         child: Text("Filter",
      //                           style: TextStyle(fontSize: 17.0, fontWeight: FontWeight.bold,
      //                               color: Color(0xFF919191)
      //                           ),
      //                         )
      //                     ),
      //                     new Container(
      //                       // child: Row(
      //                       //   children: <Widget>[
      //                       child: Column(children: [
      //
      //                         //                             Container(child: InkWell(
      //                         //
      //                         //
      //                         //
      //                         //
      //                         //                               // EasyLoading.showToast("No data availlable");
      //                         //
      //                         //
      //                         //
      //                         //               onTap: () {
      //                         // print("RiM new click");
      //                         //
      //                         // filteredUsers = users
      //                         //     .where((u) => (u.status.toLowerCase().contains("RIM_NEW".toLowerCase())) ||u.status.toString().contains("ENG_NEW")|| u.status.toString().contains("ENG_REJECT") )
      //                         //     .toList();
      //                         // // Navigator.pop(context);
      //                         //   if(filteredUsers.length==0){
      //                         //     Fluttertoast.showToast(
      //                         //         msg: "No Data Available",
      //                         //         toastLength: Toast.LENGTH_SHORT,
      //                         //         gravity: ToastGravity.CENTER,
      //                         //         timeInSecForIosWeb: 1,
      //                         //         backgroundColor: Colors.red,
      //                         //         textColor: Colors.white,
      //                         //         fontSize: 16.0
      //                         //     );
      //                         //   print("length"+filteredUsers.length.toString());
      //                         //
      //                         //   }
      //                         //   else{
      //                         //     setState(() {
      //                         //
      //                         // filteredUsers = users
      //                         //     .where((u) => (u.status.toLowerCase().contains("RIM_NEW".toLowerCase())) ||u.status.toString().contains("ENG_NEW")|| u.status.toString().contains("ENG_REJECT") )
      //                         //     .toList();
      //                         //   Navigator.pop(context);
      //                         //
      //                         //
      //                         // });
      //                         //     }
      //                         //   }
      //                         //
      //                         //                             child: Text('NEW'),
      //                         //
      //                         // // EasyLoading.showToast("No data availlable");
      //                         //                 ),
      //                         //                             padding: EdgeInsets.only(top: 15.0),) ,
      //
      //
      //
      //                         Container(child:InkWell(
      //                           onTap: () {
      //                             print("eng accept new click");
      //                             filteredUsers = users
      //                                 .where((u) => (u.status.toString().contains("RIM_NEW")) ||u.status.toString().contains("ENG_NEW")|| u.status.toString().contains("ENG_REJECT") )
      //                                 .toList();
      //
      //                             if(filteredUsers.length==0){
      //                               Fluttertoast.showToast(
      //                                   msg: "No Data Available",
      //                                   toastLength: Toast.LENGTH_SHORT,
      //                                   gravity: ToastGravity.CENTER,
      //                                   timeInSecForIosWeb: 1,
      //                                   backgroundColor: Colors.red,
      //                                   textColor: Colors.white,
      //                                   fontSize: 16.0
      //                               );
      //                             }
      //                             else{
      //                               setState(() {
      //                                 filteredUsers = users
      //                                     .where((u) => (u.status.toString().contains("RIM_NEW")) ||u.status.toString().contains("ENG_NEW")|| u.status.toString().contains("ENG_REJECT") )
      //                                     .toList();
      //                                 Navigator.pop(context);
      //                               });
      //                             }
      //
      //
      //
      //                           }, // Handle your callback
      //                           child:  Text('NEW'),
      //                         ),  padding: EdgeInsets.only(top: 15.0),),
      //
      //
      //
      //
      //                         Container(child:InkWell(
      //                           onTap: () {
      //                             print("eng accept new click");
      //                             filteredUsers = users
      //                                 .where((u) => (u.status.toString().contains("ENG_ACCEPT")  ||  u.status.toString().contains("IM_ASGND_PRTNR") ||u.status.toString().contains("PENG_ACCEPT") ))
      //                                 .toList();
      //
      //                             if(filteredUsers.length==0){
      //                               Fluttertoast.showToast(
      //                                   msg: "No Data Available",
      //                                   toastLength: Toast.LENGTH_SHORT,
      //                                   gravity: ToastGravity.CENTER,
      //                                   timeInSecForIosWeb: 1,
      //                                   backgroundColor: Colors.red,
      //                                   textColor: Colors.white,
      //                                   fontSize: 16.0
      //                               );
      //                             }
      //                             else{
      //                               setState(() {
      //                                 print("accept"+filteredUsers.length.toString());
      //
      //                                 filteredUsers = users
      //                                     .where((u) => (u.status.toString().contains("ENG_ACCEPT")  ||  u.status.toString().contains("IM_ASGND_PRTNR") ||u.status.toString().contains("PENG_ACCEPT") ))
      //                                     .toList();
      //                                 print("accept"+filteredUsers.length.toString());
      //
      //                                 Navigator.pop(context);
      //                               });
      //                             }
      //
      //                             print("accept"+filteredUsers.length.toString());
      //
      //
      //                           }, // Handle your callback
      //                           child:  Text('Assign to Agent'),
      //                          ),  padding: EdgeInsets.only(top: 15.0),),
      //
      //
      //
      //                         Container(child:InkWell(
      //                           onTap: () {
      //                             print("in journey new click");
      //                             filteredUsers = users
      //                                 .where((u) => (u.status.toString().contains("ENG_PFS")) || u.status.toString().contains("PENG_PFS"))
      //                                 .toList();
      //
      //                             if(filteredUsers.length==0){
      //                               Fluttertoast.showToast(
      //                                   msg: "No Data Available",
      //                                   toastLength: Toast.LENGTH_SHORT,
      //                                   gravity: ToastGravity.CENTER,
      //                                   timeInSecForIosWeb: 1,
      //                                   backgroundColor: Colors.red,
      //                                   textColor: Colors.white,
      //                                   fontSize: 16.0
      //                               );
      //                             }
      //                             else{
      //                               setState(() {
      //                                 filteredUsers = users
      //                                     .where((u) => (u.status.toString().contains("ENG_PFS")) || u.status.toString().contains("PENG_PFS"))
      //                                     .toList();
      //                                 Navigator.pop(context);
      //                               });
      //                             }
      //
      //
      //                           }, // Handle your callback
      //                           child:  Text('Engineer on the way'),
      //                         ),  padding: EdgeInsets.only(top: 15.0),),
      //
      //
      //
      //
      //                         Container(child:InkWell(
      //                           onTap: () {
      //                             print("wip new click");
      //
      //
      //
      //                             //
      //                             // users.forEach((userDetail) {
      //                             //
      //                             //   userDetail.status.contains("ENG_WIP")|| userDetail.status.contains("ENG_RAS")|| userDetail.status.contains("ENG_SPAREQ")|| userDetail.status.contains("ENG_WIP") ||
      //                             //       userDetail.status.contains("RIM_WIP") || userDetail.status.contains("RIM_TRNSIM") || userDetail.status.contains("RIM_ACCEPT") || userDetail.status.contains("ENG_TRANSIM") ||
      //                             //       userDetail.status.contains("RIM_PA") ||  userDetail.status.contains("RIM_TOE") ||  userDetail.status.contains("RIM_FAULTPR") ||  userDetail.status.contains("RIM_FAULTPRST") ||
      //                             //       userDetail.status.contains("STR_NEW") || userDetail.status.contains("STR_WIP") || userDetail.status.contains("STR_SPR_BP") || userDetail.status.contains("STR_SPR_DSPTH-ER") ||
      //                             //       userDetail.status.contains("STR_SPR_DSPTH") ||     userDetail.status.contains("STR_FLTY_PFR") || userDetail.status.contains("STR_TRNS_IM") ||   userDetail.status.contains("STR_TRNS_TRC_RPR") ||
      //                             //       userDetail.status.contains("STR_TRNS_PRTNR_RPR") || userDetail.status.contains("STR_SPR_DSPTH") || userDetail.status.contains("STR_FLTY_NTBP") ||  userDetail.status.contains("PART_NEW") ||
      //                             //       userDetail.status.contains("IM_WIP") || userDetail.status.contains("PART_ASGN_PRTNR") || userDetail.status.contains("PART_ASGN_ENGNR") ||  userDetail.status.contains("TRC_NOT_REPAIRABLE") || userDetail.status.contains("TRC_REPAIRED") ||
      //                             //       userDetail.status.contains("TRC_NEW") || userDetail.status.contains("RIM_TIM")|| userDetail.status.contains("RIM_PART_AUTH") || userDetail.status.contains("RIM_FPR"));
      //                             //       filteredUsers.add(userDetail);
      //                             // });
      //                             // print("check length" + filteredUsers.length.toString());
      //                             // setState(() {
      //                             //
      //                             // });
      //
      //
      //
      //
      //
      //
      //                             filteredUsers = users
      //                                 .where((u) => (u.status.toString().contains("ENG_WIP".toString()) || u.status.toString().contains("ENG_RAS".toString()) ||  u.status.toString().contains("ENG_SPAREQ".toString()) || u.status.toString().contains("ENG_WIP".toString()) || u.status.toString().contains("ENG_TRANSIM".toString()) || u.status.toString().contains("RIM_ACCEPT".toString()) ||
      //                                 u.status.toString().contains("RIM_PA".toString())||  u.status.toString().contains("RIM_WIP".toString()) ||  u.status.toString().contains("RIM_TRNSIM".toString()) ||
      //                                 u.status.toString().contains("RIM_TOE".toString()) ||   u.status.toString().contains("RIM_FAULTPR".toString())  ||   u.status.toString().contains("RIM_FAULTPRST".toString()) ||
      //                                 u.status.toString().contains("STR_NEW".toString())  ||   u.status.toString().contains("STR_WIP".toString())  ||    u.status.toString().contains("STR_SPR_BP".toString())  ||
      //                                 u.status.toString().contains("STR_SPR_DSPTH".toString()) ||     u.status.toString().contains("STR_SPR_DSPTH-ER".toString()) ||
      //                                 u.status.toString().contains("STR_FLTY_PFR".toString())   ||   u.status.toString().contains("STR_TRNS_IM".toString())  ||   u.status.toString().contains("STR_TRNS_TRC_RPR".toString()) ||
      //                                 u.status.toString().contains("STR_TRNS_PRTNR_RPR".toString()) ||  u.status.toString().contains("STR_FLTY_NTBP".toString()) ||
      //                                  u.status.toString().contains("IM_WIP".toString()) ||  u.status.toString().contains("PART_NEW".toString()) ||
      //                                 u.status.toString().contains("PART_WIP".toString()) ||   u.status.toString().contains("PART_ASGN_PRTNR".toString()) ||  u.status.toString().contains("PART_ASGN_ENGNR".toString()) ||
      //                                 u.status.toString().contains("TRC_NEW".toString()) ||   u.status.toString().contains("TRC_WIP".toString()) ||   u.status.toString().contains("TRC_REPAIRED".toString()) ||  u.status.toString().contains("TRC_NOT_REPAIRABLE".toString())
      //                                 ||   u.status.toString().contains("RIM_TIM".toString()) ||
      //                                 u.status.toString().contains("RIM_TENG".toString()) ||   u.status.toString().contains("RIM_PART_AUTH".toString()) ||  u.status.toString().contains("RIM_FPR".toString()) ||  u.status.toString().contains("RIM_FPR_SR".toString())||
      //                                 u.status.toString().startsWith("IM_NEW".toString()) ||  u.status.toString().startsWith("RIM_RTN_ENG".toString()) ||   u.status.toString().startsWith("RIM_STD_REQ".toString())||
      //                                 u.status.toString().startsWith("RIM_FPR_STD_REQ".toString()) ||   u.status.toString().startsWith("STR_UP".toString()) ||   u.status.toString().startsWith("STR_TBD".toString())
      //                                 ||  u.status.toString().startsWith("STR_PKUP".toString()) ||  u.status.toString().startsWith("STR_DEL_ER".toString()) ||
      //                                 u.status.toString().startsWith("STR_HOVR_TRC_RPR".toString()) ||  u.status.toString().startsWith("STR_HOVR_PTR_RPR".toString()) ||
      //                                 u.status.toString().startsWith("STR_REP".toString()) ||  u.status.toString().startsWith("STR_UREP".toString()) ||
      //                                 u.status.toString().startsWith("STR_REP".toString()) ||   u.status.toString().startsWith("IM_SPART_REQ".toString())||
      //                                 u.status.toString().startsWith("IM_TRANSFER_RIM".toString()) ||  u.status.toString().startsWith("IM_WPS".toString()) || u.status.toString().startsWith("ENG_SPR_REQ".toString()) ||
      //                                 u.status.toString().startsWith("ENG_TRNS_IM".toString()) ||   u.status.toString().startsWith("ENG_WPS".toString()) ||
      //                                 u.status.toString().startsWith("PART_ACCEPT".toString()) ||  u.status.toString().startsWith("PART_REJECT".toString()) ||
      //                                 u.status.toString().startsWith("PART_SPR_REQ".toString())||  u.status.toString().startsWith("PART_ASGN_PENGNR".toString()) ||
      //                                 u.status.toString().startsWith("PART_WPS".toString()) || u.status.toString().startsWith("PENG_RAS".toString()) ||  u.status.toString().startsWith("PENG_WIP".toString()) ||
      //                                 u.status.toString().startsWith("PENG_SPR_REQ".toString()) ||  u.status.toString().startsWith("PENG_TRNS_PART".toString()) ||
      //                                 u.status.toString().startsWith("PENG_WPS".toString()) ||  u.status.toString().contains("STR_DEL_ENR") || u.status.toString().contains("IM_ASGND_ENGNR")
      //                             ))
      //                                 .toList();
      //                             print("wipbefore"+filteredUsers.length.toString());
      //                             setState(() {
      //                               if(filteredUsers.length==0){
      //
      //                                 Fluttertoast.showToast(
      //                                     msg: "No Data Available",
      //                                     toastLength: Toast.LENGTH_SHORT,
      //                                     gravity: ToastGravity.CENTER,
      //                                     timeInSecForIosWeb: 1,
      //                                     backgroundColor: Colors.red,
      //                                     textColor: Colors.white,
      //                                     fontSize: 16.0
      //                                 );
      //                               }
      //                               else{
      //                                 setState(() {
      //                                   print("wipac"+filteredUsers.length.toString());
      //                                   filteredUsers = users
      //                                       .where((u) => (u.status.toString().contains("ENG_WIP".toString()) || u.status.toString().contains("ENG_RAS".toString()) ||  u.status.toString().contains("ENG_SPAREQ".toString()) || u.status.toString().contains("ENG_WIP".toString()) || u.status.toString().contains("ENG_TRANSIM".toString()) || u.status.toString().contains("RIM_ACCEPT".toString()) ||
      //                                       u.status.toString().contains("RIM_PA".toString())||  u.status.toString().contains("RIM_WIP".toString()) ||  u.status.toString().contains("RIM_TRNSIM".toString()) ||
      //                                       u.status.toString().contains("RIM_TOE".toString()) ||   u.status.toString().contains("RIM_FAULTPR".toString())  ||   u.status.toString().contains("RIM_FAULTPRST".toString()) ||
      //                                       u.status.toString().contains("STR_NEW".toString())  ||   u.status.toString().contains("STR_WIP".toString())  ||    u.status.toString().contains("STR_SPR_BP".toString())  ||
      //                                       u.status.toString().contains("STR_SPR_DSPTH".toString()) ||     u.status.toString().contains("STR_SPR_DSPTH-ER".toString()) ||
      //                                       u.status.toString().contains("STR_FLTY_PFR".toString())   ||   u.status.toString().contains("STR_TRNS_IM".toString())  ||   u.status.toString().contains("STR_TRNS_TRC_RPR".toString()) ||
      //                                       u.status.toString().contains("STR_TRNS_PRTNR_RPR".toString()) ||  u.status.toString().contains("STR_FLTY_NTBP".toString()) ||
      //                                       u.status.toString().contains("IM_WIP".toString()) ||  u.status.toString().contains("PART_NEW".toString()) ||
      //                                       u.status.toString().contains("PART_WIP".toString()) ||   u.status.toString().contains("PART_ASGN_PRTNR".toString()) ||  u.status.toString().contains("PART_ASGN_ENGNR".toString()) ||
      //                                       u.status.toString().contains("TRC_NEW".toString()) ||   u.status.toString().contains("TRC_WIP".toString()) ||   u.status.toString().contains("TRC_REPAIRED".toString()) ||  u.status.toString().contains("TRC_NOT_REPAIRABLE".toString())
      //                                       ||   u.status.toString().contains("RIM_TIM".toString()) ||
      //                                       u.status.toString().contains("RIM_TENG".toString()) ||   u.status.toString().contains("RIM_PART_AUTH".toString()) ||  u.status.toString().contains("RIM_FPR".toString()) ||  u.status.toString().contains("RIM_FPR_SR".toString())||
      //                                       u.status.toString().startsWith("IM_NEW".toString()) ||  u.status.toString().startsWith("RIM_RTN_ENG".toString()) ||   u.status.toString().startsWith("RIM_STD_REQ".toString())||
      //                                       u.status.toString().startsWith("RIM_FPR_STD_REQ".toString()) ||   u.status.toString().startsWith("STR_UP".toString()) ||   u.status.toString().startsWith("STR_TBD".toString())
      //                                       ||  u.status.toString().startsWith("STR_PKUP".toString()) ||  u.status.toString().startsWith("STR_DEL_ER".toString()) ||
      //                                       u.status.toString().startsWith("STR_HOVR_TRC_RPR".toString()) ||  u.status.toString().startsWith("STR_HOVR_PTR_RPR".toString()) ||
      //                                       u.status.toString().startsWith("STR_REP".toString()) ||  u.status.toString().startsWith("STR_UREP".toString()) ||
      //                                       u.status.toString().startsWith("STR_REP".toString()) ||   u.status.toString().startsWith("IM_SPART_REQ".toString())||
      //                                       u.status.toString().startsWith("IM_TRANSFER_RIM".toString()) ||  u.status.toString().startsWith("IM_WPS".toString()) || u.status.toString().startsWith("ENG_SPR_REQ".toString()) ||
      //                                       u.status.toString().startsWith("ENG_TRNS_IM".toString()) ||   u.status.toString().startsWith("ENG_WPS".toString()) ||
      //                                       u.status.toString().startsWith("PART_ACCEPT".toString()) ||  u.status.toString().startsWith("PART_REJECT".toString()) ||
      //                                       u.status.toString().startsWith("PART_SPR_REQ".toString())||  u.status.toString().startsWith("PART_ASGN_PENGNR".toString()) ||
      //                                       u.status.toString().startsWith("PART_WPS".toString()) || u.status.toString().startsWith("PENG_RAS".toString()) ||  u.status.toString().startsWith("PENG_WIP".toString()) ||
      //                                       u.status.toString().startsWith("PENG_SPR_REQ".toString()) ||  u.status.toString().startsWith("PENG_TRNS_PART".toString()) ||
      //                                       u.status.toString().startsWith("PENG_WPS".toString()) ||  u.status.toString().contains("STR_DEL_ENR") || u.status.toString().contains("IM_ASGND_ENGNR")
      //                                   ))
      //                                       .toList();
      //                                   print("wipbetween"+filteredUsers.length.toString());F
      //
      //                                   Navigator.pop(context);
      //                                 });
      //
      //                                 // u.status.toLowerCase().contains("IM_NEW".toLowerCase()) ||
      //
      //                               }
      //
      //                             });
      //                             print("wipafter"+filteredUsers.length.toString());
      //                           }, // Handle your callback
      //                           child:  Text('WIP'),
      //                         ),  padding: EdgeInsets.only(top: 15.0),),
      //
      //
      //
      //                         Container(child: InkWell(
      //                           onTap: () {
      //                             print("uob new click");
      //
      //                             filteredUsers = users
      //                                 .where((u) => (u.status.toString().contains("ENG_UOB") || u.status.toString().contains("RIM_UOB") || u.status.toString().contains("RIM_UNDER_OBSERVATION") || u.status.toString().contains("IM_OBS") ||
      //                                 u.status.toString().contains("PART_UOB") ||  u.status.toString().contains("PENG_UOB")))
      //                                 .toList();
      //
      //                             if(filteredUsers.length==0){
      //                               Fluttertoast.showToast(
      //                                   msg: "No Data Available",
      //                                   toastLength: Toast.LENGTH_SHORT,
      //                                   gravity: ToastGravity.CENTER,
      //                                   timeInSecForIosWeb: 1,
      //                                   backgroundColor: Colors.red,
      //                                   textColor: Colors.white,
      //                                   fontSize: 16.0
      //                               );
      //                             }
      //                             else{
      //
      //                               setState(() {
      //                                 filteredUsers = users
      //                                     .where((u) => (u.status.toString().contains("ENG_UOB") || u.status.toString().contains("RIM_UOB") || u.status.toString().contains("RIM_UNDER_OBSERVATION") || u.status.toString().contains("IM_OBS") ||
      //                                     u.status.toString().contains("PART_UOB") ||  u.status.toString().contains("PENG_UOB")))
      //                                     .toList();
      //                                 Navigator.pop(context);
      //                               });
      //
      //                             }
      //
      //
      //
      //
      //                           }, // Handle your callback
      //                           child:  Text('UnderObservation'),
      //                         ),
      //
      //                           padding: EdgeInsets.only(top: 15.0),),
      //
      //                         Container( child: InkWell(
      //                           onTap: () {
      //                             print("resplved new click");
      //                             filteredUsers = users
      //                                 .where((u) => (u.status.toString().contains("ENG_RESOLVED") || u.status.toString().contains("RIM_RESOLVED") || u.status.toString().contains("STR_SPR_DLVR-R") || u.status.toString().contains("STR_STDBY_PICK") || u.status.toString().contains("STR_REPD_NTBP") || u.status.toString().contains("IM_RESOLVED") || u.status.toString().contains("ENG_RESO") ||
      //                                   u.status.toString().contains("STR_RESOLVED") || u.status.toString().contains("PART_RESOLVED")  ||  u.status.toString().contains("PENG_RESO")
      //                             ))
      //                                 .toList();
      //                             if(filteredUsers.length==0){
      //                               Fluttertoast.showToast(
      //                                   msg: "No Data Available",
      //                                   toastLength: Toast.LENGTH_SHORT,
      //                                   gravity: ToastGravity.CENTER,
      //                                   timeInSecForIosWeb: 1,
      //                                   backgroundColor: Colors.red,
      //                                   textColor: Colors.white,
      //                                   fontSize: 16.0
      //                               );
      //                             }
      //                             else{
      //
      //                               setState(() {
      //                                 filteredUsers = users
      //                                     .where((u) => (u.status.toString().contains("ENG_RESOLVED") || u.status.toString().contains("RIM_RESOLVED") || u.status.toString().contains("STR_SPR_DLVR-R") || u.status.toString().contains("STR_STDBY_PICK") || u.status.toString().contains("STR_REPD_NTBP") || u.status.toString().contains("IM_RESOLVED") || u.status.toString().contains("ENG_RESO") ||
      //                                      u.status.toString().contains("STR_RESOLVED") || u.status.toString().contains("PART_RESOLVED")  ||  u.status.toString().contains("PENG_RESO")
      //                                 ))
      //                                     .toList();
      //                                 Navigator.pop(context);
      //                               });
      //                             }
      //                           }, // Handle your callback
      //                           child:  Text('Resolved'),
      //                         ),  padding: EdgeInsets.only(top: 15.0),),
      //
      //                         Container( child: InkWell(
      //                           onTap: () {
      //                             print("hold new click");
      //                             filteredUsers = users
      //                                 .where((u) => (u.status.toString().startsWith("ENG_PDC") || u.status.toString().startsWith("RIM_PDC") || u.status.toString().startsWith("IM_PDC") || u.status.toString().startsWith("PART_PDC") ||
      //                                 u.status.toString().startsWith("STR_PDC") ||  u.status.toString().startsWith("PART_PDC") || u.status.toString().startsWith("PENG_PDC")))
      //                                 .toList();
      //
      //                             if(filteredUsers.length==0){
      //                               Fluttertoast.showToast(
      //                                   msg: "No Data Available",
      //                                   toastLength: Toast.LENGTH_SHORT,
      //                                   gravity: ToastGravity.CENTER,
      //                                   timeInSecForIosWeb: 1,
      //                                   backgroundColor: Colors.red,
      //                                   textColor: Colors.white,
      //                                   fontSize: 16.0
      //                               );
      //                             }
      //                             else{
      //
      //                               setState(() {
      //                                 filteredUsers = users
      //                                     .where((u) => (u.status.toString().startsWith("ENG_PDC") || u.status.toString().startsWith("RIM_PDC") || u.status.toString().startsWith("IM_PDC") || u.status.toString().startsWith("PART_PDC") ||
      //                                     u.status.toString().startsWith("STR_PDC") ||  u.status.toString().startsWith("PART_PDC") || u.status.toString().startsWith("PENG_PDC")))
      //                                     .toList();
      //                                 Navigator.pop(context);
      //                               });
      //                             }
      //
      //                           }, // Handle your callback
      //                           child:  Text('Hold'),
      //                         ),  padding: EdgeInsets.only(top: 15.0),),
      //
      //
      //                         Container( child: InkWell(
      //                           onTap: () {
      //                             print("stand new click");
      //                             filteredUsers = users
      //                                 .where((u) => (u.status.toString().contains("Standby Delivered") || u.status.toString().contains("Standby Delivered and Faulty Picked") || u.status.toString().contains("STR_STDBY_DLVR") || u.status.toString().contains("STR_STDBY-DLVR_FP")
      //                                 ||u.status.toString().contains("ENG_STDBY_INSTL") || u.status.toString().contains("PENG_STDBY_INSTL")))
      //                                 .toList();
      //                             if(filteredUsers.length==0){
      //                               Fluttertoast.showToast(
      //                                   msg: "No Data Available",
      //                                   toastLength: Toast.LENGTH_SHORT,
      //                                   gravity: ToastGravity.CENTER,
      //                                   timeInSecForIosWeb: 1,
      //                                   backgroundColor: Colors.red,
      //                                   textColor: Colors.white,
      //                                   fontSize: 16.0
      //                               );
      //                             }
      //                             else{
      //                               setState(() {
      //                                 filteredUsers = users
      //                                     .where((u) => (u.status.toString().contains("Standby Delivered") || u.status.toString().contains("Standby Delivered and Faulty Picked") || u.status.toString().contains("STR_STDBY_DLVR") || u.status.toString().contains("STR_STDBY-DLVR_FP")
      //                                 ||u.status.toString().contains("ENG_STDBY_INSTL") || u.status.toString().contains("PENG_STDBY_INSTL")))
      //                                     .toList();
      //                                 Navigator.pop(context);
      //                               });
      //                             }
      //
      //                           }, // Handle your callback
      //                           child:  Text('Stand By Given'),
      //                         ),  padding: EdgeInsets.only(top: 15.0),),
      //
      //
      //
      //
      //
      //
      //
      //                         Container(child:InkWell(
      //                           onTap: () {
      //                             print("all new click");
      //                             Services.getUsers().then((usersFromServer) {
      //                               setState(() {
      //                                 users = usersFromServer.cast<Article>();
      //                                 filteredUsers = users;
      //                                 Navigator.pop(context);
      //                               });
      //                             });
      //                           }, // Handle your callback
      //                           child:  Text('ALL'
      //                           ),
      //                         ),  padding: EdgeInsets.only(top: 15.0),),
      //                       ],),
      //                       //   ],
      //                       // ),
      //                     ),
      //                   ],
      //                 ),
      //               ),
      //             );
      //           },
      //         );
      //       },
      //       child: Image(image: AssetImage('assets/filter_icon.png'),) ,
      //       shape: CircleBorder(side: BorderSide(color: Colors.transparent)),
      //     ),
      //   ],
      //   flexibleSpace: Image(
      //     image: AssetImage('assets/appbar_background.png'),
      //     fit: BoxFit.cover,
      //   ),
      //   backgroundColor: Colors.transparent,),
      body: RefreshIndicator(
        onRefresh: _pullRefresh,
        child: SingleChildScrollView(
          child: Column(children: <Widget>[
            Container(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Align(
                      alignment: Alignment.topLeft,
                      child: Container(
                        width: MediaQuery.of(context).size.width * 0.80,
                        height: 45,
                        child: TextField(
                          inputFormatters: <TextInputFormatter>[
                            FilteringTextInputFormatter.allow(
                                RegExp("[0-9a-zA-Z]")),
                          ],
                          decoration: InputDecoration(
                              contentPadding: EdgeInsets.all(15.0),
                              hintText: 'Search by Ticket Id & Asset Type',
                              hintStyle: TextStyle(fontSize: 14.0),
                              prefixIcon: Icon(Icons.search),
                              border: OutlineInputBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(25.0)),
                              )),
                          onChanged: (string) {
                            _debouncer.run(() {
                              setState(() {
                                filteredUsers = users
                                    .where((u) => (u.assetType
                                            .toLowerCase()
                                            .contains(string.toLowerCase()) ||
                                        u.ticketSerialNo
                                            .toLowerCase()
                                            .contains(string.toLowerCase())))
                                    .toList();
                              });
                            });
                          },
                        ),
                      ),
                    ),
                    Flexible(
                      child: SizedBox(
                        width: MediaQuery.of(context).size.width * 0.15,
                        child: IconButton(
                          icon: Icon(Icons.filter_alt_rounded),
                          // textColor: Colors.white,
                          onPressed: () {
                            print("Its working");
                            showFilterDialog();
                          },
                          // child:
                          // shape: CircleBorder(side: BorderSide(color: Colors.transparent)),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Container(
                padding: EdgeInsets.fromLTRB(20, 20, 20, 0),
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Expanded(
                      flex: 1,
                      child: Text("Ticket Id.",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontSize: 12.0,
                              fontWeight: FontWeight.bold,
                              color: Colors.black)),
                    ),

                    // SizedBox(width: 8,),
                    Expanded(
                        flex: 1,
                        child: Text("Asset Type",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontSize: 12.0,
                                fontWeight: FontWeight.bold,
                                color: Colors.black))),

                    // SizedBox(width: 20,),
                    Expanded(
                      flex: 1,
                      child: Text("Date",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontSize: 12.0,
                              fontWeight: FontWeight.bold,
                              color: Colors.black)),
                    ),

                    Expanded(
                      flex: 1,
                      child: Align(
                        child: Text("Status",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontSize: 12.0,
                                fontWeight: FontWeight.bold,
                                color: Colors.black)),
                      ),
                    ),
                  ],
                )),
            _buildList(),
          ]),
        ),
      ),
    );
  }

  String formatDate(String date) {
    DateTime tempDate = new DateFormat("yyyy-MM-dd hh:mm:ss").parse(date);
  }

  assetType(assettype) {
    var at = toBeginningOfSentenceCase(assettype);
    return at;
  }

  date(date) {
    var newDate = DateFormat('dd-MM-yyyy').format(date);
    return newDate;
  }

  _status(status, isStandByInstall, lastRemark, isPartRequested, bool isNewTicket) {
  //  print("data chake");
    // print("data check Abhi" + isNewTicket.toString());
    // print("BB==> " + status + " ==> " + lastRemark + " ==> " + lastStatus.toString() + " ==> " + isPartRequested);
    if (status == "RIM_NEW") {
      /* if (isPartRequested == "1") {
        return 'WIP';
      } else*/
      if (isNewTicket) {
        return 'NEW';
      } else if (!isNewTicket) {
        return 'WIP';
      }
      else {
        return 'NEW';
      }

      // _getColorByEvent('New');
    } else if (status == "RIM_ACCEPT") {
      // _getColorByEvent('WIP');
      return 'WIP';
    } else if (status == "RIM_WIP") {
      // _getColorByEvent('WIP');
      // "RIM_ACCEPT","RIM_WIP"
      return "WIP";
    } else if (status == "RIM_UOB") {
      //_getColorByEvent('Under Observation');
      return "Under Observation";
    } else if (status == "RIM_PA") {
      //_getColorByEvent('WIP');
      // "RIM_ACCEPT","RIM_WIP","RIM_PA"
      return "WIP";
    } else if (status == "RIM_PDC") {
      //_getColorByEvent('HOLD');
      // if(isNewTicket)
      // {
      //   return "On HOLD";
      // }else if(!isNewTicket){
      //   return "WIP";
      // }
      return "On HOLD";
    } else if (status == "RIM_FAULTPRST") {
      //_getColorByEvent('WIP');
      // "RIM_ACCEPT","RIM_WIP","RIM_PA","RIM_FAULTPRST"
      return "WIP";
    } else if (status == "RIM_RESOLVED") {
      //_getColorByEvent('RESOLVED');
      return "RESOLVED";
    } else if (status == "RIM_TRNSIM") {
      //_getColorByEvent('WIP');

      return "WIP";
    } else if (status == "RIM_TOE") {
      //_getColorByEvent('WIP');

      return "WIP";
    } else if (status == "RIM_FAULTPR") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "RIM_UNDER_OBSERVATION") {
      //"RIM_UOB","RIM_UNDER_OBSERVATION"
      return "Under Observation";
    } else if (status == "RIM_TIM") {
      return "WIP";
    } else if (status == "RIM_TENG") {
      return "WIP";
    } else if (status == "RIM_PART_AUTH") {
      return "WIP";
    } else if (status == "RIM_FPR") {
      return "WIP";
    } else if (status == "RIM_FPR_SR") {
      return "WIP";
    } else if (status == "RIM_RTN_ENG") {
      return "WIP";
    } else if (status == "RIM_STD_REQ") {
      return "WIP";
    } else if (status == "RIM_FPR_STD_REQ") {
      return "WIP";
    }
    if (status == "ENG_NEW") {
      if (isNewTicket) {
        return 'NEW';
      } else if (!isNewTicket) {
        return 'WIP';
      }
      return 'NEW';
    }
    //_getColorByEvent('New');
    // if(lastStatus != null && lastStatus == "IM_TRANSFER_RIM"){
    //   return 'WIP';
    // }// 34278
    //  else{
    //  }

    else if (status == "ENG_ACCEPT") {
      //_getColorByEvent('Assign to Aggent');
      if(isNewTicket)
      {
        return "Assign to Engineer";
      }else if(!isNewTicket){
        return "WIP";
      }
      return 'Assigned to Engineer';
    } else if (status == "ENG_REJECT") {
      //_getColorByEvent('New');
      return "NEW";
    } else if (status == "ENG_PFS") {
      //_getColorByEvent('Aggent on the way');
      // if(isNewTicket)
      // {
      //   return "Engineer on the way";
      // }else if(!isNewTicket){
      //   return "WIP";
      // }
      return "Engineer on the way";
    } else if (status == "ENG_RAS") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "ENG_RESOLVED") {
      //_getColorByEvent('RESOLVED');
      return "RESOLVED";
    } else if (status == "ENG_UOB") {
      //_getColorByEvent('WIP');
      //"RIM_UOB","RIM_UNDER_OBSERVATION","ENG_UOB"
      return "Under Observation";
    } else if (status == "ENG_SPAREQ") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "ENG_SPR_REQ") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "ENG_WIP") {
      //_getColorByEvent('WIP');

      return "WIP";
    } else if (status == "ENG_PDC") {
      //_getColorByEvent('WIP');
      //"RIM_PDC","ENG_PDC"
      // if(isNewTicket)
      // {
      //   return "On HOLD";
      // }else if(!isNewTicket){
      //   return "WIP";
      // }
      return "On Hold";
    } else if (status == "ENG_RESO") {
      //_getColorByEvent('WIP');
      return "RESOLVED";
    } else if (status == "ENG_TRANSIM") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "ENG_TRANS_IM") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "ENG_TRNS_IM") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "ENG_WPS") {
      //_getColorByEvent('WIP');

      return "WIP";
    }

    if (status == "PENG_NEW") {
      if (isNewTicket) {
        return 'NEW';
      } else if (!isNewTicket) {
        return 'WIP';
      }
      return 'NEW';
    } else if (status == "PENG_ACCEPT") {
      //_getColorByEvent('Assign to Aggent');
      // "ENG_ACCEPT","PENG_ACCEPT"
      if(isNewTicket)
      {
        return "Assign to Engineer";
      }else if(!isNewTicket){
        return "WIP";
      }
      return 'Assign to Engineer';
    } else if (status == "PENG_REJECT") {
      //_getColorByEvent('New');
      return "NEW";
    } else if (status == "PENG_PFS") {
      // "ENG_PFS","PENG_PFS"
      //_getColorByEvent('Aggent on the way');
      // if(isNewTicket)
      // {
      //   return "Engineer on the way";
      // }else if(!isNewTicket){
      //   return "WIP";
      // }
      return "Engineer on the way";
    } else if (status == "PENG_RAS") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "PENG_RESOLVED") {
      //_getColorByEvent('RESOLVED');
      return "RESOLVED";
    } else if (status == "PENG_UOB") {
      //_getColorByEvent('WIP');
      //"RIM_UOB","RIM_UNDER_OBSERVATION","ENG_UOB","PENG_UOB"
      return "Under Observation";
    } else if (status == "PENG_SPAREQ") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "PENG_TRNS_PART") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "PENG_WPS") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "PENG_SPR_REQ") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "PENG_WIP") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (isStandByInstall == "1" && status=="STR_NEW") {
      //_getColorByEvent('WIP');

      return 'Standby Provided';
      /*else{
        return "WIP";
      }*/

      /* if (isStandby == "1") {
        return 'Standby Provided';
      } elseelse if(!isStandby==0){
        return "WIP";
      }*/

      /*if(isNewTicket)
      {
        return "On HOLD";
      }else if(!isNewTicket){
        return "WIP";
      }*/

      return "Standby Provided";
     // return "WIP";
    } else if (status == "PENG_PDC") {
      //_getColorByEvent('WIP');
      //"RIM_PDC","ENG_PDC","PENG_PDC"
      // if(isNewTicket)
      // {
      //   return "On HOLD";
      // }else if(!isNewTicket){
      //   return "WIP";
      // }
      return "On Hold";
    } else if (status == "PENG_RESO") {
      //_getColorByEvent('WIP');
      return "RESOLVED";
    } else if (status == "PENG_STDBY_INSTL") {
      //_getColorByEvent('WIP');

      /*if (isStandByInstall == "1" && status=='STR_NEW') {
        return 'Standby Provided';
      } else{
        return "WIP";
      }*/

      /* if (isStandby == "1" && status=='STR_NEW') {
        return 'Standby Provided';
      } else{
        return "WIP";
      }*/

       return "Standby Provided";
      //return "WIP";
    } else if (status == "PENG_TRANSIM") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "PENG_TRANS_IM") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "PENG_TRNS_IM") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "Standby Delivered") {
      //_getColorByEvent('WIP');
      //"Standby Delivered","STR_STDBY_DLVR","STR_STDBY-DLVR_FP","Standby Delivered and Faulty Picked"
      // return "Standby Given";
       return "Standby Provided";
     // return "WIP";
    } else if (status == "Standby Delivered and Faulty Picked") {
      //_getColorByEvent('WIP');
      // return "Standby Given";
      return "Standby Provided";
    //  return "WIP";
    } else if (status == "IM_NEW") {
      if (isNewTicket) {
        return 'NEW';
      } else if (!isNewTicket) {
        return 'WIP';
      }
      return "WIP";
    } else if (status == "RIM_TIM") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "IM_WIP") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "IM_ASGND_PRTNR") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "IM_ASGND_ENGNR") {
      //_getColorByEvent('WIP');

      return "WIP";
    } else if (status == "IM_PDC") {
      //_getColorByEvent('WIP');
      //"RIM_PDC","ENG_PDC","PENG_PDC","IM_PDC"
      // if(isNewTicket)
      // {
      //   return "On HOLD";
      // }else if(!isNewTicket){
      //   return "WIP";
      // }
      return "On Hold";
    } else if (status == "IM_OBS") {
      //_getColorByEvent('WIP');
      //"RIM_UOB","RIM_UNDER_OBSERVATION","ENG_UOB","PENG_UOB","IM_OBS"
      return "Under Observation";
    } else if (status == "IM_RESOLVED") {
      //_getColorByEvent('WIP');
      return "RESOLVED";
    } else if (status == "IM_SPART_REQ") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "IM_TRANSFER_RIM") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "IM_WPS") {
      //_getColorByEvent('WIP');

      return "WIP";
    }

    else if (status == "STR_NEW") {
      /*if (isNewTicket) {
        return 'NEW';
      } else if (!isNewTicket) {
        return 'WIP';
      }*/
      return 'WIP';
    }
    //_getColorByEvent('WIP');

   else if (status == "STR_UP") {
      //_getColorByEvent('WIP');
      return 'WIP';
    } else if (status == "STR_WIP") {
      //_getColorByEvent('WIP');
      return 'WIP';
    } else if (status == "STR_SPR_BP") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "STR_SPR_DSPTH") {
      //_getColorByEvent('WIP');

      return "WIP";
    } else if (status == "STR_SPR_DLVR-R") {
      //_getColorByEvent('WIP');
      return "RESOLVED";
    } else if (status == "STR_SPR_DSPTH-ER") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "STR_STDBY_DSPTH") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "STR_STDBY_DLVR") {
      //"STR_STDBY_DLVR","STR_STDBY-DLVR_FP"
      //_getColorByEvent('WIP');
      // return "Standby Given";
       return "Standby Provided";
    //  return "WIP";
    } else if (status == "STR_STDBY_PICK") {
      //_getColorByEvent('WIP');
      return "RESOLVED";
    } else if (status == "STR_FLTY_PFR") {
      //_getColorByEvent('WIP');

      return "WIP";
    } else if (status == "STR_STDBY-DLVR_FP") {
      //_getColorByEvent('WIP');
      // return "Standby Given";
       return "Standby Provided";
     // return "WIP";
    } else if (status == "STR_REP_DLVRD_FP") {
      //_getColorByEvent('WIP');
      return "RESOLVED";
    } else if (status == "STR_REPD_NTBP") {
      //_getColorByEvent('WIP');
      return "RESOLVED";
    } else if (status == "STR_TRNS_IM") {
      //_getColorByEvent('WIP');

      return "WIP";
    } else if (status == "STR_TRNS_TRC_RPR") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "STR_TRNS_PRTNR_RPR") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "STR_FLTY_NTBP") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "STR_TBD") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "STR_PKUP") {
      //_getColorByEvent('WIP');

      return "WIP";
    } else if (status == "STR_PDC") {
      return "On HOLD";
      // //_getColorByEvent('WIP');
      // //"RIM_PDC","ENG_PDC","PENG_PDC","IM_PDC","STR_PDC"
      // if(isNewTicket)
      // {
      //
      // }else if(!isNewTicket){
      //   return "WIP";
      // }
      // return "On Hold";
    }

    ////// for last Remark ///

    else if (status == "STR_DEL_ENR") {
      // if (lastRemark == "Spare Delivered Faulty Picked up") {
      //   return "WIP";
      // } else if (lastRemark == "Spare Delivered Faulty pending to pick") {
      //   return "WIP";
      // }
       if (lastRemark == "Standby Delivered Faulty Picked up") {

        return "Standby Provdided";
      }
       else if (lastRemark == "Standby Delivered Faulty pending to pick") {
        return "Standby Provided";
      }
      // else if (lastRemark == "Unit replacement Delivered Faulty Picked up") {
      //   //
      //   return "WIP";
      // } else if (lastRemark ==
      //     "Unit replacement Delivered Faulty pending to pick") {
      //   return "WIP";
      // } else if (lastRemark == "Spare delivered after repair") {
      //   return "WIP";
      // } else if (lastRemark == "Full unit delivered after repair") {
      //   return "WIP";
      // }
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "STR_DEL_ER") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "STR_RESOLVED") {
      //_getColorByEvent('WIP');
      return "Resolved";
    } else if (status == "STR_HOVR_TRC_RPR") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "STR_HOVR_PTR_RPR") {
      //_getColorByEvent('WIP');

      return "WIP";
    } else if (status == "STR_REP") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "STR_UREP") {
      //_getColorByEvent('WIP');

      return "WIP";
    }
    ///////////////

    if (status == "PART_NEW") {
      //_getColorByEvent('WIP');
      return 'WIP';
    }
    if (status == "PART_ACCEPT") {
      //_getColorByEvent('WIP');
      return 'WIP';
    }
    if (status == "PART_REJECT") {
      //_getColorByEvent('WIP');

      return 'WIP';
    } else if (status == "PART_WIP") {
      //_getColorByEvent('WIP');
      return 'WIP';
    } else if (status == "PART_ASGN_PRTNR") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "PART_ASGN_PENGNR") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "PART_SPR_REQ") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "PART_ASGN_ENGNR") {
      //_getColorByEvent('WIP');

      return "WIP";
    } else if (status == "PART_PDC") {
      //_getColorByEvent('WIP');
      //"RIM_PDC","ENG_PDC","PENG_PDC","IM_PDC","STR_PDC","PART_PDC"
      // if(isNewTicket)
      // {
      //   return "On HOLD";
      // }else if(!isNewTicket){
      //   return "WIP";
      // }
      return "On Hold";
    } else if (status == "PART_RESOLVED") {
      //_getColorByEvent('WIP');
      return "RESOLVED";
    } else if (status == "PART_WPS") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "PART_UOB") {
      return "Under Observation";
      // //_getColorByEvent('WIP');
      // if(isNewTicket)
      //   {
      //     return "Under Observation";
      //   }else if(!isNewTicket){
      //   return "WIP";
      // }
      //"RIM_UOB","RIM_UNDER_OBSERVATION","ENG_UOB","PENG_UOB","IM_OBS","PART_UOB"
      return "Under Observation";
    } else if (status == "TRC_NEW") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "TRC_WIP") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "TRC_REPAIRED") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "TRC_NOT_REPAIRABLE") {
      //_getColorByEvent('WIP');

      return "WIP";
    }


  }
  Future<bool> _onBackPressed() {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('Are you sure?'),
            content: Text('Do you want to exit from App!!'),
            actions: <Widget>[
              FlatButton(
                child: Text('YES'),
                onPressed: () {
                  Navigator.of(context).pop(true);
                },
              ),
              FlatButton(
                child: Text('NO'),
                onPressed: () {
                  Navigator.of(context).pop(false);
                },
              ),
            ],
          );
        });
  }
  Color _colorFromHex(String hexColor) {
    final hexCode = hexColor.replaceAll('#', '');
    return Color(int.parse('FF$hexCode', radix: 16));
  }

  Future<bool> isConnected() async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile) {
      return true;
    } else if (connectivityResult == ConnectivityResult.wifi) {
      return true;
    }
    return false;
  }

  Color checkStatus(String st) {
    Color a;
    if (st == "OK") a = Colors.red;
  }

  Widget loadingView() => Center(
        child: CircularProgressIndicator(
          backgroundColor: Colors.red,
        ),
      );


}
