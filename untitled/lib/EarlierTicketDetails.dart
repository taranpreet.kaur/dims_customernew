import 'package:customer/ETDetail2.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'Deatils2.dart';
import 'Details1.dart';
import 'ETDetail1.dart';

class ETicketDetails extends StatefulWidget {
  String id;
  ETicketDetails(this.id);
  @override
  TicketDetailState createState() => TicketDetailState(id);
}
class TicketDetailState extends State<ETicketDetails> {
  String id;
  TicketDetailState(this.id);

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    this.id = widget.id;
  }
  @override
  Widget build(BuildContext context) {
    Color color1 = _colorFromHex("#00ABC5");
    print("Ticket id is: "+id);

    return MaterialApp(
        home: DefaultTabController(length: 2,
            child: Scaffold
              (appBar: PreferredSize(
              preferredSize: Size.fromHeight(100.0),
              child: AppBar(
                centerTitle: true,
                //toolbarHeight: 50,
                leading: InkWell(
                    onTap: (){
                      Navigator.of(context).pop();
                      //
                      // }


                    },
                    child: Icon(Icons.arrow_back)),
                title: Text("Ticket Details"),

                flexibleSpace: Image(
                  image: AssetImage('assets/appbar_background.png'),
                  fit: BoxFit.cover,
                ),


                bottom:PreferredSize(
                    preferredSize: new Size(10.0, 10.0),
                    child: TabBar(indicatorColor:Colors.white,
                      tabs: [
                        Tab(text: 'Status'),
                        Tab(text: 'OtherDetail',)],)),),
            ),
              body:
              TabBarView(children: [ETDetails1(id),ETDeatils2()],),))
    );
  }
}
Color _colorFromHex(String hexColor) {
  final hexCode = hexColor.replaceAll('#', '');
  return Color(int.parse('FF$hexCode', radix: 16));
}

