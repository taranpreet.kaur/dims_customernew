import 'dart:async';
import 'dart:convert';
import 'package:customer/EarlierCallHistory.dart';
import 'package:customer/NotificationNew.dart';
import 'package:customer/TicketDetails.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:customer/Constant.dart';
import 'package:customer/MapPage.dart';
import 'package:customer/MapScreen.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:workmanager/workmanager.dart';
import 'Dashboard.dart';
import 'Tracking.dart';

void main() {
  String id;
  WidgetsFlutterBinding.ensureInitialized();
  Workmanager().initialize(callbackDispatcher);

  Workmanager().registerPeriodicTask(
    "2",
    // use the same task name used in callbackDispatcher function for identifying the task
    // Each task must have a unique name if you want to add multiple tasks;
    myTask,
    // When no frequency is provided the default 15 minutes is set.
    // Minimum frequency is 15 min.
    // Android will automatically change your frequency to 15 min if you have configured a lower frequency than 15 minutes.
    frequency: Duration(minutes: 15), // change duration according to your needs
  );
  runApp(ETDetails1(id));
}

const myTask = "syncWithTheBackEnd";

class ETDetails1 extends StatefulWidget {
  String id;
  var ticketId;

  ETDetails1(this.id) : super();
  SharedPreferences sharedPreferences;

  @override
  DetailState createState() => DetailState(id);
}

class DetailState extends State<ETDetails1> {
  String id;
  LatLng currentPostion;

  bool loading = false;

  DetailState(this.id);

  var ticketId;
  var isButtonEnable;
  var lastRemark;
  var lastStatus;
  var assetIdCount;
  bool isNewTicket;
  var status;
  var isPartRequested;
  Timer timer;
  bool isLoading = true;

  @override
  void initState() {
    super.initState();
    getDetails();
    autoRefreshDetails();


    // initStateAsync();
  }

  Future<Null> refreshList() async {
    await getDetails();
  }

  void autoRefreshDetails() {
    timer = Timer.periodic(Duration(seconds: 5), (Timer t) async {
      await getDetails();
    });
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    // this will stop api calling
    if (timer != null) {
      timer.cancel();
    }
  }

  @override
  Widget build(BuildContext context) {
    Color color1 = _colorFromHex("#00ABC5");

    return WillPopScope(
        onWillPop: _onBackPressed,
        child: RefreshIndicator(
          onRefresh: refreshList,
          child: Scaffold(
              body: isLoading
                  ? Center(
                child: CircularProgressIndicator(),
              )
                  : Center(
                  child: Padding(
                      padding: EdgeInsets.all(10),
                      child: ListView(
                        children: <Widget>[
                          Container(
                            alignment: Alignment.center,
                            padding: EdgeInsets.all(10),
                          ),
                          Container(
                            padding: EdgeInsets.all(10),
                            child: Text(
                              'Ticket Details',
                              textAlign: TextAlign.left,
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 20),
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.all(10),
                            child: Text(
                              'AssetId',
                              textAlign: TextAlign.left,
                              overflow: TextOverflow.ellipsis,
                              style:
                              TextStyle(fontWeight: FontWeight.normal),
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.all(10),
                            child: TextField(
                              controller: assetId_controller ?? "AssetId",
                              textAlign: TextAlign.left,
                              style: TextStyle(fontWeight: FontWeight.bold),
                              readOnly: true,
                            ),
                          ),

                          Container(
                            padding: EdgeInsets.all(10),
                            child: Text(
                              'TicketId',
                              textAlign: TextAlign.left,
                              overflow: TextOverflow.ellipsis,
                              style:
                              TextStyle(fontWeight: FontWeight.normal),
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.all(10),
                            child: TextField(
                              controller: ticketId_controller ?? "TicketId",
                              textAlign: TextAlign.left,
                              style: TextStyle(fontWeight: FontWeight.bold),
                              readOnly: true,
                            ),
                          ),

                          Container(
                            padding: EdgeInsets.all(10),
                            child: Text(
                              'AssetType',
                              textAlign: TextAlign.left,
                              overflow: TextOverflow.ellipsis,
                              style:
                              TextStyle(fontWeight: FontWeight.normal),
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.all(10),
                            child: TextField(
                              controller:
                              assettype_controller ?? "AssetType",
                              textAlign: TextAlign.left,
                              style: TextStyle(fontWeight: FontWeight.bold),
                              readOnly: true,
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.all(10),
                            child: Text(
                              'Status',
                              textAlign: TextAlign.left,
                              overflow: TextOverflow.ellipsis,
                              style:
                              TextStyle(fontWeight: FontWeight.normal),
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.all(10),
                            child: TextField(
                              controller: status_controller ?? "Status",
                              textAlign: TextAlign.left,
                              style: TextStyle(fontWeight: FontWeight.bold),
                              readOnly: true,
                            ),
                          ),

                          Container(
                            padding: EdgeInsets.all(10),
                            child: Text(
                              'Live Tracking',
                              textAlign: TextAlign.left,
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 20),
                            ),
                          ),
                          Container(
                            height: 450,
                            width: 360,
                            color: Colors.white,
                            padding: EdgeInsets.all(2.0),
                            child: loading
                                ? GoogleMap(
                              markers: Set.of(markers.values),
                              onMapCreated: _onMapCreated,
                              myLocationEnabled: true,
                              myLocationButtonEnabled: true,
                              mapType: MapType.normal,
                              scrollGesturesEnabled: true,
                              zoomGesturesEnabled: true,
                              initialCameraPosition: CameraPosition(
                                target: currentPostion,
                                zoom: 12.0,
                              ),
                            )
                                : Container(),
                          ),

                          _buildLoginButton(),

                          // Container(
                          //   margin: EdgeInsets.all(25),
                          //   child: FlatButton(
                          //     child: Text('Start Tracking',
                          //
                          //
                          //     style: TextStyle(fontSize: 12.0),
                          //     ),
                          //     color: color1,
                          //     textColor: Colors.white,
                          //     onPressed: () {
                          //       print("value of status"+status_controller.text);
                          //
                          //       if(status_controller.text=="Agent on the way")
                          //       {
                          //         String id=ticketId;;
                          //
                          //         Navigator.push(context, MaterialPageRoute(builder: (context) => MyApp1(id,ticket_lat,ticket_lng,eng_lat,eng_lng)),);
                          //
                          //       }
                          //       else
                          //       {
                          //         Fluttertoast.showToast(
                          //             msg: "Engineer not starting their journey yet",
                          //             toastLength: Toast.LENGTH_SHORT,
                          //             gravity: ToastGravity.CENTER,
                          //             timeInSecForIosWeb: 1,
                          //             backgroundColor: Colors.red,
                          //             textColor: Colors.white,
                          //             fontSize: 16.0
                          //         );
                          //       }
                          //     },
                          //
                          //   ),
                          //
                          // ),
                        ],
                      )))),
        ));
  }

  Future<bool> _onBackPressed() {
    print("==comin in detail");
    return showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('Are you sure?'),
            content: Text('Do you want to exit from Tracking!!'),
            actions: <Widget>[
              FlatButton(
                child: Text('YES'),
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => Dashboard()),
                  );
                },
              ),
              FlatButton(
                child: Text('NO'),
                onPressed: () {
                  Navigator.of(context).pop(false);
                },
              ),
            ],
          );
        });
  }

  Widget _buildLoginButton() {
    // print("checkData " + rtcCheck );
    if (status_controller.text == "RESOLVED") {
      return Container();
    } else {
      Color color1 = _colorFromHex("#00ABC5");
      return Container(
        margin: EdgeInsets.all(25),
        child: FlatButton(
          child: Text(
            'Start Tracking',
            style: TextStyle(fontSize: 12.0),
          ),
          color: color1,
          textColor: Colors.white,
          onPressed: () {
            print("value of status" + status_controller.text);
            if (status_controller.text == "Engineer on the way") {
              String id = ticketId;
              ;
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => Tracking(
                          id, ticket_lat, ticket_lng, eng_lat, eng_lng)));
              // MyApp1(id,ticket_lat,ticket_lng,eng_lat,eng_lng)),
              // id,ticket_lat,ticket_lng,eng_lat,eng_lng
            } else if (status == "ENG_RAS") {
              Fluttertoast.showToast(
                  msg: "Engineer already reached at your location",
                  toastLength: Toast.LENGTH_SHORT,
                  gravity: ToastGravity.CENTER,
                  timeInSecForIosWeb: 1,
                  backgroundColor: Colors.red,
                  textColor: Colors.white,
                  fontSize: 16.0);
            } else {
              Fluttertoast.showToast(
                  msg: "Engineer is yet to start the journey!",
                  toastLength: Toast.LENGTH_SHORT,
                  gravity: ToastGravity.CENTER,
                  timeInSecForIosWeb: 1,
                  backgroundColor: Colors.red,
                  textColor: Colors.white,
                  fontSize: 16.0);
            }
          },
        ),
      );
    }
  }

  _status(status, lastRemark, isPartRequested) {
   if(status == "ENG_STDBY_INSTL"){
     print("Status11 "+status+" lastRemark "+" ispartRequested "+isPartRequested.toString());
   }


    if (status == "RIM_NEW") {
      if (isPartRequested == "1") {
        return 'WIP';
      } else {
        return 'NEW';
      }
      // _getColorByEvent('New');

    } else if (status == "RIM_ACCEPT") {
      // _getColorByEvent('WIP');
      return 'WIP';
    } else if (status == "RIM_WIP") {
      // _getColorByEvent('WIP');
      return "WIP";
    } else if (status == "RIM_UOB") {
      //_getColorByEvent('Under Observation');
      return "Under Observation";
    } else if (status == "RIM_PA") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "RIM_PDC") {
      //_getColorByEvent('HOLD');
      return "On HOLD";
    } else if (status == "RIM_FAULTPRST") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "RIM_RESOLVED") {
      //_getColorByEvent('RESOLVED');
      return "RESOLVED";
    } else if (status == "RIM_TRNSIM") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "RIM_TOE") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "RIM_FAULTPR") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "RIM_UNDER_OBSERVATION") {
      return "Under Observation";
    } else if (status == "RIM_TIM") {
      return "WIP";
    } else if (status == "RIM_TENG") {
      return "WIP";
    } else if (status == "RIM_PART_AUTH") {
      return "WIP";
    } else if (status == "RIM_FPR") {
      return "WIP";
    } else if (status == "RIM_FPR_SR") {
      return "WIP";
    } else if (status == "RIM_RTN_ENG") {
      return "WIP";
    } else if (status == "RIM_STD_REQ") {
      return "WIP";
    } else if (status == "RIM_FPR_STD_REQ") {
      return "WIP";
    }
    if (status == "ENG_NEW") {
      //_getColorByEvent('New');
      return 'NEW';
    } else if (status == "ENG_ACCEPT") {
      //_getColorByEvent('Assign to Aggent');
      return 'Assigned to Engineer';
    } else if (status == "ENG_REJECT") {
      //_getColorByEvent('New');
      return "NEW";
    } else if (status == "ENG_PFS") {
      //_getColorByEvent('Aggent on the way');
      return "Engineer on the way";
    } else if (status == "ENG_RAS") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "ENG_RESOLVED") {
      //_getColorByEvent('RESOLVED');

      return "RESOLVED";
    } else if (status == "ENG_UOB") {
      //_getColorByEvent('WIP');
      return "Under Observation";
    } else if (status == "ENG_SPAREQ") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "ENG_SPR_REQ") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "ENG_WIP") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "ENG_PDC") {
      //_getColorByEvent('WIP');
      return "On Hold";
    } else if (status == "ENG_RESO") {
      //_getColorByEvent('WIP');

      return "RESOLVED";
    } else if (status == "ENG_TRANSIM") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "ENG_TRANS_IM") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "ENG_TRNS_IM") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "ENG_WPS") {
      //_getColorByEvent('WIP');
      return "WIP";
    }
    if (status == "PENG_NEW") {
      //_getColorByEvent('New');
      return 'NEW';
    } else if (status == "PENG_ACCEPT") {
      //_getColorByEvent('Assign to Aggent');
      return 'Assign to Engineer';
    } else if (status == "PENG_REJECT") {
      //_getColorByEvent('New');
      return "NEW";
    } else if (status == "PENG_PFS") {
      //_getColorByEvent('Aggent on the way');
      return "Engineer on the way";
    } else if (status == "PENG_RAS") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "PENG_RESOLVED") {
      //_getColorByEvent('RESOLVED');

      return "RESOLVED";
    } else if (status == "PENG_UOB") {
      //_getColorByEvent('WIP');
      return "Under Observation";
    } else if (status == "PENG_SPAREQ") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "PENG_TRNS_PART") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "PENG_WPS") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "PENG_SPR_REQ") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "PENG_WIP") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "ENG_STDBY_INSTL") {
      //_getColorByEvent('WIP');
      return "Standby Provided";
    } else if (status == "PENG_PDC") {
      //_getColorByEvent('WIP');
      return "On Hold";
    } else if (status == "PENG_RESO") {
      //_getColorByEvent('WIP');

      return "RESOLVED";
    } else if (status == "PENG_STDBY_INSTL") {
      //_getColorByEvent('WIP');
      return "Standby Provided";
    } else if (status == "PENG_TRANSIM") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "PENG_TRANS_IM") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "PENG_TRNS_IM") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "Standby Delivered") {
      //_getColorByEvent('WIP');
      return "Standby Provided";
    } else if (status == "Standby Delivered and Faulty Picked") {
      //_getColorByEvent('WIP');
      return "Standby Provided";
    } else if (status == "IM_NEW") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "RIM_TIM") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "IM_WIP") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "IM_ASGND_PRTNR") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "IM_ASGND_ENGNR") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "IM_PDC") {
      //_getColorByEvent('WIP');
      return "On Hold";
    } else if (status == "IM_OBS") {
      //_getColorByEvent('WIP');
      return "Under Observation";
    } else if (status == "IM_RESOLVED") {
      //_getColorByEvent('WIP');

      return "RESOLVED";
    } else if (status == "IM_SPART_REQ") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "IM_TRANSFER_RIM") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "IM_WPS") {
      //_getColorByEvent('WIP');
      return "WIP";
    }

    if (status == "STR_NEW") {
      //_getColorByEvent('WIP');
      return 'WIP';
    }
    if (status == "STR_UP") {
      //_getColorByEvent('WIP');
      return 'WIP';
    } else if (status == "STR_WIP") {
      //_getColorByEvent('WIP');
      return 'WIP';
    } else if (status == "STR_SPR_BP") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "STR_SPR_DSPTH") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "STR_SPR_DLVR-R") {
      //_getColorByEvent('WIP');

      return "RESOLVED";
    } else if (status == "STR_SPR_DSPTH-ER") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "STR_STDBY_DSPTH") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "STR_STDBY_DLVR") {
      //_getColorByEvent('WIP');
      return "Standby Provided";
    } else if (status == "STR_STDBY_PICK") {
      //_getColorByEvent('WIP');

      return "RESOLVED";
    } else if (status == "STR_FLTY_PFR") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "STR_STDBY-DLVR_FP") {
      //_getColorByEvent('WIP');
      return "Standby Provided";
    } else if (status == "STR_REP_DLVRD_FP") {
      //_getColorByEvent('WIP');
      return "RESOLVED";
    } else if (status == "STR_REPD_NTBP") {
      //_getColorByEvent('WIP');
      return "RESOLVED";
    } else if (status == "STR_TRNS_IM") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "STR_TRNS_TRC_RPR") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "STR_TRNS_PRTNR_RPR") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "STR_FLTY_NTBP") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "STR_TBD") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "STR_PKUP") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "STR_PDC") {
      //_getColorByEvent('WIP');
      return "On Hold";
    } else if (status == "STR_DEL_ENR") {
      // if (lastRemark == "Spare Delivered Faulty Picked up") {
      //   return "WIP";
      // } else if (lastRemark == "Spare Delivered Faulty pending to pick") {
      //   return "WIP";
      // } else
      print("LLLLL => "+lastRemark);
      if (lastRemark == "Standby Delivered Faulty Picked up") {
        return "Standby Provided";
      } else if (lastRemark == "Standby Delivered Faulty pending to pick") {
        return "Standby Provided";
      }else{
        return "No Status";
      }
      // else if (lastRemark == "Unit replacement Delivered Faulty Picked up") {
      //   return "WIP";
      // } else if (lastRemark ==
      //     "Unit replacement Delivered Faulty pending to pick") {
      //   return "WIP";
      // } else if (lastRemark == "Spare delivered after repair") {
      //   return "WIP";
      // } else if (lastRemark == "Full unit delivered after repair") {
      //   return "WIP";
     // }
      //_getColorByEvent('WIP');

    } else if (status == "STR_DEL_ER") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "STR_RESOLVED") {
      //_getColorByEvent('WIP');

      return "Resolved";
    } else if (status == "STR_HOVR_TRC_RPR") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "STR_HOVR_PTR_RPR") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "STR_REP") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "STR_UREP") {
      //_getColorByEvent('WIP');
      return "WIP";
    }

    if (status == "PART_NEW") {
      //_getColorByEvent('WIP');
      return 'WIP';
    }
    if (status == "PART_ACCEPT") {
      //_getColorByEvent('WIP');
      return 'WIP';
    }
    if (status == "PART_REJECT") {
      //_getColorByEvent('WIP');
      return 'WIP';
    } else if (status == "PART_WIP") {
      //_getColorByEvent('WIP');
      return 'WIP';
    } else if (status == "PART_ASGN_PRTNR") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "PART_ASGN_PENGNR") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "PART_SPR_REQ") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "PART_ASGN_ENGNR") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "PART_PDC") {
      //_getColorByEvent('WIP');
      return "On Hold";
    } else if (status == "PART_RESOLVED") {
      //_getColorByEvent('WIP'); // TICKET_CANCELLED
      //
      return "RESOLVED";
    } else if (status == "TICKET_CANCELLED") {
      //_getColorByEvent('WIP');
      return "Ticket Cancelled";
    } else if (status == "PART_WPS") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "PART_UOB") {
      //_getColorByEvent('WIP');
      return "Under Observation";
    } else if (status == "TRC_NEW") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "TRC_WIP") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "TRC_REPAIRED") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "TRC_NOT_REPAIRABLE") {
      //_getColorByEvent('WIP');
      return "WIP";
    }
  }

  getDetails() async {
    print("cjkeek");
    // setState(() {
    //   isLoading = true;
    //   CircularProgressIndicator();
    // });
    print("==getdetails");
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    var id = sharedPreferences.getString("ticket_id");
    print("ticket_id" + id.toString());
    String token = sharedPreferences.getString("token");
    Map<String, String> headers = {"token": token};
    var url = Constants.base_url + "api/ticket/ticketDetail";
    var request = http.MultipartRequest('POST', Uri.parse(url));
    request.headers.addAll(headers);
    request.fields['ticketID'] = id;
    var response = await request.send();
    if (response.statusCode == 200) {
      setState(() {
        isLoading = false;
      });
      var result = await http.Response.fromStream(response);
      final jsonResponse = jsonDecode(result.body);
      print("==jsonResponse" + jsonResponse.toString());
      var address = jsonResponse[0]['address'];
      var line1 = address['line1'];
      var line2 = address['line2'];
      var city = address['city'];
      var state = address['state'];
      var postalcode = address['postalcode'];
      var postalcode1 = jsonResponse[0]["postalcode"];
      var add;
      if (postalcode != null) {
        add = line1 + " " + line2 + city + " " + state + " " + postalcode;
        print("==address" + add.toString());
      } else {
        add = line1 + " " + line2 + city + " " + state + " " + postalcode1;
        print("==address" + add.toString());
      }

      var location = jsonResponse[0]['location'];
      var coordinates = location['coordinates'];
      if (coordinates.isEmpty) {
        ticket_lat = 0.0;
        ticket_lng = 0.0;
      } else {
        ticket_lat = coordinates[1];
        ticket_lng = coordinates[0];
      }
      // lat=coordinates[1] ?? 28.605407;
      // lng=coordinates[0] ?? 77.2146617;
      print("==coordinates" + ticket_lat.toString() + ticket_lng.toString());

      //  var line1=address["line1"];
      //  var city=address["city"];
      //  var state=address["state"];
      // var pincode=address["pincode"];
      status = jsonResponse[0]["status"];
      print(" status =>  "+status);
      lastRemark = jsonResponse[0]["lastRemark"];
      print(" status =>  "+lastRemark);
      lastStatus = jsonResponse[0]["lastStatus"];
      isNewTicket = jsonResponse[0]["isNewTicket"];
      var assetId = jsonResponse[0]["assetId"];
      var assetType = jsonResponse[0]["assetType"];
      var ticket_type = jsonResponse[0]["ticket_type"];
      ticketId = jsonResponse[0]["_id"];
      var custName = jsonResponse[0]["custName"];
      var email = jsonResponse[0]["email"];
      var serviceType = jsonResponse[0]["callType"];
      var assestWarranty = jsonResponse[0]["assestWarranty"];
      var issuecategory = jsonResponse[0]["issueCategory"];
      var issueSubCategory = jsonResponse[0]["issueSubCategory"];
      var ticketSerialNo = jsonResponse[0]["ticketSerialNo"];
      var assetSerial = jsonResponse[0]["assetSerial"];
      var serviceItemNo = jsonResponse[0]["serviceItemNo"];
      var createdAt = jsonResponse[0]["createdAt"].toString();
      assetIdCount = jsonResponse[0]["assetIdCount"].toString();
      isPartRequested = jsonResponse[0]["isPartRequested"].toString();
      print("IsPartrequest is" + assetIdCount.toString());
      sharedPreferences.setString("assetIdCount", assetIdCount);
      // _dateController.text = DateFormat('dd-MM-yyyy').format();

      var assetId1 = toBeginningOfSentenceCase(assetId);
      var custName1 = toBeginningOfSentenceCase(custName);
      var st = toBeginningOfSentenceCase(serviceType);
      var isc = toBeginningOfSentenceCase(issueSubCategory);
      var ic = toBeginningOfSentenceCase(issuecategory);
      var ts = toBeginningOfSentenceCase(ticketSerialNo);
      var tt = toBeginningOfSentenceCase(ticket_type);
      var at = toBeginningOfSentenceCase(assetType);
      var addnew = toBeginningOfSentenceCase(add);

      final date = assestWarranty;
      DateTime parseDate = new DateFormat("yyyy-MM-dd").parse(date);
      var inputDate = DateTime.parse(parseDate.toString());
      var outputFormat = DateFormat('dd-MM-yyyy');
      var outputDate = outputFormat.format(inputDate);
      print("" + outputDate);

      sharedPreferences.setString("address_detail", addnew);
      sharedPreferences.setString("custName_detail", custName1);
      sharedPreferences.setString("assetSerial", assetSerial);
      sharedPreferences.setString("serviceItemNo", serviceItemNo);
      sharedPreferences.setString("createdAt", createdAt);
      if(email != null){
        //   print("Email => "+email);
      }else{
        //  print("Email is null");
      }
      //  sharedPreferences.setString("email_detail", email);
      //  sharedPreferences.setString("email_profile", email);
      sharedPreferences.setString("assetId", assetId1);
      //   sharedPreferences.setString("assetSerial", );
      print("The assetid data is" + assetId.toString());
      sharedPreferences.setString("serviceType_detail", st);
      sharedPreferences.setString("assestWarranty_detail", outputDate);
      sharedPreferences.setString("issuecategory_detail", ic);
      sharedPreferences.setString("issueSubCategory_detail", isc);
      sharedPreferences.setString("ticketSerialNo_detail", ts);

      if (status_controller.text == "WIP") {
        isButtonEnable = false;
      }

      if (status == "ENG_PFS") {
        var engineer = jsonResponse[0]['engineer'];
        var location = engineer['location'];
        var coordinates = location['coordinates'];
        eng_lat = coordinates[1];
        eng_lng = coordinates[0];
        print("Englat" + eng_lat.toString() + eng_lng.toString());
      }

      // print("==address"+address);
      // print("==line1"+line1);
      print("==result" + result.body.toString());
      // var address1=line1+", "+city+", "+state+", ";
      //  var  sharedPreferences    = await SharedPreferences.getInstance();
      //    sharedPreferences.setString("address",address1);
      assettype_controller.value = TextEditingValue(
        text: at ?? "",

        // selection: TextSelection.fromPosition(
        //   TextPosition(offset: assetType.length),
        // ),
      );

      ticketId_controller.value = TextEditingValue(
        text: ts ?? "",

        // selection: TextSelection.fromPosition(
        //   TextPosition(offset: ticketSerialNo.length),
        //  ),
      );

      assetId_controller.value = TextEditingValue(
        text: assetId1 ?? "",

        // selection: TextSelection.fromPosition(
        //   TextPosition(offset: assetId.length),
        // ),
      );
      status_controller.value = TextEditingValue(
        text: _status(status, lastRemark,isPartRequested) ?? "",

        // selection: TextSelection.fromPosition(
        //   TextPosition(offset: status.length),
        // ),
      );
      printLocation(ticket_lat, ticket_lng);
    } else {
      print("==response error" + response.toString());
    }
  }

  void printLocation(lat, lng) async {
    setState(() {
      if (lat == null && lng == null) {
        lat = 28.605407;
        lng = 77.2146617;
      }
      currentPostion = LatLng(lat, lng);
      _addMarker(LatLng(lat, lng), "origin",
          BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueAzure));
      loading = true;
    });
  }

  _addMarker(LatLng position, String id, BitmapDescriptor descriptor) {
    print("position " + position.toString());
    MarkerId markerId = MarkerId(id);
    Marker marker =
    Marker(markerId: markerId, icon: descriptor, position: position);
    markers[markerId] = marker;
  }

  void _onMapCreated(GoogleMapController controller) {
    myController = controller;
    myController.animateCamera(
      CameraUpdate.newCameraPosition(
        CameraPosition(target: LatLng(ticket_lat, ticket_lng), zoom: 15),
      ),
    );
    _addMarker(LatLng(ticket_lat, ticket_lng), "origin",
        BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueAzure));
    // _location.onLocationChanged.listen((l) {
    //   myController.animateCamera(
    //     CameraUpdate.newCameraPosition(
    //       CameraPosition(target: LatLng(l.latitude, l.longitude),zoom: 15),
    //     ),
    //   );
    // });
  }

  Map<MarkerId, Marker> markers = <MarkerId, Marker>{};
}

void callbackDispatcher() {
// this method will be called every hour
  Workmanager().executeTask((task, inputdata) async {
    switch (task) {
      case myTask:
        print("this method was called from native!");
        Fluttertoast.showToast(msg: "this method was called from native!");
        break;

      case Workmanager.iOSBackgroundTask:
        print("iOS background fetch delegate ran");
        break;
    }

    //Return true when the task executed successfully or not
    return Future.value(true);
  });
}

var assetId_controller = TextEditingController();
var ticketId_controller = TextEditingController();
var assettype_controller = TextEditingController();
var status_controller = TextEditingController();
var button_controller = TextEditingController();
final LatLng _center = const LatLng(45.521563, -122.677433);
GoogleMapController myController;
var ticket_lat;
var ticket_lng;
var eng_lat;
var eng_lng;
var isLoading = false;

Color _colorFromHex(String hexColor) {
  final hexCode = hexColor.replaceAll('#', '');
  return Color(int.parse('FF$hexCode', radix: 16));
}

Color _getColorByEvent(String event) {
  if (event == "ENG_ACCEPTED") return Colors.green;
  if (event == "ENG_REJECTED") return Colors.red;
  return Colors.blue;
}
