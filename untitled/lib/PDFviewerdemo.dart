// import 'package:flutter/material.dart';
// import 'package:syncfusion_flutter_pdfviewer/pdfviewer.dart';
//
//
//
// /// Represents Homepage for Navigation
// class PDFHomePage extends StatefulWidget {
//   @override
//   _HomePage createState() => _HomePage();
// }
//
// class _HomePage extends State<PDFHomePage> {
//   final GlobalKey<SfPdfViewerState> _pdfViewerKey = GlobalKey();
//
//   @override
//   void initState() {
//     super.initState();
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         centerTitle: true,
//         title: const Text('PDF View'),
//         flexibleSpace: Image(
//           image: AssetImage('assets/appbar_background.png'),
//           fit: BoxFit.cover,
//         ),
//
//         actions: <Widget>[
//           IconButton(
//             icon: Icon(
//               Icons.edit,
//               color: Colors.white,
//             ),
//             onPressed: () {
//             //  Navigator.push(context, MaterialPageRoute(builder: (context) => EditProfile()),);
//
//             },
//           )
//         ],
//       ),
//       body: SfPdfViewer.network(
//         'https://cdn.syncfusion.com/content/PDFViewer/flutter-succinctly.pdf',
//         key: _pdfViewerKey,
//       ),
//     );
//   }
// }