import 'package:customer/TicketDetails.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';


void main() {
  runApp(new MaterialApp(
    title: "Example",
    home: new FabHideOnKeyboard(),
  ));
}

class FabHideOnKeyboard extends StatefulWidget {
  @override
  _FabHideOnKeyboardState createState() => new _FabHideOnKeyboardState();
}

class _FabHideOnKeyboardState extends State<FabHideOnKeyboard> {
  @override
  Widget build(BuildContext context) {
    final bool showFab = MediaQuery.of(context).viewInsets.bottom==0.0;
    return Scaffold(
      resizeToAvoidBottomInset: true,
      body:
      Container(
        alignment: Alignment.center,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text("TextField:"),
            TextField()
          ],
        ),
      ),
      /*floatingActionButton: showFab?Icon(Icons.add):null,*/
    );
  }
}