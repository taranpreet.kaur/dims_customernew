import 'package:badges/badges.dart';
import 'package:customer/Details1.dart';
import 'package:customer/EarlierTicketListingTab.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:intl/intl.dart';


import 'EarlierCallHistory.dart';

  class Deatils2 extends StatefulWidget{
  @override
  State<Deatils2> createState() => _Deatils2State();
}

class _Deatils2State extends State<Deatils2> {

  var _controller = TextEditingController(text: 'name');

  var email_controller=TextEditingController(text:'emailId');

  var phone_controller=TextEditingController(text:'PhoneNumber');

  var address_controller=TextEditingController(text:'Address');

  var serviceType_controller=TextEditingController(text:'ServiceType');

  var assestWarranty_controller=TextEditingController(text:'AssestWarranty');

  var ticketSerialNo_controller=TextEditingController(text:'TicketSerialNo');

  var issuecategory_controller=TextEditingController(text:'Issuecategory');

  var issuesubcategory_controller=TextEditingController(text:'IssueSubcategory');
  bool showElevatedButtonBadge = true;
  var assetIdCount;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    prefs();
  }

  Widget _elevatedButtonBadge() {
    return Align(
      alignment: Alignment.centerLeft,
      child: Badge(
        showBadge: showElevatedButtonBadge,
        padding: EdgeInsets.all(10),
        badgeColor: Colors.redAccent,
        badgeContent: Text(
            assetIdCount == null ? "0" : assetIdCount.toString(),
          style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
        ),
        child: ElevatedButton(
          style: ElevatedButton.styleFrom(
           // primary: Colors.green,
          //  onPrimary: Colors.white,
          //  shadowColor: Colors.greenAccent,
          //  elevation: 3,
            /*shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(32.0)),*/
            minimumSize: Size(300, 40), //////// HERE we are setting size of button height and width
          ),
          onPressed: () {
            print("Tap is working fine");
            Navigator.push(context,MaterialPageRoute(builder:(context)=> EarlierCallTicketListing()),);
            // setState(() {
            //   showElevatedButtonBadge = !showElevatedButtonBadge;
            // });
          },
          child: Text('Earlier Call History',
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
          ),
        ),
      ),
    );
  }

  @override
  build(BuildContext context) {

    Color color1 = _colorFromHex("#00ABC5");




    return Scaffold(


        body: Center(
            child: Padding(

                padding: EdgeInsets.all(10),
                child: ListView(
                  children: <Widget>[
                    Container(
                      alignment: Alignment.center,
                      padding: EdgeInsets.all(10),
                    ),
                    Container(
                      padding: EdgeInsets.all(10),
                      child: Text(
                        'Customer Info',
                        textAlign: TextAlign.left,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(fontWeight: FontWeight.bold,fontSize:20),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.all(10),
                      child: Text(
                        'Customer Name',
                        textAlign: TextAlign.left,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(fontWeight: FontWeight.normal),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.all(10),
                      child: TextField(

                        controller: _controller??"Name",

                        textAlign: TextAlign.left,

                        style: TextStyle(fontWeight: FontWeight.bold),
                        readOnly: true,

                      ),

                    ),

                       Container(
                      padding: EdgeInsets.all(10),
                      child: Text(
                        'Email',
                        textAlign: TextAlign.left,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(fontWeight: FontWeight.normal),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.all(10),
                      child:TextField(

                        controller: email_controller??"Email",

                        textAlign: TextAlign.left,

                        style: TextStyle(fontWeight: FontWeight.bold),
                        readOnly: true,

                      ),
                    ),


                    Container(
                      padding: EdgeInsets.all(10),
                      child: Text(
                        'Address',
                        textAlign: TextAlign.left,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(fontWeight: FontWeight.normal),
                      ),
                    ),
                    // Container(
                    //   padding: EdgeInsets.all(10),
                    //   child:TextField(
                    //
                    //     controller: address_controller??"Address",
                    //     textAlign: TextAlign.left,
                    //
                    //     style: TextStyle(fontWeight: FontWeight.bold),
                    //     readOnly: true,
                    //
                    //   ),
                    // ),






                    Container(
                        padding: EdgeInsets.all(10),
                        // width: 260,
                        // height:  50,
                        child: TextField(
                          // "nstance of 'TicketResponse', Instance of 'TicketResponse', Instance of 'TicketResponse', Instance of 'TicketResponse', Instance of 'TicketResponse', Instance of 'TicketResponse', Instance of 'TicketResponse', Instance of 'TicketResponse', Instance of 'TicketResponse']"
                            controller: address_controller??"Address",
                            maxLines: 3,

                          style: TextStyle(fontWeight: FontWeight.bold),
                          readOnly: true,
                        )
                    ),




                    Container(
                      padding: EdgeInsets.all(10),
                      child: Text(
                        'Customer Type',
                        textAlign: TextAlign.left,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(fontWeight: FontWeight.normal),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.all(10),
                      child: Text(
                        'Contractual',
                        textAlign: TextAlign.left,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                    ),


                    Container(
                      alignment: Alignment.center,
                      padding: EdgeInsets.all(10),
                    ),
                    Container(
                      padding: EdgeInsets.all(10),
                      child: Text(
                        'Agreement Info',
                        textAlign: TextAlign.left,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(fontWeight: FontWeight.bold,fontSize:20),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.all(10),
                      child: Text(
                        'Service Type',
                        textAlign: TextAlign.left,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(fontWeight: FontWeight.normal),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.all(10),
                      child: TextField(

                        controller: serviceType_controller??"Service",

                        textAlign: TextAlign.left,

                        style: TextStyle(fontWeight: FontWeight.bold),
                        readOnly: true,

                      ),

                    ),

                    Container(
                      padding: EdgeInsets.all(10),
                      child: Text(
                        'Asset AMC/Warranty',
                        textAlign: TextAlign.left,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(fontWeight: FontWeight.normal),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.all(10),
                      child:TextField(

                        controller: assestWarranty_controller??"AssetWarranty",

                        textAlign: TextAlign.left,

                        style: TextStyle(fontWeight: FontWeight.bold),
                        readOnly: true,

                      ),
                    ),
                    Container(
                      padding: EdgeInsets.all(10),
                      child: Text(
                        'Ticket Serial No',
                        textAlign: TextAlign.left,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(fontWeight: FontWeight.normal),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.all(10),
                      child: TextField(
                       controller: ticketSerialNo_controller??"TicketSerialNo",
                        textAlign: TextAlign.left,
                        readOnly: true,
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                    ),


                    Container(

                      alignment: Alignment.center,
                      padding: EdgeInsets.all(10),
                    ),
                    Container(
                      padding: EdgeInsets.all(10),
                      child: Text(
                        'Incident Info',
                        textAlign: TextAlign.left,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(fontWeight: FontWeight.bold,fontSize:20),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.all(10),
                      child: Text(
                        'Issue Category',
                        textAlign: TextAlign.left,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(fontWeight: FontWeight.normal),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.all(10),
                      child: TextField(

                        controller: issuecategory_controller??"IssueCategory",

                        textAlign: TextAlign.left,

                        style: TextStyle(fontWeight: FontWeight.bold),
                        readOnly: true,

                      ),

                    ),

                    Container(
                      padding: EdgeInsets.all(10),
                      child: Text(
                        'Issue Sub-Category',
                        textAlign: TextAlign.left,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(fontWeight: FontWeight.normal),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.all(10),
                      child:TextField(

                        controller: issuesubcategory_controller??"IssueSubCategory",

                        textAlign: TextAlign.left,

                        style: TextStyle(fontWeight: FontWeight.bold),
                        readOnly: true,

                      ),
                    ),

                    /*Container(
                      width: 50,
                      padding: EdgeInsets.all(5),
                      child: FlatButton(
                        child: Text(
                          'Earlier Call History',
                          style: TextStyle(fontSize: 12.0),
                        ),
                        color: color1,
                        textColor: Colors.white,
                        onPressed: () {
                          print("Tap is working fine");
                          Navigator.push(context,MaterialPageRoute(builder:(context)=> EarlierCallHistory(assetsId: assetId_controller.text,)),);
                        },
                      ),
                    ),*/
                   Container(
                     width: 50,
                     //color: Colors.orange,
                     padding: EdgeInsets.all(10),
                     child: _elevatedButtonBadge(),
                   ),




                  ],
                ))));
  }

  Color _colorFromHex(String hexColor) {
    final hexCode = hexColor.replaceAll('#', '');
    return Color(int.parse('FF$hexCode', radix: 16));
  }

  Future<String> prefs() async {
    final prefs = await SharedPreferences.getInstance();
    var n=prefs.get("custName_detail");
    String email =prefs.get("email");
  //  print("email => "+email);
    String phone=prefs.get("phone");
    String address=prefs.get("address_detail");
    String serviceType=prefs.get("serviceType_detail");
    String assestWarranty=prefs.get("assestWarranty_detail");
    String ticketSerialNo=prefs.get("ticketSerialNo_detail");
    String issueCategory=prefs.get("issuecategory_detail");
    String issueSubCategory=prefs.get("issueSubCategory_detail");
    assetIdCount=prefs.get("assetIdCount");

   // print("issue"+issueCategory+issueSubCategory+ticketSerialNo+serviceType);


    print("==The assetId count is"+assetIdCount+ticketSerialNo);

    var sentence = toBeginningOfSentenceCase(n);
    _controller.value = TextEditingValue(
      text: sentence,

      selection: TextSelection.fromPosition(
        TextPosition(offset: sentence.length),
      ),
    );
    if(email != null){
      email_controller.value = TextEditingValue(
        text: email,
        selection: TextSelection.fromPosition(
          TextPosition(offset: email.length),
        ),
      );
    }

    // phone_controller .value = TextEditingValue(
    //   text: phone,
    //   selection: TextSelection.fromPosition(
    //     TextPosition(offset: phone.length),
    //   ),
    // );

    address_controller .value = TextEditingValue(
      text: address,
      selection: TextSelection.fromPosition(
        TextPosition(offset: address.length),
      ),
    );


    assestWarranty_controller .value = TextEditingValue(
      text: assestWarranty,
      selection: TextSelection.fromPosition(
        TextPosition(offset: assestWarranty.length),
      ),
    );

    serviceType_controller .value = TextEditingValue(
      text: serviceType,
      selection: TextSelection.fromPosition(
        TextPosition(offset: serviceType.length),
      ),
    );
    // serviceType_controller .value = TextEditingValue(
    //   text: serviceType,
    //   selection: TextSelection.fromPosition(
    //     TextPosition(offset: serviceType.length),
    //   ),
    // );

    ticketSerialNo_controller .value = TextEditingValue(
      text: ticketSerialNo,
      selection: TextSelection.fromPosition(
        TextPosition(offset: ticketSerialNo.length),
      ),
    );


    issuecategory_controller .value = TextEditingValue(
      text: issueCategory,
      selection: TextSelection.fromPosition(
        TextPosition(offset: issueCategory.length),
      ),
    );

    issuesubcategory_controller .value = TextEditingValue(
      text: issueSubCategory,
      selection: TextSelection.fromPosition(
        TextPosition(offset: issueSubCategory.length),
      ),
    );


    // address_controller .value = TextEditingValue(
    //   text: address,
    //   selection: TextSelection.fromPosition(
    //     TextPosition(offset: address.length),
    //   ),
    // );
    //
    //
   setState(() {

   });

  }
}