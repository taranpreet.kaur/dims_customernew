import 'Notify.dart';

class NotificationResponse{
  Notify notification_details;

  NotificationResponse(
      {
        this.notification_details

      });

   factory NotificationResponse.fromJson(Map<String, dynamic> json) {
    return NotificationResponse(notification_details : Notify.fromJson(json["notification_details"]));
  }

}