class TicketListListData {   /// model class
  List<TicketHistoryModel> tickets;

  TicketListListData({this.tickets});

  TicketListListData.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      tickets = new List<TicketHistoryModel>();
      json['data'].forEach((v) {
        tickets.add(new TicketHistoryModel.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.tickets != null) {
      data['data'] = this.tickets.map((v) => v.toJson()).toList();
    }
    return data;
  }
}


class TicketHistoryModel {
  String sId;
  String ticketId;
  String rejectDateTm;
  String rejectionBy;
  String tmcId;
  String assetType;
  String ticketSerialNo;
  String status;
  String createdAt;
  String updatedAt;
  int iV;

  TicketHistoryModel(
      {this.sId,
        this.ticketId,
        this.rejectDateTm,
        this.rejectionBy,
        this.tmcId,
        this.assetType,
        this.ticketSerialNo,
        this.status,
        this.createdAt,
        this.updatedAt,
        this.iV});

  TicketHistoryModel.fromJson(Map<String, dynamic> json) {
    sId = json['_id'];
    ticketId = json['ticketId'];
    rejectDateTm = json['rejectDateTm'];
    rejectionBy = json['rejectionBy'];
    tmcId = json['tmcId'];
    assetType = json['assetType'];
    ticketSerialNo = json['ticketSerialNo'];
    status = json['status'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
    iV = json['__v'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_id'] = this.sId;
    data['ticketId'] = this.ticketId;
    data['rejectDateTm'] = this.rejectDateTm;
    data['rejectionBy'] = this.rejectionBy;
    data['tmcId'] = this.tmcId;
    data['assetType'] = this.assetType;
    data['ticketSerialNo'] = this.ticketSerialNo;
    data['status'] = this.status;
    data['createdAt'] = this.createdAt;
    data['updatedAt'] = this.updatedAt;
    data['__v'] = this.iV;
    return data;
  }
}
