import 'dart:developer';
import 'dart:ffi';

import 'package:customer/DomainSelection.dart';
import 'package:customer/EarlierTicketDetails.dart';
import 'package:customer/Services.dart';
import 'package:customer/TicketDetails.dart';
import 'package:date_time_picker/date_time_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:intl/intl.dart';

import 'globals.dart';

class EarlierCallTicketListing extends StatefulWidget {
  @override
  State<EarlierCallTicketListing> createState() => EarlierListState();
}

class EarlierListState extends State<EarlierCallTicketListing> {
  List<dynamic> ticketList = [];
  List<dynamic> filteredusers = [];

  @override
  void initState() {
    super.initState();
    getAllList();
  }

  @override
  Widget build(BuildContext context) {
    // Color color1 = _colorFromHex("#00ABC5");
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        //leading: Icon(Icons.arrow_back),
        title: Text("All List"),
        flexibleSpace: Image(
          image: AssetImage('assets/appbar_background.png'),
          fit: BoxFit.cover,
        ),
        backgroundColor: Colors.transparent,
      ),
      body: Column(
        children: [
          Container(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Align(
                    alignment: Alignment.topLeft,
                    child: Container(
                      width: MediaQuery.of(context).size.width * 0.90,
                      height: 45,
                      child: TextField(

                        decoration: InputDecoration(
                            contentPadding: EdgeInsets.all(15.0),
                            hintText: 'Search by Ticket Id & Asset Type',
                            hintStyle: TextStyle(fontSize: 14.0),
                            prefixIcon: Icon(Icons.search),
                            border: OutlineInputBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(25.0)),
                            )),
                        onChanged: (string) {
                          setState(() {
                            filteredusers = ticketList
                                .where((u) => (u['assetType']
                                        .toString()
                                        .toLowerCase()
                                        .contains(string.toLowerCase()) ||
                                    u['ticketSerialNo']
                                        .toString()
                                        .toLowerCase()
                                        .contains(string.toLowerCase()) || u['serviceItemNo']
                                .toString()
                                .toLowerCase()
                                .contains(string.toLowerCase())))
                                .toList();
                          });
              // this will search tickets from api if list not available in local list
                          if(filteredusers.length == 0 && !isSearchLoading){
                            getAllList(pServiceItemNo: string);
                          }
                        },
                      ),
                    ),
                  ),
                  /*Flexible(
                    child: SizedBox(
                      width: MediaQuery.of(context).size.width * 0.15,
                      child: IconButton(
                        icon: Icon(Icons.filter_alt_rounded),
                        // textColor: Colors.white,
                        onPressed: () {
                          print("Its working");
                          showFilterDialog();
                        },
                        // child:
                        // shape: CircleBorder(side: BorderSide(color: Colors.transparent)),
                      ),
                    ),
                  ),*/
                ],
              ),
            ),
          ),
          Container(
              padding: EdgeInsets.fromLTRB(20, 20, 20, 0),
              child: Row(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Expanded(
                    flex: 1,
                    child: Text("Ticket Id.",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontSize: 12.0,
                            fontWeight: FontWeight.bold,
                            color: Colors.black)),
                  ),

                  // SizedBox(width: 8,),
                  Expanded(
                      flex: 1,
                      child: Text("Asset Type",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontSize: 12.0,
                              fontWeight: FontWeight.bold,
                              color: Colors.black))),

                  // SizedBox(width: 20,),
                  Expanded(
                    flex: 1,
                    child: Text("Date",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontSize: 12.0,
                            fontWeight: FontWeight.bold,
                            color: Colors.black)),
                  ),

                  Expanded(
                    flex: 1,
                    child: Align(
                      child: Text("Status",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontSize: 12.0,
                              fontWeight: FontWeight.bold,
                              color: Colors.black)),
                    ),
                  ),
                ],
              )),
          _buildlist(),
        ],
      ),
    );
    // for (var i = 0; i < 10; i++) {
    // if (i==)
    //children.add(new ListTile());
    // }
  }

  Widget _buildlist() {
    return Expanded(
        child: ListView.separated(
      separatorBuilder: (context, index) => Divider(
          // color: Colors.black,
          ),
      itemCount: filteredusers.length,
      itemBuilder: (context, index) => Padding(
        padding: EdgeInsets.all(8.0),
        child: InkWell(
          onTap: () async {
            SharedPreferences sharedPreferences =
                await SharedPreferences.getInstance();
            sharedPreferences.setString(
                "ticket_id", filteredusers[index]['_id']);
            sharedPreferences.setString("createdAt", filteredusers[index]['createdAt']);
            sharedPreferences.setString("assetType", filteredusers[index]['assetType']);
            sharedPreferences.setString("status", filteredusers[index]['status']);
            sharedPreferences.setString("ticketid", filteredusers[index]['ticketSerialNo']);
            leading:Icon(Icons.arrow_back);
            //   Navigator.push(context,MaterialPageRoute(builder:(context)=> TicketListing()),);
            Navigator.of(context, rootNavigator: true).push(
              MaterialPageRoute(
                builder: (context) => ETicketDetails(
                  filteredusers[index]['_id'],
                ),
              ),
            );
          },
          child: Container(
            padding: EdgeInsets.fromLTRB(20, 20, 20, 0),
            //height: 30.0,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(
                  flex: 1,
                  child: Padding(
                      padding: EdgeInsets.fromLTRB(1, 1, 1, 1),
                      child:
                          Text(filteredusers[index]['ticketSerialNo'] ?? "NA")),
                ),
                Expanded(
                  flex: 1,
                  child: Text(filteredusers[index]['assetType'] ?? "NA"),
                ),
                Expanded(
                  flex: 1,
                  child: Text(dateCreation(filteredusers[index]['createdAt'])?? "NA"),
                ),
                Expanded(
                  flex: 1,
                  child: Center(
                    child: Text(
                        "${_status(filteredusers[index]['status'], this.filteredusers[index]['lastRemark'], this.filteredusers[index]['isPartRequested'])}"),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    ));
  }
  //// conver date from utc

  dateCreation(date) {
    var dateTime = DateFormat("yyyy-MM-ddTHH:mm:ssZ").parse(date, true);
    var dateLocal = dateTime.toLocal();


    DateTime parseDatess =
    new DateFormat("yyyy-MM-ddTHH:mm:ssZ").parse(date);
    String formattedDate = DateFormat('yy-MM-dd hh:mm:ss aaa').format(dateLocal);
    print("print Date"+formattedDate);


    var newFormat = DateFormat("dd-MM-yyyy hh:mm:ss aaa");
    String updatedDtss = newFormat.format(dateLocal);
    return updatedDtss;
  }

  void showFilterDialog() {
    // filterText = "";
    showDialog(
      context: context,
      builder: (context) {
        return Dialog(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
          elevation: 16,
          child: Container(
            width: 300,
            height: 500,
            padding: EdgeInsets.fromLTRB(10, 20, 10, 10),
            alignment: Alignment.centerLeft,
            child: Column(
              children: [
                Align(
                    alignment: Alignment.center,
                    child: Text(
                      "Status",
                      style: TextStyle(
                          fontSize: 17.0,
                          fontWeight: FontWeight.bold,
                          color: Color(0xFF919191)),
                    )),
                new Container(
                  child: Column(
                    children: [
                      Container(
                        child: InkWell(
                          onTap: () {
                            print("ALL");

                            buttonList(["ALL"]);

                            Navigator.of(context).pop();
                          },
                          child: Text('All Tickets'),
                        ),
                        padding: EdgeInsets.only(top: 20.0),
                      ),
                      Container(
                        child: InkWell(
                          onTap: () {
                            print("New Ticket");

                            buttonList([
                              "ENG_NEW",
                              "ENG_REJECT",
                              "RIM_NEW",
                              "IM_NEW",
                              "PENG_NEW",
                              "PENG_REJECT",
                            ],filterType: "NEW");

                            Navigator.of(context).pop();
                          },
                          child: Text('New'),
                        ),
                        padding: EdgeInsets.only(top: 20.0),
                      ),
                      Container(
                        child: InkWell(
                          onTap: () {
                            buttonList([
                              "RIM_ACCEPT",
                              "RIM_NEW",
                              "RIM_WIP",
                              "RIM_PDC",
                              "RIM_PA",
                              "RIM_FAULTPRST",
                              "RIM_TRNSIM",
                              "RIM_TOE",
                              "RIM_FAULTPR",
                              "RIM_TIM",
                              "RIM_TENG",
                              "RIM_PART_AUTH",
                              "RIM_FPR",
                              "RIM_FPR_SR",
                              "RIM_RTN_ENG",
                              "RIM_STD_REQ",
                              "RIM_FPR_STD_REQ",
                              "ENG_RAS",
                              "ENG_NEW",
                              "ENG_PFS",
                              "ENG_ACCEPT",
                              "ENG_SPAREQ",
                              "ENG_SPR_REQ",
                              "ENG_WIP",
                              "ENG_PDC",
                              "ENG_TRANSIM",
                              "ENG_TRANS_IM",
                              "ENG_TRNS_IM",
                              "ENG_WPS",
                              "PENG_RAS",
                              "PENG_SPAREQ",
                              "PENG_TRNS_PART",
                              "PENG_WPS",
                              "PENG_SPR_REQ",
                              "PENG_WIP",
                              "PENG_NEW",
                              "PENG_PDC"
                                  "PENG_PFS",
                              "PENG_ACCEPT",
                              "PENG_TRANSIM",
                              "PENG_TRANS_IM",
                              "PENG_TRNS_IM",
                              "TRC_NEW",
                              "TRC_WIP",
                              "TRC_REPAIRED",
                              "TRC_NOT_REPAIRABLE",
                              "PART_WPS",
                              "PART_ASGN_ENGNR",
                              "PART_SPR_REQ",
                              "PART_ASGN_PENGNR",
                              "PART_ASGN_PRTNR",
                              "PART_WIP",
                              "PART_REJECT",
                              "PART_ACCEPT",
                              "PART_NEW",
                              "PART_PDC",
                              "IM_NEW",
                              "RIM_TIM",
                              "IM_WIP",
                              "IM_PDC",
                              "IM_ASGND_PRTNR",
                              "IM_ASGND_ENGNR",
                              "IM_SPART_REQ",
                              "IM_TRANSFER_RIM",
                              "IM_WPS",
                              "STR_NEW",
                              "STR_PDC",
                              "STR_UP",
                              "STR_WIP",
                              "STR_SPR_BP",
                              "STR_SPR_BP",
                              "STR_SPR_DSPTH",
                              "STR_SPR_DSPTH-ER",
                              "STR_STDBY_DSPTH",
                              "STR_FLTY_PFR",
                              "STR_TRNS_IM",
                              "STR_TRNS_TRC_RPR",
                              "STR_TRNS_PRTNR_RPR",
                              "STR_FLTY_NTBP",
                              "STR_TBD",
                              "STR_PKUP",
                              "STR_HOVR_PTR_RPR",
                              "STR_HOVR_TRC_RPR",
                              "STR_RESOLVED",
                              "STR_DEL_ER",
                              "STR_DEL_ENR",
                              "STR_REP",
                              "STR_UREP",
                              "STR_REP",
                              "STR_UREP"
                            ], filterType: "WIP");
                            Navigator.of(context).pop();
                          }, // Handle your callback
                          child: Text('WIP'),
                        ),
                        padding: EdgeInsets.only(top: 20.0),
                      ),
                      Container(
                        child: InkWell(
                          onTap: () {
                            buttonList(["ENG_PFS", "PENG_PFS"]);
                            Navigator.of(context).pop();
                          }, // Handle your callback
                          child: Text('Engineer on the way'),
                        ),
                        padding: EdgeInsets.only(top: 20.0),
                      ),
                      Container(
                        child: InkWell(
                          onTap: () {
                            buttonList(["ENG_ACCEPT", "PENG_ACCEPT"]);
                            Navigator.of(context).pop();
                          }, // Handle your callback
                          child: Text('Assigned To Engineer'),
                        ),
                        padding: EdgeInsets.only(top: 20.0),
                      ),
                      Container(
                        child: InkWell(
                          onTap: () {
                            print("OnHold Ticket");
                            buttonList([
                              "RIM_PDC",
                              "ENG_PDC",
                              "PENG_PDC",
                              "IM_PDC",
                              "STR_PDC",
                              "PART_PDC"
                            ]);
                            Navigator.of(context).pop();
                          }, // Handle your callback
                          child: Text('OnHold'),
                        ),
                        padding: EdgeInsets.only(top: 20.0),
                      ),
                      Container(
                        child: InkWell(
                          onTap: () {
                            print("Under Observation Ticket");

                            buttonList([
                              "RIM_UOB",
                              "RIM_UNDER_OBSERVATION",
                              "ENG_UOB",
                              "PENG_UOB",
                              "IM_OBS",
                              "PART_UOB"
                            ]);
                            Navigator.of(context).pop();
                          }, // Handle your callback
                          child: Text('Under Observation'),
                        ),
                        padding: EdgeInsets.only(top: 20.0),
                      ),
                      Container(
                        child: InkWell(
                          onTap: () {
                            print("Standby Provided Ticket");

                            buttonList([
                              "ENG_STDBY_INSTL",
                              "PENG_STDBY_INSTL",
                              // "STR_DEL_ENR",
                              "Standby Delivered Faulty Picked up",
                              "Standby Delivered Faulty pending to pick",
                              "Standby Delivered",
                              "STR_STDBY_DLVR",
                              "STR_STDBY-DLVR_FP",
                              "Standby Delivered and Faulty Picked"
                            ]);
                            Navigator.of(context).pop();
                          }, // Handle your callback
                          child: Text('Standby Provided'),
                        ),
                        padding: EdgeInsets.only(top: 20.0),
                      ),
                      /*Container(
                        child: InkWell(
                          onTap: () {
                            print("Standby Given Ticket");

                            buttonList([

                            ]);
                            Navigator.of(context).pop();
                          }, // Handle your callback
                          child: Text('Standby Given'),
                        ),
                        padding: EdgeInsets.only(top: 20.0),
                      ),*/
                    ],
                  ),

                  //   ],
                  // ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  buttonList(List<String> statusList, {String filterType}) {
    filteredusers.clear();
    Future.delayed(Duration.zero, () {
      if (statusList[0] == "ALL") {
        filteredusers.addAll(ticketList);
      } else {
        ticketList.forEach((element) {

          if (statusList.contains(element['status'].toString())) {
            if (filterType == "WIP") {
              if (_status(
                      element['status'].toString(),
                      element['lastRemark'].toString(),
                      element['isPartRequested'].toString(),
                      ) ==
                  "WIP") {
                filteredusers.add(element);
              }
            } else if (filterType == "NEW") {
              if (_status(
                      element['status'].toString(),
                      element['lastRemark'].toString(),
                      element['isPartRequested'].toString(),
                      ) ==
                  "NEW") {
                filteredusers.add(element);
              }
            }
          }
        });
      }

      // debugger();
      print("filterlength => " + filteredusers.length.toString());

      setState(() {});
    });
  }


  bool isSearchLoading = false;

  Future<String> getAllList({String pServiceItemNo}) async {
    print("api is called");
    isSearchLoading = true;
    filteredusers.clear();
    ticketList.clear();
    setState(() {

    });
    final prefs = await SharedPreferences.getInstance();

    var aseetId = prefs.get("assetId");
    //var ticketSerialNo=prefs.get("ticketSerialNo_detail");
    var ServiceitemNo = prefs.get("serviceItemNo");
    var createdat = prefs.get("createdAt");
    var assetserial = prefs.get("assetSerial");
    if(pServiceItemNo != null){
      ServiceitemNo = pServiceItemNo;
      aseetId = null;
    }

    List<dynamic> apiTicketList = await Services.getTicketHistorylist(
        assetsId: aseetId,
        assetSerial: assetserial,
        createdAt: createdat,
        serviceItemNo: ServiceitemNo);

    isSearchLoading = false;
    if(apiTicketList != null){
      ticketList.addAll(apiTicketList);

      filteredusers.addAll(apiTicketList);
      print("==api l;ist"+apiTicketList.length.toString());
    }else{
      print("Unable to get tickets");
    }

  //  print(filteredusers.length.toString());
    //
    // if (Globals.fromScreen.isNotEmpty &&
    //     Globals.fromScreen == "resolved_tickets") {
    //   apiTicketList.forEach((element) {
    //     ticketList.add(element);
    //   });
    // } else {
    //   apiTicketList.forEach((element) {
    //     if (_status(element['status'], element['lastRemark'],
    //             element['isPartRequested'], element['isNewTicket']) ==
    //         "RESOLVED") {
    //     } else {
    //       ticketList.add(element);
    //     }
    //   });
    // }

    print("length =. " + ticketList.length.toString());

    setState(() {});
  }

  _status(status, lastRemark, isPartRequested) {
    if (status == "RIM_NEW") {
      if (isPartRequested == "1") {
        return 'WIP';
      } else {
        return 'NEW';
      }
      // _getColorByEvent('New');

    } else if (status == "RIM_ACCEPT") {
      // _getColorByEvent('WIP');
      return 'WIP';
    } else if (status == "RIM_WIP") {
      // _getColorByEvent('WIP');
      return "WIP";
    } else if (status == "RIM_UOB") {
      //_getColorByEvent('Under Observation');
      return "Under Observation";
    } else if (status == "RIM_PA") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "RIM_PDC") {
      //_getColorByEvent('HOLD');
      return "On HOLD";
    } else if (status == "RIM_FAULTPRST") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "RIM_RESOLVED") {
      //_getColorByEvent('RESOLVED');
      return "RESOLVED";
    } else if (status == "RIM_TRNSIM") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "RIM_TOE") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "RIM_FAULTPR") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "RIM_UNDER_OBSERVATION") {
      return "Under Observation";
    } else if (status == "RIM_TIM") {
      return "WIP";
    } else if (status == "RIM_TENG") {
      return "WIP";
    } else if (status == "RIM_PART_AUTH") {
      return "WIP";
    } else if (status == "RIM_FPR") {
      return "WIP";
    } else if (status == "RIM_FPR_SR") {
      return "WIP";
    } else if (status == "RIM_RTN_ENG") {
      return "WIP";
    } else if (status == "RIM_STD_REQ") {
      return "WIP";
    } else if (status == "RIM_FPR_STD_REQ") {
      return "WIP";
    }
    if (status == "ENG_NEW") {
      //_getColorByEvent('New');
      return 'NEW';
    } else if (status == "ENG_ACCEPT") {
      //_getColorByEvent('Assign to Aggent');
      return 'Assigned to Engineer';
    } else if (status == "ENG_REJECT") {
      //_getColorByEvent('New');
      return "NEW";
    } else if (status == "ENG_PFS") {
      //_getColorByEvent('Aggent on the way');
      return "Engineer on the way";
    } else if (status == "ENG_RAS") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "ENG_RESOLVED") {
      //_getColorByEvent('RESOLVED');

      return "RESOLVED";
    } else if (status == "ENG_UOB") {
      //_getColorByEvent('WIP');
      return "Under Observation";
    } else if (status == "ENG_SPAREQ") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "ENG_SPR_REQ") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "ENG_WIP") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "ENG_PDC") {
      //_getColorByEvent('WIP');
      return "On Hold";
    } else if (status == "ENG_RESO") {
      //_getColorByEvent('WIP');

      return "RESOLVED";
    } else if (status == "ENG_TRANSIM") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "ENG_TRANS_IM") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "ENG_TRNS_IM") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "ENG_WPS") {
      //_getColorByEvent('WIP');
      return "WIP";
    }
    if (status == "PENG_NEW") {
      //_getColorByEvent('New');
      return 'NEW';
    } else if (status == "PENG_ACCEPT") {
      //_getColorByEvent('Assign to Aggent');
      return 'Assign to Engineer';
    } else if (status == "PENG_REJECT") {
      //_getColorByEvent('New');
      return "NEW";
    } else if (status == "PENG_PFS") {
      //_getColorByEvent('Aggent on the way');
      return "Engineer on the way";
    } else if (status == "PENG_RAS") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "PENG_RESOLVED") {
      //_getColorByEvent('RESOLVED');

      return "RESOLVED";
    } else if (status == "PENG_UOB") {
      //_getColorByEvent('WIP');
      return "Under Observation";
    } else if (status == "PENG_SPAREQ") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "PENG_TRNS_PART") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "PENG_WPS") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "PENG_SPR_REQ") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "PENG_WIP") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "ENG_STDBY_INSTL") {
      //_getColorByEvent('WIP');
      return "Standby Provided";
    } else if (status == "PENG_PDC") {
      //_getColorByEvent('WIP');
      return "On Hold";
    } else if (status == "PENG_RESO") {
      //_getColorByEvent('WIP');

      return "RESOLVED";
    } else if (status == "PENG_STDBY_INSTL") {
      //_getColorByEvent('WIP');
      return "Standby Provided";
    } else if (status == "PENG_TRANSIM") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "PENG_TRANS_IM") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "PENG_TRNS_IM") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "Standby Delivered") {
      //_getColorByEvent('WIP');
      return "Standby Provided";
    } else if (status == "Standby Delivered and Faulty Picked") {
      //_getColorByEvent('WIP');
      return "Standby Provided";
    } else if (status == "IM_NEW") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "RIM_TIM") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "IM_WIP") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "IM_ASGND_PRTNR") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "IM_ASGND_ENGNR") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "IM_PDC") {
      //_getColorByEvent('WIP');
      return "On Hold";
    } else if (status == "IM_OBS") {
      //_getColorByEvent('WIP');
      return "Under Observation";
    } else if (status == "IM_RESOLVED") {
      //_getColorByEvent('WIP');

      return "RESOLVED";
    } else if (status == "IM_SPART_REQ") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "IM_TRANSFER_RIM") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "IM_WPS") {
      //_getColorByEvent('WIP');
      return "WIP";
    }

    if (status == "STR_NEW") {
      //_getColorByEvent('WIP');
      return 'WIP';
    }
    if (status == "STR_UP") {
      //_getColorByEvent('WIP');
      return 'WIP';
    } else if (status == "STR_WIP") {
      //_getColorByEvent('WIP');
      return 'WIP';
    } else if (status == "STR_SPR_BP") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "STR_SPR_DSPTH") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "STR_SPR_DLVR-R") {
      //_getColorByEvent('WIP');

      return "RESOLVED";
    } else if (status == "STR_SPR_DSPTH-ER") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "STR_STDBY_DSPTH") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "STR_STDBY_DLVR") {
      //_getColorByEvent('WIP');
      return "Standby Provided";
    } else if (status == "STR_STDBY_PICK") {
      //_getColorByEvent('WIP');

      return "RESOLVED";
    } else if (status == "STR_FLTY_PFR") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "STR_STDBY-DLVR_FP") {
      //_getColorByEvent('WIP');
      return "Standby Provided";
    } else if (status == "STR_REP_DLVRD_FP") {
      //_getColorByEvent('WIP');
      return "RESOLVED";
    } else if (status == "STR_REPD_NTBP") {
      //_getColorByEvent('WIP');
      return "RESOLVED";
    } else if (status == "STR_TRNS_IM") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "STR_TRNS_TRC_RPR") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "STR_TRNS_PRTNR_RPR") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "STR_FLTY_NTBP") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "STR_TBD") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "STR_PKUP") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "STR_PDC") {
      //_getColorByEvent('WIP');
      return "On Hold";
    } else if (status == "STR_DEL_ENR") {
      if (lastRemark == "Spare Delivered Faulty Picked up") {
        return "WIP";
      } else if (lastRemark == "Spare Delivered Faulty pending to pick") {
        return "WIP";
      } else if (lastRemark == "Standby Delivered Faulty Picked up") {
        return "Standby Provided";
      } else if (lastRemark == "Standby Delivered Faulty pending to pick") {
        return "Standby Provided";
      } else if (lastRemark == "Unit replacement Delivered Faulty Picked up") {
        return "WIP";
      } else if (lastRemark ==
          "Unit replacement Delivered Faulty pending to pick") {
        return "WIP";
      } else if (lastRemark == "Spare delivered after repair") {
        return "WIP";
      } else if (lastRemark == "Full unit delivered after repair") {
        return "WIP";
      }
      //_getColorByEvent('WIP');

    } else if (status == "STR_DEL_ER") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "STR_RESOLVED") {
      //_getColorByEvent('WIP');

      return "Resolved";
    } else if (status == "STR_HOVR_TRC_RPR") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "STR_HOVR_PTR_RPR") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "STR_REP") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "STR_UREP") {
      //_getColorByEvent('WIP');
      return "WIP";
    }

    if (status == "PART_NEW") {
      //_getColorByEvent('WIP');
      return 'WIP';
    }
    if (status == "PART_ACCEPT") {
      //_getColorByEvent('WIP');
      return 'WIP';
    }
    if (status == "PART_REJECT") {
      //_getColorByEvent('WIP');
      return 'WIP';
    } else if (status == "PART_WIP") {
      //_getColorByEvent('WIP');
      return 'WIP';
    } else if (status == "PART_ASGN_PRTNR") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "PART_ASGN_PENGNR") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "PART_SPR_REQ") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "PART_ASGN_ENGNR") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "PART_PDC") {
      //_getColorByEvent('WIP');
      return "On Hold";
    } else if (status == "PART_RESOLVED") {
      //_getColorByEvent('WIP'); // TICKET_CANCELLED
      //
      return "RESOLVED";
    } else if (status == "TICKET_CANCELLED") {
      //_getColorByEvent('WIP');
      return "Ticket Cancelled";
    } else if (status == "PART_WPS") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "PART_UOB") {
      //_getColorByEvent('WIP');
      return "Under Observation";
    } else if (status == "TRC_NEW") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "TRC_WIP") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "TRC_REPAIRED") {
      //_getColorByEvent('WIP');
      return "WIP";
    } else if (status == "TRC_NOT_REPAIRABLE") {
      //_getColorByEvent('WIP');
      return "WIP";
    }
  }
}
