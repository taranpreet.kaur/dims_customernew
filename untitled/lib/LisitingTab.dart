import 'package:customer/ResolveTickets.dart';
import 'package:customer/SearchListViewExample.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'globals.dart';

class LisitingTab extends StatefulWidget{

  LisitingTab();

  @override
  State<LisitingTab> createState() => _LisitingTabState();
}

class _LisitingTabState extends State<LisitingTab> with TickerProviderStateMixin{


  TabController tabController = null;


   @override
  void initState() {
    // TODO: implement initState
    super.initState();
    tabController = TabController(length: 2, vsync: this, initialIndex: Globals.filterText == "TODAYRESOLVED" ? 1 : 0);

  }


  @override
  Widget build(BuildContext context) {



    Color color1 = _colorFromHex("#00ABC5");

    return MaterialApp(
        home: DefaultTabController(length: 2,
            child: Scaffold(
              resizeToAvoidBottomInset: false,
          appBar: AppBar(title: Text("Ticket Listing"),centerTitle: true,

            /*leading: InkWell(
                onTap: (){
                //  Navigator.of(context).pop();
                  //
                  // }


                },
                child: Icon(Icons.arrow_back)),*/
            //title: Text("Ticket Details"),


            flexibleSpace: Image(
              image: AssetImage('assets/appbar_background.png'),
              fit: BoxFit.cover,
            ),
            backgroundColor: Colors.transparent,
            bottom:PreferredSize(
              preferredSize: new Size(20.0, 20.0),
              child: TabBar(
                controller: tabController,
                indicatorColor:Colors.white,
                tabs: [
                  Tab(text: 'All Tickets'),
                  Tab(text: 'Resolve Tickets',)
                ],
              ),
            ),
          ),
          body:
          TabBarView(
            controller: tabController,
            children: [
              UserFilterDemo(),
              ResolveTickets()
            ],
          ),
        )
        )
    );
  }

  Color _colorFromHex(String hexColor) {
    final hexCode = hexColor.replaceAll('#', '');
    return Color(int.parse('FF$hexCode', radix: 16));
  }
}