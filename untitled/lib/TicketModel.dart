import 'Location.dart';

class TicketModel {
  Location location;
  String assetType;
  String assetId;
  String id;
  String status;
  String lastStatus;
  String lastRemark;
  String ticketSerialNo;
  Engineer engineer;
  String isPartRequested;
  bool isNewTicket;

  TicketModel(
      {this.location,
      this.assetType,
      this.assetId,
      this.id,
      this.status,
        this.lastStatus,
        this.lastRemark,
      this.ticketSerialNo,
        this.isNewTicket,
      this.engineer,String isPartRequested});

  factory TicketModel.fromJson(Map<String, dynamic> json) {
    return TicketModel(
        location: new Location.fromJson(json['location']),
        assetType: json['assetType'],
        assetId: json['assetId'],
        id: json['_id'],
        status: json['status'],
        lastStatus: json['lastStatus'],
        lastRemark: json['lastRemark'],
        ticketSerialNo: json['ticketSerialNo'],
        isNewTicket: json['isNewTicket'],
        isPartRequested:json['isPartRequested'].toString(),
        engineer: json['engineer'] != null
            ? new Engineer.fromJson(json['engineer'])
            : null);
  }

  static Map<String, dynamic> toMap(TicketModel music) => {
        'location': music.location,
        'assetType': music.assetType,
        'assetId': music.assetId,
        '_id': music.id,
        'status': music.status,
        'lastStatus': music.lastStatus,
        'lastRemark': music.lastRemark,
        'isPartRequested': music.isPartRequested,
        'ticketSerialNo': music.ticketSerialNo,
        'engineer': music.engineer,
       'isNewTicket': music.isNewTicket
      };
}

class Engineer {
  String _sId;
  String _name;
  String _id;

  Engineer({String sId, String name, String id}) {
    this._sId = sId;
    this._name = name;
    this._id = id;
  }

  String get sId => _sId;

  set sId(String sId) => _sId = sId;

  String get name => _name;

  set name(String name) => _name = name;

  String get id => _id;

  set id(String id) => _id = id;

  Engineer.fromJson(Map<String, dynamic> json) {
    _sId = json['_id'];
    _name = json['name'];
    _id = json['id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_id'] = this._sId;
    data['name'] = this._name;
    data['id'] = this._id;
    return data;
  }
}
