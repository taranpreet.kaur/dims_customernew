import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:customer/common_utils.dart';
import 'Services.dart';

class EarlierCallHistory extends StatefulWidget {
  final String assetsId;
  EarlierCallHistory({this.assetsId});

  @override
  EarlierCallHistoryView createState() => EarlierCallHistoryView();
}

class EarlierCallHistoryView extends State<EarlierCallHistory> {
  List<dynamic> dataList = [];
  List<String> showFilterList = [
    "Customer Info",
    "Agreement Info",
    "Incident Info",
  ];

  //List<TicketHistoryData>  historyDetailsList = [];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    print("AAA => "+widget.assetsId.toString());
    getTicketHistoryDetailsApi();
  }

  @override
  Widget build(BuildContext context) {
    final mainListview = ListView.builder(
        itemCount: dataDetailsList.length,
        shrinkWrap: true,
        itemBuilder: (context, index) {
          return mainHeader(
              title: dataDetailsList[index]['title'],
              dynamicList: dataDetailsList[index]['details_list']);
        });

    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text("Earlier Call History"),
        flexibleSpace: Image(
          image: AssetImage('assets/appbar_background.png'),
          fit: BoxFit.cover,
        ),
        backgroundColor: Colors.transparent,
      ),
      body: mainListview,
    );
  }

  // void createDetailsList(){
  //   TicketHistoryData ticketHistoryData = new TicketHistoryData();
  //   ticketHistoryData.title = "Customer Info";
  //   List<TicketHistoryDetails> customerInfoList = [];
  //   TicketHistoryDetails cTicketHistoryDetails1  = new TicketHistoryDetails("Customer Name", "custName", "");
  //   TicketHistoryDetails ticketHistoryDetails2  = new TicketHistoryDetails("Email", "custName", "");
  //   TicketHistoryDetails ticketHistoryDetails3  = new TicketHistoryDetails("Address", "custName", "");
  //   TicketHistoryDetails ticketHistoryDetails4  = new TicketHistoryDetails("Customer Type", "custName", "");
  // }

  Future<void> getTicketHistoryDetailsApi() async {
    dataList = await Services.getTicketHistoryDetails(assetsId: widget.assetsId);

    if (dataList != null && dataList.length > 0) {
      dataList.forEach((element) {
        dataDetailsList.forEach((detailsElement) {
          List<dynamic>   detailsList = detailsElement['details_list'];
         detailsList.forEach((listElement) {
           // print("Keys => "+listElement['key']);
           if(listElement['key'] == "address"){
             listElement['value'] = element[listElement['key']]['line1']
                 +","+element[listElement['key']]['line2']
                 +","+element[listElement['key']]['city']+"," +
                 element[listElement['key']]['state']+","+element[listElement['key']]['postalcode'];
           }else if(element[listElement['key']] != null){
             listElement['value'] = element[listElement['key']];
           }else{
             listElement['value'] = "Data not available";
           }
         });
        });
      });
    } else {
      Fluttertoast.showToast(msg: "Failed to get details");
    }
  }

  Widget mainHeader({String title, List<dynamic> dynamicList}) {
    return Card(
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
          Text(title,style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 15.0
          ),),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: List.generate(dynamicList.length, (index) {
              return getListItem(
                  label: dynamicList[index]['label'],
                  value: dynamicList[index]['value']);
            }),
          )
        ],),
      ),
    );
  }

  Widget getListItem({String label, String value}) {
    final textEditingController = TextEditingController();
    textEditingController.text = value ?? "NA";
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            padding: EdgeInsets.all(10),
            child: Text(
              label ?? "NA" ,
              textAlign: TextAlign.left,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(fontWeight: FontWeight.normal),
            ),
          ),
          Container(
            padding: EdgeInsets.all(10),
            child: TextField(
              controller: textEditingController,
              enabled: false,
              textAlign: TextAlign.left,
              style: TextStyle(fontWeight: FontWeight.bold),
              readOnly: true,
            ),
          ),
        ],
      ),
    );
  }
}

List<dynamic> dataDetailsList = [
  {
    "title": "Ticket Details",
    "details_list": [
      {"label": "AssetId", "key": "assetId", "value": ""},
      {"label": "TicketId", "key": "ticketSerialNo", "value": ""},
      {"label": "Asset Type", "key": "assetType", "value": ""},
      {"label": "Status", "key": "status", "value": ""}    /// sir 5 mnts m aaya ok
    ]
  },
  {
    "title": "Customer Info",
    "details_list": [
      {"label": "Customer name", "key": "custName", "value": ""},
      {"label": "Email", "key": "email", "value": ""},
      {"label": "Address", "key": "address", "value": ""},
    //  {"label": "Customer Type", "key": "custName", "value": ""}    /// sir 5 mnts m aaya ok
    ]
  },
  {
    "title": "Agreement Info",
    "details_list": [
      {"label": "Service Type", "key": "callType", "value": ""},
      {"label": "Asset AMC/Warranty", "key": "assestWarranty", "value": ""},
      {"label": "Ticket Serial No", "key": "ticketSerialNo", "value": ""},

    ]
  },
  {
    "title": "Incident Info",
    "details_list": [
      {"label": "Issue Category", "key": "issueCategory", "value": ""},
      {"label": "Issue Sub-category", "key": "issueSubCategory", "value": ""},

    ]
  }
];

_status(status, lastRemark, isPartRequested) {
  if (status == "RIM_NEW") {
    if (isPartRequested == "1") {
      return 'WIP';
    } else {
      return 'NEW';
    }
    // _getColorByEvent('New');

  } else if (status == "RIM_ACCEPT") {
    // _getColorByEvent('WIP');
    return 'WIP';
  } else if (status == "RIM_WIP") {
    // _getColorByEvent('WIP');
    return "WIP";
  } else if (status == "RIM_UOB") {
    //_getColorByEvent('Under Observation');
    return "Under Observation";
  } else if (status == "RIM_PA") {
    //_getColorByEvent('WIP');
    return "WIP";
  } else if (status == "RIM_PDC") {
    //_getColorByEvent('HOLD');
    return "On HOLD";
  } else if (status == "RIM_FAULTPRST") {
    //_getColorByEvent('WIP');
    return "WIP";
  } else if (status == "RIM_RESOLVED") {
    //_getColorByEvent('RESOLVED');
    return "RESOLVED";
  } else if (status == "RIM_TRNSIM") {
    //_getColorByEvent('WIP');
    return "WIP";
  } else if (status == "RIM_TOE") {
    //_getColorByEvent('WIP');
    return "WIP";
  } else if (status == "RIM_FAULTPR") {
    //_getColorByEvent('WIP');
    return "WIP";
  } else if (status == "RIM_UNDER_OBSERVATION") {
    return "Under Observation";
  } else if (status == "RIM_TIM") {
    return "WIP";
  } else if (status == "RIM_TENG") {
    return "WIP";
  } else if (status == "RIM_PART_AUTH") {
    return "WIP";
  } else if (status == "RIM_FPR") {
    return "WIP";
  } else if (status == "RIM_FPR_SR") {
    return "WIP";
  } else if (status == "RIM_RTN_ENG") {
    return "WIP";
  } else if (status == "RIM_STD_REQ") {
    return "WIP";
  } else if (status == "RIM_FPR_STD_REQ") {
    return "WIP";
  }
  if (status == "ENG_NEW") {
    //_getColorByEvent('New');
    return 'NEW';
  } else if (status == "ENG_ACCEPT") {
    //_getColorByEvent('Assign to Aggent');
    return 'Assigned to Engineer';
  } else if (status == "ENG_REJECT") {
    //_getColorByEvent('New');
    return "NEW";
  } else if (status == "ENG_PFS") {
    //_getColorByEvent('Aggent on the way');
    return "Engineer on the way";
  } else if (status == "ENG_RAS") {
    //_getColorByEvent('WIP');
    return "WIP";
  } else if (status == "ENG_RESOLVED") {
    //_getColorByEvent('RESOLVED');

    return "RESOLVED";
  } else if (status == "ENG_UOB") {
    //_getColorByEvent('WIP');
    return "Under Observation";
  } else if (status == "ENG_SPAREQ") {
    //_getColorByEvent('WIP');
    return "WIP";
  } else if (status == "ENG_SPR_REQ") {
    //_getColorByEvent('WIP');
    return "WIP";
  } else if (status == "ENG_WIP") {
    //_getColorByEvent('WIP');
    return "WIP";
  } else if (status == "ENG_PDC") {
    //_getColorByEvent('WIP');
    return "On Hold";
  } else if (status == "ENG_RESO") {
    //_getColorByEvent('WIP');

    return "RESOLVED";
  } else if (status == "ENG_TRANSIM") {
    //_getColorByEvent('WIP');
    return "WIP";
  } else if (status == "ENG_TRANS_IM") {
    //_getColorByEvent('WIP');
    return "WIP";
  } else if (status == "ENG_TRNS_IM") {
    //_getColorByEvent('WIP');
    return "WIP";
  } else if (status == "ENG_WPS") {
    //_getColorByEvent('WIP');
    return "WIP";
  }
  if (status == "PENG_NEW") {
    //_getColorByEvent('New');
    return 'NEW';
  } else if (status == "PENG_ACCEPT") {
    //_getColorByEvent('Assign to Aggent');
    return 'Assign to Engineer';
  } else if (status == "PENG_REJECT") {
    //_getColorByEvent('New');
    return "NEW";
  } else if (status == "PENG_PFS") {
    //_getColorByEvent('Aggent on the way');
    return "Engineer on the way";
  } else if (status == "PENG_RAS") {
    //_getColorByEvent('WIP');
    return "WIP";
  } else if (status == "PENG_RESOLVED") {
    //_getColorByEvent('RESOLVED');

    return "RESOLVED";
  } else if (status == "PENG_UOB") {
    //_getColorByEvent('WIP');
    return "Under Observation";
  } else if (status == "PENG_SPAREQ") {
    //_getColorByEvent('WIP');
    return "WIP";
  } else if (status == "PENG_TRNS_PART") {
    //_getColorByEvent('WIP');
    return "WIP";
  } else if (status == "PENG_WPS") {
    //_getColorByEvent('WIP');
    return "WIP";
  } else if (status == "PENG_SPR_REQ") {
    //_getColorByEvent('WIP');
    return "WIP";
  } else if (status == "PENG_WIP") {
    //_getColorByEvent('WIP');
    return "WIP";
  } else if (status == "ENG_STDBY_INSTL") {
    //_getColorByEvent('WIP');
    return "Standby Provided";
  } else if (status == "PENG_PDC") {
    //_getColorByEvent('WIP');
    return "On Hold";
  } else if (status == "PENG_RESO") {
    //_getColorByEvent('WIP');

    return "RESOLVED";
  } else if (status == "PENG_STDBY_INSTL") {
    //_getColorByEvent('WIP');
    return "Standby Provided";
  } else if (status == "PENG_TRANSIM") {
    //_getColorByEvent('WIP');
    return "WIP";
  } else if (status == "PENG_TRANS_IM") {
    //_getColorByEvent('WIP');
    return "WIP";
  } else if (status == "PENG_TRNS_IM") {
    //_getColorByEvent('WIP');
    return "WIP";
  } else if (status == "Standby Delivered") {
    //_getColorByEvent('WIP');
    return "Standby Given";
  } else if (status == "Standby Delivered and Faulty Picked") {
    //_getColorByEvent('WIP');
    return "Standby Given";
  } else if (status == "IM_NEW") {
    //_getColorByEvent('WIP');
    return "WIP";
  } else if (status == "RIM_TIM") {
    //_getColorByEvent('WIP');
    return "WIP";
  } else if (status == "IM_WIP") {
    //_getColorByEvent('WIP');
    return "WIP";
  } else if (status == "IM_ASGND_PRTNR") {
    //_getColorByEvent('WIP');
    return "WIP";
  } else if (status == "IM_ASGND_ENGNR") {
    //_getColorByEvent('WIP');
    return "WIP";
  } else if (status == "IM_PDC") {
    //_getColorByEvent('WIP');
    return "On Hold";
  } else if (status == "IM_OBS") {
    //_getColorByEvent('WIP');
    return "Under Observation";
  } else if (status == "IM_RESOLVED") {
    //_getColorByEvent('WIP');

    return "RESOLVED";
  } else if (status == "IM_SPART_REQ") {
    //_getColorByEvent('WIP');
    return "WIP";
  } else if (status == "IM_TRANSFER_RIM") {
    //_getColorByEvent('WIP');
    return "WIP";
  } else if (status == "IM_WPS") {
    //_getColorByEvent('WIP');
    return "WIP";
  }

  if (status == "STR_NEW") {
    //_getColorByEvent('WIP');
    return 'WIP';
  }
  if (status == "STR_UP") {
    //_getColorByEvent('WIP');
    return 'WIP';
  } else if (status == "STR_WIP") {
    //_getColorByEvent('WIP');
    return 'WIP';
  } else if (status == "STR_SPR_BP") {
    //_getColorByEvent('WIP');
    return "WIP";
  } else if (status == "STR_SPR_DSPTH") {
    //_getColorByEvent('WIP');
    return "WIP";
  } else if (status == "STR_SPR_DLVR-R") {
    //_getColorByEvent('WIP');

    return "RESOLVED";
  } else if (status == "STR_SPR_DSPTH-ER") {
    //_getColorByEvent('WIP');
    return "WIP";
  } else if (status == "STR_STDBY_DSPTH") {
    //_getColorByEvent('WIP');
    return "WIP";
  } else if (status == "STR_STDBY_DLVR") {
    //_getColorByEvent('WIP');
    return "Standby Given";
  } else if (status == "STR_STDBY_PICK") {
    //_getColorByEvent('WIP');

    return "RESOLVED";
  } else if (status == "STR_FLTY_PFR") {
    //_getColorByEvent('WIP');
    return "WIP";
  } else if (status == "STR_STDBY-DLVR_FP") {
    //_getColorByEvent('WIP');
    return "Standby Given";
  } else if (status == "STR_REP_DLVRD_FP") {
    //_getColorByEvent('WIP');
    return "RESOLVED";
  } else if (status == "STR_REPD_NTBP") {
    //_getColorByEvent('WIP');
    return "RESOLVED";
  } else if (status == "STR_TRNS_IM") {
    //_getColorByEvent('WIP');
    return "WIP";
  } else if (status == "STR_TRNS_TRC_RPR") {
    //_getColorByEvent('WIP');
    return "WIP";
  } else if (status == "STR_TRNS_PRTNR_RPR") {
    //_getColorByEvent('WIP');
    return "WIP";
  } else if (status == "STR_FLTY_NTBP") {
    //_getColorByEvent('WIP');
    return "WIP";
  } else if (status == "STR_TBD") {
    //_getColorByEvent('WIP');
    return "WIP";
  } else if (status == "STR_PKUP") {
    //_getColorByEvent('WIP');
    return "WIP";
  } else if (status == "STR_PDC") {
    //_getColorByEvent('WIP');
    return "On Hold";
  } else if (status == "STR_DEL_ENR") {
    if (lastRemark == "Spare Delivered Faulty Picked up") {
      return "WIP";
    } else if (lastRemark == "Spare Delivered Faulty pending to pick") {
      return "WIP";
    } else if (lastRemark == "Standby Delivered Faulty Picked up") {
      return "Standby Provided";
    } else if (lastRemark == "Standby Delivered Faulty pending to pick") {
      return "Standby Provided";
    } else if (lastRemark == "Unit replacement Delivered Faulty Picked up") {
      return "WIP";
    } else if (lastRemark ==
        "Unit replacement Delivered Faulty pending to pick") {
      return "WIP";
    } else if (lastRemark == "Spare delivered after repair") {
      return "WIP";
    } else if (lastRemark == "Full unit delivered after repair") {
      return "WIP";
    }
    //_getColorByEvent('WIP');

  } else if (status == "STR_DEL_ER") {
    //_getColorByEvent('WIP');
    return "WIP";
  } else if (status == "STR_RESOLVED") {
    //_getColorByEvent('WIP');

    return "Resolved";
  } else if (status == "STR_HOVR_TRC_RPR") {
    //_getColorByEvent('WIP');
    return "WIP";
  } else if (status == "STR_HOVR_PTR_RPR") {
    //_getColorByEvent('WIP');
    return "WIP";
  } else if (status == "STR_REP") {
    //_getColorByEvent('WIP');
    return "WIP";
  } else if (status == "STR_UREP") {
    //_getColorByEvent('WIP');
    return "WIP";
  }

  if (status == "PART_NEW") {
    //_getColorByEvent('WIP');
    return 'WIP';
  }
  if (status == "PART_ACCEPT") {
    //_getColorByEvent('WIP');
    return 'WIP';
  }
  if (status == "PART_REJECT") {
    //_getColorByEvent('WIP');
    return 'WIP';
  } else if (status == "PART_WIP") {
    //_getColorByEvent('WIP');
    return 'WIP';
  } else if (status == "PART_ASGN_PRTNR") {
    //_getColorByEvent('WIP');
    return "WIP";
  } else if (status == "PART_ASGN_PENGNR") {
    //_getColorByEvent('WIP');
    return "WIP";
  } else if (status == "PART_SPR_REQ") {
    //_getColorByEvent('WIP');
    return "WIP";
  } else if (status == "PART_ASGN_ENGNR") {
    //_getColorByEvent('WIP');
    return "WIP";
  } else if (status == "PART_PDC") {
    //_getColorByEvent('WIP');
    return "On Hold";
  } else if (status == "PART_RESOLVED") {
    //_getColorByEvent('WIP'); // TICKET_CANCELLED
    //
    return "RESOLVED";
  } else if (status == "TICKET_CANCELLED") {
    //_getColorByEvent('WIP');
    return "Ticket Cancelled";
  } else if (status == "PART_WPS") {
    //_getColorByEvent('WIP');
    return "WIP";
  } else if (status == "PART_UOB") {
    //_getColorByEvent('WIP');
    return "Under Observation";
  } else if (status == "TRC_NEW") {
    //_getColorByEvent('WIP');
    return "WIP";
  } else if (status == "TRC_WIP") {
    //_getColorByEvent('WIP');
    return "WIP";
  } else if (status == "TRC_REPAIRED") {
    //_getColorByEvent('WIP');
    return "WIP";
  } else if (status == "TRC_NOT_REPAIRABLE") {
    //_getColorByEvent('WIP');
    return "WIP";
  }
}
