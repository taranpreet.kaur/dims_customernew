class IssueSubCategory{
String subCategoryName;


IssueSubCategory({

  this.subCategoryName,

});

factory IssueSubCategory.fromJson(Map<String, dynamic> json) {
  return IssueSubCategory(
    subCategoryName: json["subCategoryName"] as String,

  );
}

static Map<String, dynamic> toMap(IssueSubCategory music) =>
    {
      'subCategoryName': music.subCategoryName,

    };
}