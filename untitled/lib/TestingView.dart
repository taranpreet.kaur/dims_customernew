import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';


class UserListScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _UserListState();
  }
}

class _UserListState extends State<UserListScreen> {

  String apiData = "";
  List<dynamic> users = [];

  void getTicketDetails() async {
    http.Response response = await http.post(
        Uri.parse("http://35.200.255.51:8080/api/ticket/ticketDetail"), body: {
      "ticketID": "622088659cea65211c1f02ab"
    });

    List<dynamic> dynamicList = jsonDecode(response.body);
    users.addAll(dynamicList);
    setState(() {

    });
  }


  @override
  void initState() {
    super.initState();
    getTicketDetails();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('User List'),
      ),
      body: Container(
        child: SingleChildScrollView(
          child: ListView.separated(
            shrinkWrap: true,

            physics: const BouncingScrollPhysics(
                parent: AlwaysScrollableScrollPhysics()),
            separatorBuilder: (context, index) =>
                Divider(
                  color: Colors.transparent,
                ),
            itemCount: users.length,
            itemBuilder: (context, index) {
              List<dynamic> skillList = [];
              users.forEach((element) {
                Map<String, dynamic> elementMap = element;
                if (elementMap['engineer']['skills'] != null) {
                  print("TTTTTT => " +
                      elementMap['engineer']['skills'].toString());
                  skillList = elementMap['engineer']['skills'];
                }

              });

              return Column(
                  children: [
              Text("Index => "+index.toString
                  ()),
                    ListView.builder(
                    shrinkWrap: true,
                      itemBuilder: (context,index)

                  {
                   return Container(
                     child: Text(skillList[index].toString()),
                   );
                  },

                  itemCount: skillList.length)

              ],
              );
            },
          ),
        ),
      ),
    );
  }
}