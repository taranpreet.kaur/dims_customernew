import 'package:customer/globals.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

import 'Constant.dart';
import 'form_model.dart';

class SignUPOtherDetail extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return SignUPOtherDetailView();
  }
}

class SignUPOtherDetailView extends State<SignUPOtherDetail> {
  dynamic selectedCompany = null;
  List<dynamic> companyList = [];
  final model = FormModel();
  String companyName;

  void getCountries() async {
    var url = Constants.base_url + "api/extranet/erpCustomerList";
    var request = http.MultipartRequest('POST', Uri.parse(url));
    print("request is" + request.toString());
    request.fields['domain_name'] = Globals.domainName;
    var response = await request.send();
    var result = await http.Response.fromStream(response);
    print("Response => "+result.body);
    if (result != null && result.statusCode == 200) {
      dynamic jsonMap = jsonDecode(result.body);
      if(jsonMap != null){
        if(jsonMap['result'] != null){
          // if(jsonMap['result']['customerarray'] != null){
            companyList = jsonMap['result']['customerarray'];
            if(companyList != null && companyList.length > 0){
              selectedCompany = companyList[0];
            }

            setState(() {

            });
          // }else{
          //   EasyLoading.showToast("data not found");
          // }
        }
      }else{
        EasyLoading.showToast("data not found");
      }
    }else{
      EasyLoading.showToast("Failed to get data");
    }
  }

  @override
  void initState() {
    super.initState();
    getCountries();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        flexibleSpace: Image(
          image: AssetImage('assets/appbar_background.png'),
          fit: BoxFit.cover,
        ),
        centerTitle: true,
        title: Text(
          "SIGNUP Other Details",
        ),
      ),
      body: Container(
        padding:EdgeInsets.all(20.0),
        child: Column(

          children: [
            selectedCompany == null
                ? Container(height: 0,width: 0,)
                : DropdownButton<dynamic>(
                    itemHeight: 50,
                    underline: Container(
                      height: 1,
                      width: MediaQuery.of(context).size.width,
                      color: Colors.black,
                    ),
                    isExpanded: true,
                    value: selectedCompany,
                    items: companyList.map((dynamic value) {
                      return DropdownMenuItem<dynamic>(
                        value: value,
                        child: Text(value['name']),
                      );
                    }).toList(),
                    onChanged: (value) {
                      selectedCompany = value;
                      setState(() {});
                    },
                  ),
            TextFormField(
              decoration: InputDecoration(
                labelText: "OTP",
                hintText: "OTP",
              ),
              keyboardType: TextInputType.text,
              maxLength: 50,
              validator: (value) {
                if (value.isEmpty) {
                  return 'Please enter name';
                }
                return null;
              },
              onSaved: (value) {
                //model.companyName = value;
              },
            ),
            TextFormField(
              decoration: InputDecoration(
                labelText: "Password",
                hintText: "Password",
              ),
              keyboardType: TextInputType.text,
              maxLength: 50,
              validator: (value) {
                if (value.isEmpty) {
                  return 'Please enter name';
                }
                return null;
              },
              onSaved: (value) {
                //model.companyName = value;
              },
            ),
            TextFormField(
              decoration: InputDecoration(
                labelText: "Confirm Password",
                hintText: "Confirm Password",
              ),
              keyboardType: TextInputType.text,
              maxLength: 50,
              validator: (value) {
                if (value.isEmpty) {
                  return 'Please enter name';
                }
                return null;
              },
              onSaved: (value) {
                //model.companyName = value;
              },
            ),
            SizedBox(height: 10.0,),
            ElevatedButton(
              child: Container(
                width:MediaQuery.of(context).size.width,
                height: 50.0,
                child:Center(child: Text('Submit',style: TextStyle(
                  fontSize: 16.0
                ),)),
              ),

              onPressed: () {
                print("Tap is worling now");
                /*if (_formKey.currentState.validate()) {
                  _formKey.currentState.save();
                  print(model);

                  // Navigator.push(context,MaterialPageRoute(builder: (context) => DomainSelection()),);
                  var address = model.emailAddress.toString();

                  var parts = address.split('@');
                  var prefix = parts[1].trim();
                  print("==prefix"+prefix);
                  //  model.companyName = selectedCompany;
                  signUp(context,model.name.toString(),model.emailAddress.toString(),model.password.toString(),
                      model.phoneNumber.toString(),model.companyName.toString());
                  // if(validatePassword(model.password.toString())){
                  //   signUp(context,model.name.toString(),model.emailAddress.toString(), model.password.toString(), model.phoneNumber.toString(),prefix);
                  //
                  // }
                  // else {
                  //   EasyLoading.showToast("Please Enter Correct format of password");
                  // }
                  // EasyLoading.show(status: "Loading...");
                  // Scaffold.of(_formKey.currentContext).showSnackBar(
                  //     SnackBar(content: Text('Processing Data')));
                }*/
              },
            )
          ],
        ),
      ),
    );
  }
}
