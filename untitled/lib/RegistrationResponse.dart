import 'CompanyInfo.dart';

class RegistrationResponse{
  CompanyInfo companyinfo;

  RegistrationResponse(
      {
        this.companyinfo

      });

  factory RegistrationResponse.fromJson(Map<String, dynamic> json) {
    return RegistrationResponse(companyinfo : CompanyInfo.fromJson(json['companies']));
  }

}