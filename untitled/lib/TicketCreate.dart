// import 'dart:convert';
// import 'dart:developer';
// import 'dart:io';
// import 'dart:ui';
// import 'package:camera/camera.dart';
// import 'package:customer/Details1.dart';
// import 'package:date_time_picker/date_time_picker.dart';
// import 'package:firebase_messaging/firebase_messaging.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter/services.dart';
// import 'package:geocoding/geocoding.dart';
// import 'package:uiblock/uiblock.dart';
// import 'package:customer/Constant.dart';
// import 'package:customer/IssueCategory.dart';
// import 'package:customer/SubCategory.dart';
// import 'package:customer/take_picture_page.dart';
// import 'package:geolocator/geolocator.dart';
// import 'package:flutter_easyloading/flutter_easyloading.dart';
// import 'package:google_place/google_place.dart';
// //import 'package:geocoder/geocoder.dart';
// import 'package:http/http.dart' as http;
// import 'package:image_picker/image_picker.dart';
// import 'package:shared_preferences/shared_preferences.dart';
// import 'Dashboard.dart';
// import 'package:intl/intl.dart';
// import 'form_model_ticket_create.dart';
// import 'dart:async';
// import 'package:flutter/cupertino.dart';
// import 'package:http_parser/http_parser.dart';

import 'dart:convert';
import 'dart:developer';
import 'dart:io';
import 'dart:ui';
import 'package:customer/Details1.dart';
import 'package:geocoding/geocoding.dart';

//import 'package:geolocator/geolocator.dart';
import 'package:camera/camera.dart';
import 'package:date_time_picker/date_time_picker.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_place/google_place.dart' as google;
import 'package:uiblock/uiblock.dart';
import 'package:customer/Constant.dart';
import 'package:customer/IssueCategory.dart';
import 'package:customer/SubCategory.dart';
import 'package:customer/take_picture_page.dart';

//import 'package:geolocator/geolocator.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:google_place/google_place.dart' as google;

//import 'package:geocoder/geocoder.dart';
import 'package:http/http.dart' as http;
import 'package:image_picker/image_picker.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'Dashboard.dart';
import 'package:intl/intl.dart';
import 'form_model_ticket_create.dart';
import 'dart:async';
import 'package:flutter/cupertino.dart';
import 'package:http_parser/http_parser.dart';

class TicketCreate extends StatefulWidget {
  final String address, lat, lng;

  // DetailsPage({Key key, this.placeId, this.googlePlace}) : super(key: key);

  TicketCreate({Key key, this.address, this.lat, this.lng}) : super();

  @override
  MyImagePickerState createState() => MyImagePickerState(address, lat, lng);
}

class MyImagePickerState extends State<TicketCreate> {
  String address, lat, lng;
  String _platformMessage = 'No Error';
  List images;
  int maxImageNo = 10;
  bool selectSingleImage = false;
  String _error = 'No Error Dectected';
  TextEditingController assetId = new TextEditingController();
  TextEditingController userName = new TextEditingController();
  TextEditingController assetType = new TextEditingController();
  TextEditingController callMode = new TextEditingController();
  TextEditingController callType = new TextEditingController();

//  TextEditingController MakeControllerasset = new TextEditingController();
  TextEditingController address1 = new TextEditingController();
  TextEditingController area = new TextEditingController();
  TextEditingController state1 = new TextEditingController();
  TextEditingController city1 = new TextEditingController();
  TextEditingController pincode = new TextEditingController();

  // TextEditingController country = new TextEditingController();
  TextEditingController email1 = new TextEditingController();
  TextEditingController mobileNo = new TextEditingController();
  TextEditingController issue = new TextEditingController();
  TextEditingController typeOfRequest1 = new TextEditingController();
  TextEditingController department1 = new TextEditingController();
  TextEditingController severity1 = new TextEditingController();
  TextEditingController accountmanager1 = new TextEditingController();
  TextEditingController serviceitemno1 = new TextEditingController();
  TextEditingController servicetype1 = new TextEditingController();
  TextEditingController assestwarranty1 = new TextEditingController();
  TextEditingController contactno1 = new TextEditingController();
  TextEditingController contactperson1 = new TextEditingController();
  TextEditingController custname1 = new TextEditingController();
  TextEditingController spareName = new TextEditingController();
  TextEditingController sparePartNo = new TextEditingController();
  TextEditingController quantity = new TextEditingController();
  TextEditingController comment = new TextEditingController();
  TextEditingController SpareassetMake = new TextEditingController();
  TextEditingController assetModel = new TextEditingController();
  List<IssueCategory> categories;
  google.GooglePlace googlePlace;
  google.DetailsResult detailsResult;
  var assetserial,
      contractType,
      contractstartdate,
      contractenddate,
      servicetype,
      responsetime,
      resolutiontime,
      accountmanager,
      severityString,
      serviceitemno,
      assestwarranty,
      contactno,
      contactperson,
      swstarttime,
      swendtime,
      contractcalllimit,
      secNdslaresolution_Hours,
      contractExpire,
      sun,
      mon,
      tue,
      wed,
      thru,
      fri,
      sat,
      custcode;
  var contractno;
  final model = FormModelTicketCreation();
  var _controller = TextEditingController(text: 'subcategory');
  var typeOfRequest;
  var department;
  String dropdownValue;
  String drop;
  var type, dept;
  static const kGoogleApiKey = "AIzaSyBqU4wR7dFqFKT-QTCjAXVEGqvYUCu4jEQ";
  String _sessionToken;
  google.GooglePlace _places;
  List<google.AutocompletePrediction> predictions = [];

  // GoogleMapsPlaces _places = GoogleMapsPlaces(apiKey: kGoogleApiKey);
  List<String> _locations = ['Please choose a location', 'A', 'B', 'C', 'D'];
  String _selectedLocation = 'Select Slot';
  String hello = "Printers&Scanners-Hardware";
  double _height;
  double _width;
  String _setTime, _setDate;
  String _hour, _minute, _time;
  String dateTime;
  DateTime selectedDate = DateTime.now();
  TimeOfDay selectedTime = TimeOfDay(hour: 00, minute: 00);
  TextEditingController _dateController = TextEditingController();
  TextEditingController _timeController = TextEditingController();

  List<String> nameList = [];
  List<String> list = [];

  List<SubCategory> nameList1 = [];
  var isLoading = false;

  var tempList = List<String>();

  // var  De
  XFile _image;
  String _path = null;

  final _formKey = GlobalKey<FormState>();
  List<SubCategory> nnn = [];
  List<dynamic> _placeList = [];

  final startTime = TimeOfDay(hour: 9, minute: 0);
  final endTime = TimeOfDay(hour: 22, minute: 0);
  final step = Duration(minutes: 30);

  List<String> _timeslot = [
    'Select Slot',
    '9:00AM to 11:00AM',
    '11:00AM to 1:00PM',
    '1:00PM to 3:00PM',
    '3:00PM to 5:00PM',
    '5:00PM to 7:00PM',
    '7:00PM to 9:00PM',
    '9:00PM to 11:00PM',
    '11:00PM to 1:00AM',
    '1:00AM to 3:00AM',
    '3:00AM to 5:00AM',
    '5:00AM to 7:00AM',
    '7:00AM to 9:00AM'
  ]; // Option 2
  String category1, subcategory1;
  final _emailRegExp = RegExp(
      r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+");

  MyImagePickerState(this.address, this.lat, this.lng);

  Future<bool> _onBackPressed() {
    Navigator.pushAndRemoveUntil<dynamic>(
      context,
      MaterialPageRoute<dynamic>(
        builder: (BuildContext context) => Dashboard(),
      ),
      (route) => false, //if you want to disable back feature set to false
    );
  }

  SharedPreferences sharedPreferences;

  String test;
  var domain_name, email;

  getprefs() async {
    sharedPreferences = await SharedPreferences.getInstance();
    var name = sharedPreferences.get("name");
    email = sharedPreferences.get("email");
    domain_name = sharedPreferences.get('domain_name');
    email1.text = email;
    userName.text = name;
  }

  bool isDepartmentStore = false;

  /* Widget SparepartRequest()
  {
    if(Department == "STORE"){

    }else
      {
        return Container();
      }

  }*/

  @override
  Widget build(BuildContext context) {
    // final times = getTimes(startTime, endTime, step)
    //     .map((tod) => tod.format(context))
    //     .toList();
    //
    // print(times);

    if (address != null) {
      address1.text = address;
    }

    _height = MediaQuery.of(context).size.height;
    _width = MediaQuery.of(context).size.width;
    dateTime = DateFormat.yMd().format(DateTime.now());
    Color color1 = _colorFromHex("#00ABC5");

    Widget spareColumn = isDepartmentStore
        ? Column(
            children: [
              Container(
                padding: EdgeInsets.all(15),
                child: Align(
                  child: Text(
                    'Spare Part Request',
                    textAlign: TextAlign.left,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                  ),
                  alignment: Alignment.centerLeft,
                ),
              ),
              Container(
                padding: EdgeInsets.fromLTRB(15, 15, 15, 15),
                child: TextFormField(
                  controller: spareName,
                  maxLength: 250,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Spare Name',
                  ),
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Please enter a Spare Name';
                    }
                    return null;
                  },
                  onSaved: (value) {
                    model.SpareName = value;
                  },
                ),
              ),
              Container(
                padding: EdgeInsets.fromLTRB(15, 15, 15, 15),
                child: TextFormField(
                  controller: sparePartNo,
                  maxLength: 250,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Spare Part Number',
                  ),
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Please enter a Spare Name';
                    }
                    return null;
                  },
                  onSaved: (value) {
                    model.SparePartNo = value;
                  },
                ),
              ),
              Container(
                padding: EdgeInsets.fromLTRB(15, 15, 15, 15),
                child: TextFormField(
                  controller: quantity,
                  maxLength: 10,
                  keyboardType: TextInputType.number,
                  inputFormatters: <TextInputFormatter>[
                    FilteringTextInputFormatter.digitsOnly
                  ],
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Quantity',
                  ),
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Please enter a Spare Name';
                    }
                    return null;
                  },
                  onSaved: (value) {
                    model.SpareQuantity = value;
                  },
                ),
              ),
              Container(
                padding: EdgeInsets.fromLTRB(15, 15, 15, 15),
                child: TextFormField(
                  controller: SpareassetMake,
                  maxLength: 250,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Make',
                  ),
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Please enter a Asset Make';
                    }
                    return null;
                  },
                  onSaved: (value) {
                    model.assetmake = value;
                  },
                ),
              ),
              Container(
                padding: EdgeInsets.fromLTRB(15, 15, 15, 15),
                child: TextFormField(
                  controller: assetModel,
                  maxLength: 250,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Model',
                  ),
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Please enter a Model';
                    }
                    return null;
                  },
                  onSaved: (value) {
                    model.assetmodel = value;
                  },
                ),
              ),
              Container(
                padding: EdgeInsets.fromLTRB(15, 15, 15, 15),
                child: TextFormField(
                  controller: comment,
                  maxLength: 250,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Comment',
                  ),
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Please enter a comment';
                    }
                    return null;
                  },
                  onSaved: (value) {
                    model.comment = value;
                  },
                ),
              ),
            ],
          )
        : Container(
            height: 0,
            width: 0,
          );

    return FlutterEasyLoading(
      child: Scaffold(
        appBar: AppBar(
          title: Text("Create Ticket"),
          flexibleSpace: Image(
            image: AssetImage('assets/appbar_background.png'),
            fit: BoxFit.cover,
          ),
          backgroundColor: Colors.transparent,
        ),
        body: WillPopScope(
          onWillPop: _onBackPressed,
          //
          // child: ListView(
          //     children: <Widget>[
          // Form(
          //   key: _formKey,
          // child: Column(
          //   mainAxisAlignment: MainAxisAlignment.center,
          //   crossAxisAlignment: CrossAxisAlignment.center,
          //   children: <Widget>[

          child: Form(
            key: _formKey,
            child: ListView(
              children: [
                Container(
                  padding: EdgeInsets.all(15),
                  child: Align(
                    child: Text(
                      'Customer Info',
                      textAlign: TextAlign.left,
                      overflow: TextOverflow.ellipsis,
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                    ),
                    alignment: Alignment.centerLeft,
                  ),
                ),
                Container(
                    padding: EdgeInsets.all(15),
                    // child: Row(
                    //
                    // children: <Widget>[
                    child: TextFormField(
                      controller: assetId,
                      maxLength: 25,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        hintText: "Enter Asset Id",
                        labelText: "Enter Asset Id",
                        suffixIcon: IconButton(
                          onPressed: () => getAssetData(assetId.text, context),
                          icon: Icon(Icons.check),
                        ),
                      ),
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Please enter a Asset Id';
                        }
                        return null;
                      },
                      onSaved: (value) {
                        model.assetId = value;
                      },
                    )
                    // child: FlatButton(
                    //   child: Text('Check', style: TextStyle(fontSize: 20.0),),
                    //   color: Colors.blueAccent,
                    //   textColor: Colors.white,
                    //   onPressed: () {},
                    // ),
                    // ]

                    ),

                Container(
                    padding: EdgeInsets.all(15),
                    // child: Row(
                    //
                    // children: <Widget>[
                    child: TextFormField(
                      controller: userName,
                      maxLength: 50,
                      decoration: InputDecoration(
                        labelText: "User Name",
                        border: OutlineInputBorder(),
                      ),
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Please enter a UserName';
                        }
                        return null;
                      },
                      onSaved: (value) {
                        model.userName = value;
                      },
                    )
                    // child: FlatButton(
                    //   child: Text('Check', style: TextStyle(fontSize: 20.0),),
                    //   color: Colors.blueAccent,
                    //   textColor: Colors.white,
                    //   onPressed: () {},
                    // ),
                    // ]
                    ),
                Container(
                  padding: EdgeInsets.all(15),
                  child: TextFormField(
                    controller: email1,
                    maxLength: 80,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Email',
                    ),
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Please enter a Email';
                      } else if (!_emailRegExp.hasMatch(value)) {
                        return 'Invalid email address!';
                      }
                      return null;
                    },
                    onSaved: (value) {
                      model.email = value;
                    },
                  ),
                ),
                Container(
                  padding: EdgeInsets.all(15),
                  child: TextFormField(
                    keyboardType: TextInputType.number,
                    maxLength: 10,
                    inputFormatters: <TextInputFormatter>[
                      FilteringTextInputFormatter.digitsOnly
                    ],
                    // Only numbers can be entered
                    controller: mobileNo,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Mobile No.',
                    ),
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Please enter a Mobile No';
                      } else if (value.length < 10) {
                        return 'Enter 10 digit mobile no';
                      }
                      return null;
                    },
                    onSaved: (value) {
                      model.mobileNo = value;
                    },
                  ),
                ),

                Container(
                  padding: EdgeInsets.all(15),
                  child: Align(
                    child: Text(
                      'Contract Info',
                      textAlign: TextAlign.left,
                      overflow: TextOverflow.ellipsis,
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                    ),
                    alignment: Alignment.centerLeft,
                  ),
                ),

                Container(
                  padding: EdgeInsets.all(15),
                  child: TextFormField(
                    controller: custname1,
                    readOnly: true,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Customer Name',
                    ),
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Please enter a Customer Name';
                      }
                      return null;
                    },
                    onSaved: (value) {
                      model.customerName = value;
                    },
                  ),
                ),

                Container(
                  padding: EdgeInsets.all(15),
                  child: TextFormField(
                    controller: assetType,
                    readOnly: true,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Asset Type',
                    ),
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Please enter Asset Type';
                      }
                      return null;
                    },
                    onSaved: (value) {
                      model.assetType = value;
                    },
                  ),
                ),
                Container(
                  padding: EdgeInsets.fromLTRB(15, 15, 15, 15),
                  child: TextFormField(
                    readOnly: true,
                    controller: callMode,
                    decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        labelText: 'Call Mode',
                        enabled: true),
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Please enter a Call Mode';
                      }
                      return null;
                    },
                    onSaved: (value) {
                      model.callMode = value;
                    },
                  ),
                ),
                Container(
                  padding: EdgeInsets.fromLTRB(15, 15, 15, 15),
                  child: TextFormField(
                    readOnly: true,
                    autofocus: false,
                    focusNode: FocusNode(),
                    enableInteractiveSelection: false,
                    controller: callType,
                    decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        labelText: 'Service Type',
                        enabled: true),
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Please enter a Service Type';
                      }
                      return null;
                    },
                    onSaved: (value) {
                      model.callType = value;
                    },
                  ),
                ),
                /*Container(
        padding: EdgeInsets.fromLTRB(15, 15, 15, 15),
        child: TextFormField(
          readOnly: true,
          autofocus: false,
          focusNode: FocusNode(),
          enableInteractiveSelection: false,
          controller: MakeControllerasset,
          decoration: InputDecoration(
              border: OutlineInputBorder(),
              labelText: 'Asset make',
              enabled: true
          ),
          validator: (value) {
            if (value.isEmpty) {
              return 'Please enter a Service Type';
            }
            return null;
          },
          onSaved: (value) {
            model.assetmake = value;
          },

        ),
      ),*/

                Container(
                  padding: EdgeInsets.all(15),
                  child: Align(
                    child: Text(
                      'Service Details',
                      textAlign: TextAlign.left,
                      overflow: TextOverflow.ellipsis,
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                    ),
                    alignment: Alignment.centerLeft,
                  ),
                ),

                new GestureDetector(
                  onTap: () {
                    issueCategory();
                    print("Container clicked");
                  },
                  child: Container(
                    padding: EdgeInsets.all(15),
                    child: DropdownButtonFormField<String>(
                      // hint: Text("Issue Category"),
                      value: drop,
                      decoration: InputDecoration(
                          border: OutlineInputBorder(),
                          labelText: 'Issue Category',
                          hintText: 'Issue Category'),
                      validator: (value) =>
                          value == null ? 'Please enter Issue Category' : null,
                      onChanged: (String newValue) {
                        setState(() {
                          drop = newValue;

                          // nameList1.clear();
                          // typeOfRequest1.clear();
                          // department1.clear();
                          //  list.remove(true);
                          list.clear();
                          getSubCategory(newValue);
                          getTypeandDepartment(drop, dropdownValue);
                          // typeOfRequest1.text="Type Of Request";
                          // department1.text="Department";
                        });
                      },
                      items: nameList.map((String value) {
                        return DropdownMenuItem<String>(
                          value: value,
                          child: Row(
                            children: <Widget>[
                              SizedBox(
                                width: 10,
                              ),
                              Text(
                                value,
                                style: TextStyle(color: Colors.black),
                              ),
                            ],
                          ),
                        );
                      }).toList(),
                    ),
                  ),
                ),

                new GestureDetector(
                  onTap: () {
                    // issueCategory();
                    print("Container clicked");
                  },
                  child: Container(
                    padding: EdgeInsets.all(15),
                    child: DropdownButtonFormField<String>(
                      // hint: Text("Issue Category"),
                      value: dropdownValue,

                      decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        labelText: 'Issue SubCategory',
                      ),
                      validator: (value) => value == null
                          ? 'Please enter Issue SubCategory'
                          : null,
                      onChanged: (String newValue) {
                        setState(() {
                          dropdownValue = newValue;

                          // nameList1.clear();
                          // typeOfRequest1.clear();
                          // department1.clear();
                          //  list.remove(true);
                          // list.clear();
                          getTypeandDepartment(drop, dropdownValue);

                          // typeOfRequest1.text="Type Of Request";
                          // department1.text="Department";
                        });
                      },
                      items: list.map((String value) {
                        return DropdownMenuItem<String>(
                          value: value,
                          child: Row(
                            children: <Widget>[
                              SizedBox(
                                width: 10,
                              ),
                              Text(
                                value,
                                style: TextStyle(color: Colors.black),
                              ),
                            ],
                          ),
                          onTap: () => {
                            //           // print("===ssss"+map.SubCategoryName+map.TypeOfRequest+map.Department)
                            dropdownValue = value
                            //           // typeOfRequest1.text=map.TypeOfRequest,
                            //           // department1.text=map.Department,
                            //
                          },
                        );
                      }).toList(),
                    ),
                  ),
                ),

                //   Container(
                //   padding: EdgeInsets.all(15),
                //   child: DropdownButtonFormField<String>(
                //     // hint: Text("Issue Sub Category"),
                //     value: dropdownValue,
                //     decoration:InputDecoration(
                //       border: OutlineInputBorder(),
                //       labelText: 'Issue Sub Category',
                //     ),
                //     validator: (value) => value == null
                //         ? 'Please enter Issue Sub Category' : null,
                //     onChanged: (String newValue) {
                //       setState(() {
                //
                //
                //         typeOfRequest1.text =typeOfRequest;
                //         // department1.text = typeOfRequest;
                //       });
                //     },
                //     items: list.map((SubCategory map) {
                //
                //       // print("namelist111313" + nameList1.toString());
                //       return DropdownMenuItem<String>(
                //
                //
                //         value: map.SubCategoryName,
                //
                //
                //         child: Row(
                //           children: <Widget>[
                //             // SizedBox(
                //             //   width: 10,
                //             // ),
                //             Text(
                //               map.SubCategoryName , style: TextStyle(color: Colors.black),),
                //
                //
                //           ],
                //
                //         ),
                //
                //         onTap:() =>{
                //           // print("===ssss"+map.SubCategoryName+map.TypeOfRequest+map.Department)
                //           // dropdownValue=map.SubCategoryName,
                //           // typeOfRequest1.text=map.TypeOfRequest,
                //           // department1.text=map.Department,
                //
                //         },
                //       );
                //     }).toList(),
                //
                //   ),
                //
                // ),

                Container(
                  padding: EdgeInsets.fromLTRB(15, 15, 15, 15),
                  child: TextFormField(
                    controller: issue,
                    maxLength: 250,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Issue Description',
                    ),
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Please enter a Issue Description';
                      }
                      return null;
                    },
                    onSaved: (value) {
                      model.issueDescription = value;
                    },
                  ),
                ),
                ///// For SparePartrequest //////
                spareColumn,

                // Container(
                //   padding: EdgeInsets.fromLTRB(15, 15, 15, 15),
                //   margin: EdgeInsets.fromLTRB(0, 5, 0, 0),
                //   child: TextFormField(
                //     controller: typeOfRequest1,
                //     decoration: InputDecoration(
                //       border: OutlineInputBorder(),
                //       labelText: 'Type Of Request',
                //     ),
                //     validator: (value) {
                //       if (value.isEmpty) {
                //         return 'Please enter a Type Of Request';
                //       }
                //       return null;
                //     },
                //     onSaved: (value) {
                //       model.typeOfRequest = value;
                //     },
                //
                //   ),
                // ),
                // Container(
                //   padding: EdgeInsets.fromLTRB(15, 15, 15, 15),
                //   margin: EdgeInsets.fromLTRB(0, 5, 0, 0),
                //
                //   child: TextFormField(
                //
                //     controller: department1,
                //     decoration: InputDecoration(
                //       border: OutlineInputBorder(),
                //       labelText: 'Department',
                //     ),
                //     validator: (value) {
                //       if (value.isEmpty) {
                //         return 'Please enter Department';
                //       }
                //       return null;
                //     },
                //     onSaved: (value) {
                //       model.department = value;
                //     },
                //
                //   ),
                // ),
                // Container(child: Text("Attachment"),
                //   padding: EdgeInsets.fromLTRB(30, 20, 10, 0),
                // ),

                // Container(
                //   padding: EdgeInsets.all(15),
                //   margin: EdgeInsets.fromLTRB(0, 5, 0, 0),
                //
                //   child: TextFormField(
                //
                //     controller: _dateController,
                //     decoration: InputDecoration(
                //       border: OutlineInputBorder(),
                //       labelText: 'Scheduled Date',
                //       suffixIcon: IconButton(
                //         onPressed: () =>
                //
                //             _selectDate(context),
                //
                //         icon: Icon(Icons.calendar_today),
                //       ),
                //     ),
                //
                //     // onSaved: (value) {
                //     //   model.mobileNo = value;
                //     // },
                //   ),
                // ),

                // Container(
                //   margin: EdgeInsets.fromLTRB(10, 10, 10, 5),
                //   padding: EdgeInsets.fromLTRB(5, 0, 0, 0),
                //
                //   child: Align(
                //
                //     alignment: Alignment.centerLeft,
                //     child:Text(
                //       'Scheduled Time',
                //       style: TextStyle(
                //         fontStyle: FontStyle.normal,
                //       ),
                //       textAlign: TextAlign.left,
                //     ),),),
                //
                //

                // Container(
                //   padding: EdgeInsets.all(10),
                //
                //   margin: EdgeInsets.fromLTRB(10,10,10,10),
                //   decoration: BoxDecoration(
                //       border: Border.all(color: Colors.black54)
                //   ),
                //   child: Row(
                //     children: <Widget>[
                //
                //
                //       InkWell(
                //         onTap: () {
                //           // _selectTime(context);
                //         },
                //         child: Container(
                //           margin: EdgeInsets.fromLTRB(10, 0, 0, 0),
                //           child: Align(alignment: Alignment.centerRight,
                //
                //             child: DropdownButton(
                //
                //               hint: Text('Select Slot'),
                //               underline: Container(
                //                 height: 1.0,
                //                 decoration: const BoxDecoration(
                //                     border: Border(bottom: BorderSide(color: Colors.transparent, width: 0.0))
                //                 ),),
                //               // Not necessary for Option 1
                //               value: _selectedLocation,
                //               onChanged: (newValue) {
                //                 setState(() {
                //                   dynamic currentTime = DateFormat.jm().format(DateTime.now());
                //                   print("currentTime"+currentTime);
                //                   _selectedLocation = newValue;
                //                 });
                //               },
                //
                //               items: _timeslot.map((location) {
                //                 return DropdownMenuItem(
                //                   child: new Text(location),
                //                   value: location,
                //                 );
                //               }).toList(),
                //             ),),
                //
                //         ),
                //       ),
                //     ],
                //   ),
                //
                // ),

                Container(
                    height: 170,
                    width: 170,
                    padding: EdgeInsets.all(15),
                    child: GestureDetector(
                      onTap: () {
                        _showPicker(context);
                      },
                      child: CircleAvatar(
                        radius: 55,
                        backgroundColor: _colorFromHex("#00ABC5"),
                        child: _path == null
                            ? Image.asset("assets/camera.png")
                            : Image.file(
                                File(_path),
                                height: 100,
                                width: 100,
                                fit: BoxFit.cover,
                              ),
                      ),
                    )),
                //     : Container(
                //   decoration: BoxDecoration(
                //       color: Colors.grey[200],
                //       borderRadius: BorderRadius.circular(50)),
                //   width: 100,
                //   height: 100,
                //   child: Icon(
                //     Icons.camera_alt,
                //     color: Colors.grey[800],
                //   ),
                // ),
                //

                Container(
                  padding: EdgeInsets.all(15),
                  child: Align(
                    child: Text(
                      'Address',
                      textAlign: TextAlign.left,
                      overflow: TextOverflow.ellipsis,
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                    ),
                    alignment: Alignment.centerLeft,
                  ),
                ),

                Container(
                  padding: EdgeInsets.fromLTRB(15, 15, 15, 15),
                  child: Center(
                    child: Column(
                      children: <Widget>[
                        Align(
                          alignment: Alignment.topCenter,
                          child: TextFormField(
                            controller: address1,
                            readOnly: false,
                            decoration: InputDecoration(
                              labelText: "Address",
                              border: OutlineInputBorder(),
                              enabled: true,

                              // prefixIcon: Icon(Icons.map),
                              // suffixIcon: IconButton(
                              //   icon: Icon(Icons.cancel),
                              // ),
                            ),
                            onChanged: (value) {
                              if (value.isNotEmpty) {
                                autoCompleteSearch(value);
                              } else {
                                if (predictions.length > 0 && mounted) {
                                  setState(() {
                                    predictions = [];
                                  });
                                }
                              }
                            },
                          ),
                        ),
                        ListView.builder(
                          physics: NeverScrollableScrollPhysics(),
                          shrinkWrap: true,
                          itemCount: predictions.length,
                          itemBuilder: (context, index) {
                            return ListTile(
                              title: Text(predictions[index].description),
                              onTap: () {
                                setState(() {
                                  address1.text =
                                      predictions[index].description;
                                  getDetils(predictions[index].placeId);
                                  predictions.clear();
                                });

                                debugPrint(predictions[index].placeId);
                              },
                            );
                          },
                        )
                      ],
                    ),
                  ),
                ),

                Container(
                  padding: EdgeInsets.fromLTRB(15, 15, 15, 15),
                  child: TextFormField(
                    controller: area,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Area',
                    ),
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Please enter a Area';
                      }
                      return null;
                    },
                    onSaved: (value) {
                      model.area = value;
                    },
                  ),
                ),
                Container(
                  padding: EdgeInsets.fromLTRB(15, 15, 15, 15),
                  child: TextFormField(
                    controller: city1,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'City',
                    ),
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Please enter a City';
                      }
                      return null;
                    },
                    onSaved: (value) {
                      model.city = value;
                    },
                  ),
                ),
                Container(
                  padding: EdgeInsets.fromLTRB(15, 15, 15, 15),
                  child: TextFormField(
                    controller: state1,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'State',
                    ),
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Please enter a State';
                      }
                      return null;
                    },
                    onSaved: (value) {
                      model.state = value;
                    },
                  ),
                ),
                Container(
                  padding: EdgeInsets.fromLTRB(15, 15, 15, 15),
                  child: TextFormField(
                    controller: pincode,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Pincode',
                      suffixIcon: IconButton(
                        onPressed: () =>
                            pincodeVerification(pincode.text, context),
                        icon: Icon(Icons.person_pin_circle),
                      ),
                    ),
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Please enter a Pincode';
                      }
                      return null;
                    },
                    onSaved: (value) {
                      model.pincode = value;
                    },
                  ),
                ),
                // Container(
                //   padding: EdgeInsets.fromLTRB(15, 15, 15, 15),
                //   child: TextFormField(
                //     readOnly: true,
                //     decoration: InputDecoration(
                //         border: OutlineInputBorder(),
                //         labelText: 'India',
                //         enabled: false
                //     ),
                //   ),
                // ),

                Container(
                    height: 50,
                    padding: EdgeInsets.fromLTRB(20, 0, 20, 10),
                    margin: EdgeInsets.fromLTRB(20, 0, 20, 0),
                    child: RaisedButton(
                        textColor: Colors.white,
                        color: color1,
                        child: Text('Submit'),
                        onPressed: () {
                          print("New Tap working");
                          // print("==drop"+drop+dropdownValue);
                          DateTime now = DateTime.now();
                          DateFormat("yyyy-MM-dd hh:mm:ss")
                              .format(DateTime.now());
                          // if (_formKey.currentState.validate()) {
                          //   _formKey.currentState.save();
                          if (assetId.text == "") {
                            EasyLoading.showToast("Please Enter AssetId");
                          } else if (userName.text == '') {
                            EasyLoading.showToast("Please Enter UserName");
                          } else if (email1.text == '') {
                            EasyLoading.showToast("Please Enter Email");
                          } else if (!_emailRegExp.hasMatch(email1.text)) {
                            EasyLoading.showToast("Please Enter Correct Email");
                          } else if (mobileNo.text == '') {
                            EasyLoading.showToast("Please Enter MobileNo");
                          } else if (mobileNo.text.length < 10) {
                            EasyLoading.showToast(
                                "Please Enter 10 digit MobileNo");
                          } else if (drop == null) {
                            ;
                            EasyLoading.showToast("Please Enter IssueCategory");
                          } else if (dropdownValue == null) {
                            EasyLoading.showToast(
                                "Please Enter IssueSubCategory");
                          } else if (issue.text == '') {
                            EasyLoading.showToast(
                                "Please Enter Issue Description");
                          } else if (address1.text == '') {
                            EasyLoading.showToast("Please Enter Address");
                          } else if (area.text == '') {
                            EasyLoading.showToast("Please Enter Area");
                          } else if (city1.text == '') {
                            EasyLoading.showToast("Please Enter City");
                          } else if (state1.text == "") {
                            EasyLoading.showToast("Please Enter State");
                          } else if (pincode.text == "") {
                            EasyLoading.showToast("Please Enter Pincode");
                          } else if (custname1.text == "") {
                            EasyLoading.showToast("Please Enter Cutomer Name");
                          } else if (assetType.text == "") {
                            EasyLoading.showToast("Please Enter AssetType");
                          } else if (callMode.text == "") {
                            EasyLoading.showToast("Please Enter CallMode");
                          } else if (servicetype1.text == "") {
                            EasyLoading.showToast("Please Enter ServiceType");
                          } else {
                            print("printing msg");
                            createTicket(
                                spareName.text,
                                sparePartNo.text,
                                quantity.text,
                                SpareassetMake.text,
                                assetModel.text,
                                comment.text,
                                assetId.text,
                                assetType.text,
                                callMode.text,
                                callType.text,
                                address1.text,
                                area.text,
                                city1.text,
                                state1.text,
                                pincode.text,
                                DateFormat("yyyy-MM-dd hh:mm:ss")
                                    .format(DateTime.now()),
                                email1.text,
                                mobileNo.text,
                                drop,
                                dropdownValue,
                                type,
                                dept,
                                issue.text,
                                context,
                                contractType);
                            // }
                          }
                        }
                        // print("formattedDate"+formattedDate);

                        // signUp(name.text,email.text, password.text,phoneNumber.text);

                        )),

                //  TextFormField(
                //     controller: address1,
                // decoration: InputDecoration(
                // border: OutlineInputBorder(),
                // labelText: 'Address',),
                //
                //
                //
                //
                //   onChanged: (value) {
                //     if (value.isNotEmpty) {
                //       autoCompleteSearch(value);
                //     } else {
                //       if (predictions.length > 0 && mounted) {
                //         setState(() {
                //           predictions = [];
                //         });
                //       }
                //     }
                //   },
                //
                //
                //    validator: (value) {
                //      if (value.isEmpty) {
                //        return 'Please enter a Address';
                //      }
                //      return null;
                //    },
                //    onSaved: (value) {
                //      model.address = value;}
                // ),
                //     SizedBox(
                //       height: 10,
                //     ),
                //     Expanded(
                //       child: ListView.builder(
                //         itemCount: predictions.length,
                //         itemBuilder: (context, index) {
                //           return ListTile(
                //             leading: CircleAvatar(
                //               child: Icon(
                //                 Icons.pin_drop,
                //                 color: Colors.white,
                //               ),
                //             ),
                //             title: Text(predictions[index].description),
                //             onTap: () {
                //               getDetils(predictions[index].placeId);
                //
                //               debugPrint(predictions[index].placeId);
                //
                //             },
                //           );
                //         },
                //       ),
                //     ),
              ],
            ),
          ),
        ),

        // ],)
      ),
    );
  }

  Widget _buildLoginButton() {
    // var response= sharedPreferences.getString("success");
    print("checkData " + sharedPreferences.getString("success"));
    if (sharedPreferences.getString("success") != null &&
        sharedPreferences.getString("success") == "200") {
      Color color1 = _colorFromHex("#00ABC5");

      return Container(
          height: 50,
          padding: EdgeInsets.fromLTRB(20, 0, 20, 10),
          margin: EdgeInsets.fromLTRB(20, 0, 20, 0),
          child: RaisedButton(
            textColor: Colors.white,
            color: color1,
            child: Text('Submit'),
            onPressed: () {
              print("This is working fine");
              DateTime now = DateTime.now();
              DateFormat("yyyy-MM-dd hh:mm:ss").format(DateTime.now());
              if (_formKey.currentState.validate()) {
                _formKey.currentState.save();
                createTicket(
                    spareName.text,
                    sparePartNo.text,
                    quantity.text,
                    SpareassetMake.text,
                    assetModel.text,
                    comment.text,
                    assetId.text,
                    assetType.text,
                    callMode.text,
                    callType.text,
                    address1.text,
                    area.text,
                    city1.text,
                    state1.text,
                    pincode.text,
                    DateFormat("yyyy-MM-dd hh:mm:ss").format(DateTime.now()),
                    email1.text,
                    mobileNo.text,
                    drop,
                    dropdownValue,
                    typeOfRequest1.text,
                    department1.text,
                    issue.text,
                    context,
                    contractType);
              }
              // print("formattedDate"+formattedDate);

              // signUp(name.text,email.text, password.text,phoneNumber.text);
            },
          ));
    } else {
      return Container();
    }
  }

  getTypeandDepartment(catName, subCatName) async {
    EasyLoading.show(status: "Loading..");
    print("==typeandDept" + catName + subCatName);
    var url = Constants.base_url + "api/extranet/issueDeptDetails";
    var request = await http.MultipartRequest('POST', Uri.parse(url));
    request.fields['catName'] = catName;
    request.fields['subCatName'] = subCatName;
    request.fields['assetType'] = assetType.text.toUpperCase();
    print("==request is this" + request.toString());
    var response = await request.send();
    if (response.statusCode == 200) {
      EasyLoading.dismiss(animation: false);

      // setState(() {
      //   isLoading =false;
      //   // EasyLoading.dismiss(animation: false);
      // });
      print("==response success get Asset data");
      var result = await http.Response.fromStream(response);
      print("RRRRR => " + result.body.toString());
      var rr = json.decode(result.body.toString());
      // var corddata = json.decode(result.body.toString()[0].location[cordinates]

      type = rr['result'][0]["TypeOfRequest"];
      dept = rr['result'][0]["Department"];
      print("Dept and request" + type + dept);
      if (dept == "STORE") {
        isDepartmentStore = true;
      } else {
        isDepartmentStore = false;
      }
      setState(() {});
      typeOfRequest1.text = type.toString();
      department1.text = dept.toString();
    }
  }

  Color _colorFromHex(String hexColor) {
    final hexCode = hexColor.replaceAll('#', '');
    return Color(int.parse('FF$hexCode', radix: 16));
  }

  void getDetils(String placeId) async {
    print("check" + placeId);

    var result = await this.googlePlace.details.get(placeId);
    print("datacoming" + result.toString());
    if (result != null && result.result != null && mounted) {
      print("==detailsResultss" + detailsResult.toString());
      setState(() async {
        detailsResult = result.result;
        print("==detailsResult" + detailsResult.toString());
        // for (var i=0;i<=detailsResult.adrAddress.length;i++){
        //
        //   var longname=detailsResult.adrAddress[i]['long_name'];
        //   var short_name=detailsResult.adrAddress[i]['short_name'];
        //   var type=detailsResult.adrAddress[i]['types'];
        //
        //   print(longname+short_name+type);
        // }
        // lat=detailsResult.geometry.location.lat.toString();
        // lng=detailsResult.geometry.location.lng.toString();

        lat = detailsResult.geometry.location.lat.toString();
        lng = detailsResult.geometry.location.lng.toString();
        print("The lat and long is" + lat.toString() + lng.toString());
        address1.text = detailsResult.formattedAddress.toString();
        //List<Placemark> placemark = await Geolocator().placemarkFromAddress(address1.text);
        List<Placemark> placemark = await placemarkFromCoordinates(
            detailsResult.geometry.location.lat,
            detailsResult.geometry.location.lng);
        city1.text = placemark[0].locality;
        area.text = detailsResult.name;
        state1.text = placemark[0].administrativeArea;
        pincode.text = placemark[0].postalCode;
        // area.text=detailsResult.name;
        // state1.text="";
        // ;
        // pincode.text="";

        // Navigator.push(
        //   context,
        //   MaterialPageRoute(
        //     builder: (context) => TicketCreate(
        //         address: detailsResult.formattedAddress.toString(),
        //         lat: detailsResult.geometry.location.lat.toString(),
        //         lng:detailsResult.geometry.location.lng.toString()
        //
        //
        //     ),
        //   ),
        // );
      });

      // if (result.result.photos != null) {
      //   for (var photo in result.result.photos) {
      //   }
      // }
    }
  }

  String validateMobile(String value) {
// Indian Mobile number are of 10 digit only
    if (value.length != 10)
      return 'Mobile Number must be of 10 digit';
    else
      return null;
  }

  void autoCompleteSearch(String value) async {
    var result = await googlePlace.autocomplete.get(value);
    if (result != null && result.predictions != null && mounted) {
      setState(() {
        predictions = result.predictions;
      });
    }
  }

  var isreadOnly = false;

  bool isEmpty() {
    setState(() {
      if ((address1.text != " ")) {
        isreadOnly = true;
      } else {
        isreadOnly = false;
      }
    });
    return isreadOnly;
  }

  ///// for create ticket changes machineserialnumber behalf of assetiD,

  createTicket(
      String spareName,
      sparePartNo,
      quantity,
      assetmake,
      assetModel,
      comment,
      assetId,
      assetType,
      callMode,
      callType,
      address,
      area,
      city,
      state,
      pincode,
      formattedDate,
      email,
      mobile,
      category,
      subcategory,
      typeofrequest,
      department,
      issue,
      BuildContext context,
      contractType) async {
    EasyLoading.show(status: "Loading..");
    print("==subcategory" + subcategory);
    print("==address" + address + area);
    // DateTime tempDate = new DateFormat("dd-MM-yyyy – kk:mm").parse(
    //     formattedDate);zz
    // DateTime tempDate = new DateFormat("yyyy-MM-dd hh:mm:ss").parse(formattedDate);
    // DateTime tempDate = new DateFormat("dd-MM-yyyy hh:mm:ss").parse(formattedDate);

    final date = '2021-01-26T03:17:00.000000Z';
    DateTime parseDate = new DateFormat("yyyy-MM-dd").parse(contractstartdate);
    var inputDate = DateTime.parse(parseDate.toString());
    var outputFormat = DateFormat('dd-MM-yyyy');
    var outputDate = outputFormat.format(inputDate);
    print("contract strt date" + outputDate);
    var url = Constants.base_url + "api/ticket/index";
    var request = await http.MultipartRequest('POST', Uri.parse(url));
    //  request.headers['token'] = "dIn671UnQ7ySQI5VJzIGeN:APA91bEKoGU3dcqpD5Dgi_fYLkJPrmAr6H0Ys9Qcs3jVLE7-rOZZ45L1kWHUAe0vP37UpOgvDo0pDdpazCD1fs8dH3rBd7aobUKM50nf_LhGqyYxRgIN55LzT0PzTebYGatQAjpmB0MW";
    print("address" + address);
    final prefs = await SharedPreferences.getInstance();
    var customerId = prefs.get("userId");
    request.fields['userName'] = userName.text;
    request.fields['assetId'] = assetId;
    request.fields['assetType'] = assetType;
    // request.fields['ticket_type']="company";
    request.fields['callMode'] = callMode;
    request.fields['callType'] = callType;
    request.fields['curr_lat'] = lat.toString();
    request.fields['curr_long'] = lng.toString();
    request.fields['line1'] = address;
    request.fields['line2'] = area;
    request.fields['city'] = city;
    request.fields['state'] = state;
    request.fields['pincode'] = pincode;
    request.fields['customerId'] = customerId;
    request.fields['sla_start_time'] = formattedDate;
    request.fields['email'] = email;
    request.fields['phoneNo'] = mobileNo.text;
    request.fields['issue'] = issue;
    request.fields['schedule'] = _dateController.text;
    request.fields['scheduledTime'] = _selectedLocation;
    request.fields['custName'] = custname1.text;
    request.fields['contactPerson'] = userName.text;
    request.fields['contactno'] = contactno;
    request.fields['assestwarranty'] = assestwarranty1.text;
    request.fields['servicetype'] = servicetype1.text;
    request.fields['serviceitemno'] = serviceitemno1.text;
    request.fields['severity'] = severity1.text;
    request.fields['accountmanager'] = accountmanager1.text;
    // request.fields['status'] = "UNASSIGNED";
    // request.fields['userId'] = customerId;
    request.fields['issueCategory'] = category;
    request.fields['issueSubCategory'] = subcategory;
    request.fields['typeOfRequest'] = typeofrequest;
    request.fields['department'] = department;
    request.fields['createdBy'] = "CUSTOMER";
    request.fields['source'] = "M";
    request.fields['assetSerial'] = assetserial;
    request.fields['contracttype'] = contractType;
    request.fields['contractStartdate'] = outputDate;
    request.fields['contractEnddate'] = contractenddate;
    request.fields['servicetype'] = servicetype;
    request.fields['responsetime_Hours'] = responsetime;
    request.fields['resolutiontime_Hours'] = resolutiontime;
    request.fields['assetMake'] = assetmake;
    request.fields['assetModel'] = assetModel;
    request.fields['spareName'] = spareName;
    request.fields['sparePartNo'] = sparePartNo;
    request.fields['quantity'] = quantity;
    request.fields['comment'] = comment;
    request.fields['serviceitemno'] = serviceitemno;
    request.fields['swStarttime'] = swstarttime;
    request.fields['swEndtime'] = swendtime;
    request.fields['contractcalllimit'] = contractcalllimit;
    print("pathak code" + custcode);
    request.fields['custCode'] = custcode;
    request.fields['priority'] = "MEDIUM";
    request.fields['secNdslaresolution_Hours'] = secNdslaresolution_Hours;
    request.fields['contractExpired'] = contractExpire;
    request.fields['sun'] = sun;
    request.fields['mon'] = mon;
    request.fields['tue'] = tue;
    request.fields['wed'] = wed;
    request.fields['thu'] = thru;
    request.fields['fri'] = fri;
    request.fields['sat'] = sat;
    request.fields['contractNo'] = contractno;

    // request.fields['userName']="Ishika";
    // request.fields['assetId'] = "SIU-84020";
    // request.fields['assetType'] = "desktop";
    // // request.fields['ticket_type']="company";
    // request.fields['callMode'] = "amc";
    // request.fields['callType'] = "comprehensive";
    // request.fields['curr_lat'] = "28.5199";
    // request.fields['curr_long'] = " 77.4587239";
    // request.fields['line1'] = " C&S ELECTRIC LTD PLOT NO-63";
    // request.fields['line2'] = " NSEZ Noida Phase-2";
    // request.fields['city'] = "Noida";
    // request.fields['state'] = " UTTAR PRADESH";
    // request.fields['pincode'] = " 201305";
    // request.fields['customerId'] = "undefined";
    // request.fields['sla_start_time'] = "Tue Nov 09 2021 15:28:12 GMT+0530 (India Standard Time)";
    // request.fields['email'] = "helpdesk.ittc@cselectric.co.in";
    // request.fields['phoneNo'] = "8010009340";
    // request.fields['issue'] = " Dell USB KEYBOARD  not working";
    // request.fields['schedule'] = " 9-11-2021";
    // request.fields['scheduledTime'] = "undefined";
    // request.fields['custName'] = " c&S Electric Limited";
    // request.fields['contactPerson'] = "Dinesh";
    // request.fields['contactno'] = "8010009340";
    // request.fields['assestwarranty'] = " 2022-05-31";
    // request.fields['servicetype'] = "comprehensive";
    // request.fields['serviceitemno'] = "SIU-84020";
    // request.fields['severity'] = "severity 3";
    // request.fields['accountmanager'] = " vipul Kumar Mishra";
    // // request.fields['status'] = "UNASSIGNED";
    // request.fields['userId'] = customerId;
    // request.fields['issueCategory']="Hardware";
    // request.fields['issueSubCategory']="Keyboard Problem";
    // request.fields['typeOfRequest']="Incident";
    // request.fields['department']="STORE";
    // request.fields['createdBy']="CCC";
    // request.fields['source']="API";
    // request.fields['assetSerial']="Gsm7102";
    // request.fields['contractType']="amc";
    // request.fields['contractStartdate']=" 2021-06-01";
    // request.fields['contractEnddate']=" 2022-05-31";
    // request.fields['servicetype']="comprehensive";
    // request.fields['responsetime_Hours']="4";
    // request.fields['resolutiontime_Hours']="8";
    // request.fields['assetMake']="dell Inc./Optiplex 3020";
    // request.fields['assetModel']="t4t4t4";
    // request.fields['serviceitemno']="SIU-84020";
    // request.fields['swStarttime']="9:00:00 Am";
    // request.fields['swEndtime']="6:00:00 Pm";
    // request.fields['contractcalllimit']=" 4t4t4t4";
    // print("pathak code" + custcode);
    // request.fields['custCode']=" c36028";
    // request.fields['priority']="MEDIUM";
    // request.fields['secNdslaresolution_Hours']=secNdslaresolution_Hours;
    // request.fields['contractExpired']="false";
    // request.fields['sun']="false";
    // request.fields['mon']="true";
    // request.fields['tue']="true";
    // request.fields['wed']="true";
    // request.fields['thu']="true";
    // request.fields['fri']="true";
    // request.fields['sat']="true";
    // request.fields['contractNo']="con-Del-8907";
    print("the assetmake is" + assetmake.toString());
    print("Create Ticket params => " + jsonEncode(request.fields));
    String allParams = jsonEncode(request.fields);
    print("The all params is" + allParams.toString());

    print("dateschedule" + _dateController.text + swstarttime + swendtime);
    print("ticketcreate" + contactno + serviceitemno);
    print("ticketcreate" +
        "Asset" +
        "resolution" +
        resolutiontime +
        responsetime +
        servicetype +
        "contract " +
        contractenddate +
        contractstartdate +
        contractType +
        "assetserial" +
        assetserial +
        "accountmanager" +
        accountmanager +
        severity1.text);
    // request.files.add(
    //   http.MultipartFile(
    //     'file',
    //     path.readAsBytes().asStream(),
    //     path.lengthSync(),
    //     filename: path,
    //     contentType: MediaType('image','jpeg'),
    //   ),
    // );

    if (_path == null || _path == "") {
      // request.files.add(
      //     http.MultipartFile(
      //         'picture',""));

    } else {
      print("image path" + _path);
      request.files.add(
        http.MultipartFile(
          "tckAttachments",
          File(_path).readAsBytes().asStream(),
          File(_path).lengthSync(),
          filename: "test.${_path.split(".").last}",
          contentType: MediaType("image", "${_path.split(".").last}"),
        ),
      );
    }

    String cust = custname1.text;
    print("==create ticket request" + request.toString());
    var response = await request.send();
    if (response.statusCode == 200) {
      EasyLoading.dismiss(animation: false);

      print("==response success ticket create");
      var result = await http.Response.fromStream(response);

      var jsonResponse = json.decode(result.body);
      print("==response" + jsonResponse.toString());

      String id = jsonResponse["ticket"]["ticketSerialNo"];
      print("==tickeet create id: " + id);
      EasyLoading.showToast(
          "Ticket Created Successfully.Your Ticket id is $id");
      getMessage();
      addNotification(customerId, "Ticket Created", "Your ticket id is $id");
      String add = address + area + city + state + pincode;
      // postRequest(assetId, id, email);
      sendEmail1(id, assetId, email, add, custname1.text, context);
      // Navigator.push(
      //   context, MaterialPageRoute(builder: (context) => Dashboard()),);

      Navigator.pushAndRemoveUntil<dynamic>(
        context,
        MaterialPageRoute<dynamic>(
          builder: (BuildContext context) => Dashboard(),
        ),
        (route) => false, //if you want to disable back feature set to false
      );
    } else if (response.statusCode == 400) {
      var result = await http.Response.fromStream(response);
      var jsonResponse = json.decode(result.body);
      print("==response" + jsonResponse.toString());
      EasyLoading.dismiss(animation: false);
    } else {}
  }

  // Future<Null> displayPrediction(Prediction p) async {
  //   if (p != null) {
  //     PlacesDetailsResponse detail = await _places.getDetailsByPlaceId(p.placeId);
  //     var placeId = p.placeId;
  //     lat = detail.result.geometry.location.lat;
  //     lng = detail.result.geometry.location.lng;
  //     address = detail.result.formattedAddress;
  //     print("==ticket lat places " + lat.toString());
  //     print("==ticket lng places " + lng.toString());
  //     print(address);
  //     address1.text = address;
  //   }
  // }

  // void onError(PlacesAutocompleteResponse response) {
  //   print("error: " + response.errorMessage);
  // }

  // Future<void> getSubCategoryList(String newValue) async {
  //
  //   var url=Constants.base_url+"api/extranet/issueDetailOther";
  //
  //   print("newvalue" + newValue);
  //   var request = http.MultipartRequest(
  //       'POST', Uri.parse(url));
  //   print("request" + request.toString());
  //   request.fields['catName'] = newValue;
  //   var response = await request.send();
  //   SnackBar(content: Text('Processing Data'));
  //   if (response.statusCode == 200) {
  //
  //     var result = await http.Response.fromStream(response);
  //     var jsonResponse = jsonDecode(result.body);
  //     print("==response" + jsonResponse.toString());
  //     setState(() {
  //       var data = json.decode(result.body);
  //       print("dtata" + data.toString());
  //       var customerarray=[];
  //       customerarray = data["issue"] as List;
  //       print("customerarraty" + customerarray.toString());
  //
  //       //   var subcategoryName = customerarray[i1]["SubCategoryName"];
  //       //   var id=customerarray[i1]["_id"];
  //       //   var typeOfRequest=customerarray[i1]["TypeOfRequest"];
  //       //   var serviceType=customerarray[i1]["Department"];
  //       //   print("==subcategory"+subcategoryName.toString());
  //
  //       // if(list!=null){
  //       //   list.clear();
  //       //   list.remove(true);
  //       list = customerarray.map<SubCategory>((json) => SubCategory.fromJson(json))
  //           .toList();
  //       list.sort((a, b) => a.SubCategoryName.compareTo(b.SubCategoryName));
  //
  //       dropdownValue=list[0].SubCategoryName;
  //       // typeOfRequest1.text=list[0].TypeOfRequest;
  //       // department1.text=list[0].Department;
  //       // }
  //
  //
  //
  //       // nnn=list;
  //       // for(var ii=0;ii<=nnn.length;ii++){
  //       //  nameList1.add();
  //       //  typeOfRequest1.text=nnn[ii].TypeOfRequest;
  //       //  department1.text=nnn[ii].Department;
  //       // }
  //
  //       // typeOfRequest = customerarray[i1]['TypeOfRequest'];
  //       // department = customerarray[i1]['Department'];
  //       // print("==subcategory" + subcategoryName.toString());
  //       // }
  //     });
  //   }
  //   else {
  //     print("==response error" + response.toString());
  //   }
  // }

  void _showcontent() {
    showDialog(
      context: context, barrierDismissible: false, // user must tap button!

      builder: (BuildContext context) {
        return new AlertDialog(
          title: new Text('Error !'),
          content: new SingleChildScrollView(
            child: new ListBody(
              children: [
                new Text("Cant't be submit right now."),
              ],
            ),
          ),
          actions: [
            new FlatButton(
              child: new Text('Ok'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  void getMessage() {
    // final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
    // _firebaseMessaging.configure(
    //     onMessage: (Map<String, dynamic> message) async {
    //       print('==on message $message');
    //       // setState(() => _message = message["notification"]["title"]);
    //     },
    //     // onBackgroundMessage: myBackgroundMessageHandler,
    //     onResume: (Map<String, dynamic> message) async {
    //       print('==on resume $message');
    //       // setState(() => _message = message["notification"]["title"]);
    //     }, onLaunch: (Map<String, dynamic> message) async {
    //   print('==on launch $message');
    //   // setState(() => _message = message["notification"]["title"]);
    // });
    FirebaseMessaging.onMessage.listen((RemoteMessage message) {
      print('Got a message whilst in the foreground!');
      print('Message data: ${message.data}');
      // _message = message.data["notification"]["title"];

      if (message.notification != null) {
        print('Message also contained a notification: ${message.notification}');
      }
    });
  }

  void _showPicker(context) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return SafeArea(
            child: Container(
              child: new Wrap(
                children: <Widget>[
                  new ListTile(
                      leading: new Icon(Icons.photo_library),
                      title: new Text('Photo Library'),
                      onTap: () {
                        _imgFromGallery();
                        Navigator.of(context).pop();
                      }),
                  new ListTile(
                    leading: new Icon(Icons.photo_camera),
                    title: new Text('Camera'),
                    onTap: () {
                      _imgFromCamera();
                      Navigator.of(context).pop();
                    },
                  ),
                ],
              ),
            ),
          );
        });
  }

  _imgFromCamera() async {
    // var  image = await ImagePicker.pickImage(source: ImageSource.camera, imageQuality: 5);
    //   // path =image.path;
    //   setState(() {
    //     _image = image;
    //   });

    final cameras = await availableCameras();
    final camera = cameras.first;

    // final result = await Navigator.push(
    //     context,
    //     MaterialPageRoute(
    //         builder: (context) => TakePicturePage(camera: camera)));
    XFile xFile =
        await ImagePicker.platform.getImage(source: ImageSource.camera);
    setState(() {
      _path = xFile.path;
    });
  }

  _imgFromGallery() async {
    final ImagePicker _picker = ImagePicker();

    _image = await ImagePicker()
        .pickImage(source: ImageSource.gallery, imageQuality: 50);
    // path=image.path;
    setState(() {
      _path = _image.path;
    });
  }

  Future<Null> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
      context: context,
      initialDate: selectedDate,
      initialDatePickerMode: DatePickerMode.day,
      firstDate: DateTime.now().subtract(Duration(days: 0)),
      lastDate: DateTime(2100),
    );
    if (picked != null) var now1 = DateTime.now();
    print("selectDate" +
        DateFormat('dd-MM-yyyy').format(DateTime.now()).toString() +
        "selected date" +
        DateFormat('dd-MM-yyyy').format(selectedDate));

    if (DateFormat('dd-MM-yyyy').format(selectedDate).toString() !=
        DateFormat('dd-MM-yyyy').format(DateTime.now())) {
      print("not equal");
    } else if (DateFormat('dd-MM-yyyy').format(selectedDate).toString() ==
        DateFormat('dd-MM-yyyy').format(DateTime.now())) {
      print("equal");
    }
    setState(() {
      selectedDate = picked;
      _dateController.text = DateFormat('dd-MM-yyyy').format(selectedDate);
      // final df = new DateFormat('dd-MM-yyyy hh:mm a');
      // int myvalue = 1558432747;
      // print(df.format(_dateController.text.toString());
    });
  }

  @override
  void initState() {
    String kPLACES_API_KEY = kGoogleApiKey;
    googlePlace = google.GooglePlace(kPLACES_API_KEY);
    _dateController.text = "Select Date";
    _timeController.text = "Select Time".toString();
    super.initState();
    // _loadCounter();
    getprefs();
  }

  // List<IssueCategory> categories;

  Future<String> getAssetData(String serialId, BuildContext context) async {
    EasyLoading.show(status: "Loading..");
    // setState(() {
    //   isLoading =true;
    //   CircularProgressIndicator();
    //  // EasyLoading.show(status: "Loading..");
    // });

    var url = Constants.base_url + "api/cust/user/getAssetData";
    print("==assetId" + serialId.toString());
    var request = await http.MultipartRequest('POST', Uri.parse(url));
    request.fields['serial_id'] = serialId.toString();
    request.fields['domain_name'] = domain_name.toString();
    print("domin" + domain_name);
    request.fields['email'] = email;
    print("email" + email);
    print("==request" + request.toString());
    var response = await request.send();
    print("==request" + request.toString());
    print("==serialId" + serialId);
    if (response.statusCode == 200) {
      EasyLoading.dismiss(animation: false);
      sharedPreferences.setString("success", "200");

      setState(() {
        if (nameList != null) {
          nameList.clear();
        }
        if (list != null) {
          list.clear();
        }
        // drop="Select Category";
        // dropdownValue="Select SubCategory";
        // isLoading =false;
        // EasyLoading.dismiss(animation: false);
      });
      print("==response success get Asset data");
      var result = await http.Response.fromStream(response);
      var data = json.decode(result.body.toString());
      print("Ticket Creation Screen" + data.toString());
      var status = data["status"].toString();
      if (status == "1") {

        var address = data["address1"].toString();
        var address2 = data["address2"].toString();
        address1.text = address.toString();
        // var email = data["email"].toString();
        area.text = address2.toString();
        var postalcode = data["postalcode"].toString();
        pincode.text = postalcode.toString();
        var city = data["city"].toString();
        city1.text = city.toString();
        var calltype = data["calltype"].toString();
        var assettype = data["assettype"].toString();
        var custname = data["custname"].toString();
        // print("customer Name" + custname);
        //  as =data["asset Make"].toString();
        //   print("Assets Make => "+assetMake.toString());
        SpareassetMake.text = data["asset Make"].toString();
        // MakeControllerasset.text = data["asset Make"].toString();
        contractstartdate = data["contractstartdate"].toString();
        contractenddate = data["contractenddate"].toString();
        responsetime = data["responsetime_Hours"].toString();
        resolutiontime = data["resolutiontime_Hours"].toString();
        contractType = data["contracttype"].toString();
        custcode = data['custcode'].toString();
        assetserial = data["assetserial"].toString();
        accountmanager = data["accountmanager"].toString();
        severityString = data["severity"].toString();
        serviceitemno = data["serviceitemno"].toString();
        servicetype = data["servicetype"].toString();
        assestwarranty = data["assestwarranty"].toString();
        contactno = data["contactno"].toString();
        contactperson = data["contactperson"].toString();
        swstarttime = data['swstarttime'].toString();
        swendtime = data['swendtime'].toString();
        secNdslaresolution_Hours = data['2ndslaresolution_Hours'].toString();
        contractno = data['contractno'].toString();
        contractcalllimit = data['contractcalllimit'].toString();
        var state = data["state"].toString();
        var callmode = data["callmode"].toString();
        state1.text = state.toString();
        var asset = toBeginningOfSentenceCase(assettype);
        var cm = toBeginningOfSentenceCase(callmode);
        var ct = toBeginningOfSentenceCase(calltype);
        contractExpire = data['contractexpired'].toString();
        sun = data['sun'].toString();
        mon = data['mon'].toString();
        tue = data['tue'].toString();
        wed = data['wed'].toString();
        thru = data['thu'].toString();
        fri = data['fri'].toString();
        sat = data['sat'].toString();
        assetType.text = asset.toString();
        callType.text = ct.toString();
        callMode.text = cm.toString();
        // email1.text = email.toString();
        var sentence = toBeginningOfSentenceCase(custname); // This is a string
        custname1.text = sentence;
        accountmanager1.text = accountmanager;
        severity1.text = severityString;
        serviceitemno1.text = serviceitemno;
        servicetype1.text = servicetype;
        assestwarranty1.text = assestwarranty;
        contactno1.text = contactno;
        contactperson1.text = contactperson;
        var completeadd = address1.text + "," + area.text + "," + pincode.text;
        //var addresses = await Geocoder.local.findAddressesFromQuery(completeadd);
        List<Location> locations = await locationFromAddress(completeadd);
        var first = locations.first;
        lat = first.latitude.toString();
        lng = first.longitude.toString();

        if (lat.isEmpty) {
          EasyLoading.showToast(
              "Contract details not fetch completely,Kindly Re-fetch");
        } else {
          sharedPreferences.setString("fail", "400");
          EasyLoading.showToast("Contract details fetch successfully");
        }

        print("jjj" + lat.toString());
        if (lat.isEmpty && lng.isEmpty) {
          var completeadd = area.text + pincode.text;
          //var addresses = await Geocoder.local.findAddressesFromQuery(completeadd);
          List<Location> locations = await locationFromAddress(completeadd);
          var first = locations.first;
          lat = first.latitude.toString();
          lng = first.longitude.toString();
          print("comming");
        } else if (lat.isEmpty && lng.isEmpty) {
          var completeadd = pincode.text;
          // var addresses = await Geocoder.local.findAddressesFromQuery(completeadd);
          List<Location> locations = await locationFromAddress(completeadd);
          var first = locations.first;
          // lat = first.coordinates.latitude.toString();
          // lng = first.coordinates.longitude.toString();
          lat = first.latitude.toString();
          lng = first.longitude.toString();
          print("comming1");
        }

        // print("ContractType => "+contractType);
        // assetModel=data["asset Model"].toString();
        //  assetmake=data["asset Make"].toString();
        //  print("Assets Make => "+assetMake.toString());
        /*assetmake_controller.text = assetMake.toString();*/
        // EasyLoading.showToast("Invalid AssetId");
      } else if (status == "0") {
        EasyLoading.showToast("Invalid AssetId");
      }
      // There is an open ticket with this asset id

    }
    // else if(response.statusCode == 400){
    //   print(" api error");
    // }
    else {
      print("Not call");
      // var request = await http.MultipartRequest('POST', Uri.parse(url));
      // var response = await request.send();
      // print(response.statusCode);
      // var result = await http.Response.fromStream(response);
      // var jsonResponse = json.decode(result.body);
      //  print("==response" + jsonResponse.toString());
      EasyLoading.showToast("Invalid AssetId.");
    }
  }

  Future<String> pincodeVerification(
      String pincode, BuildContext context) async {
    EasyLoading.show(status: "Loading..");
    // setState(() {
    //   isLoading =true;
    //   CircularProgressIndicator();
    //  // EasyLoading.show(status: "Loading..");
    // });
    print("==assetId" + pincode.toString());
    var url = Constants.base_url + "api/ticket/verifyPin";

    var request = await http.MultipartRequest('POST', Uri.parse(url));
    request.fields['pincode'] = pincode.toString();
    // request.fields['assetType']=domain_name;
    print("==request" + request.toString());
    var response = await request.send();
    UIBlock.block(context);
    if (response.statusCode == 200) {
      EasyLoading.dismiss(animation: false);
      // setState(() {
      //   isLoading =false;
      //   // EasyLoading.dismiss(animation: false);
      // });
      print("==response success get Asset data");
      var result = await http.Response.fromStream(response);
      var data1 = json.decode(result.body.toString());
      print("data" + data1.toString());
      var exit = data1["pincodeExist"];
      if (exit == true) {
        print("===true");
        UIBlock.unblock(context);

        EasyLoading.showToast("available");
      } else {
        UIBlock.unblock(context);

        EasyLoading.showToast("not available");
        print("===false");
      }
    } else {
      print("==response error" + response.toString());
    }
  }

  addNotification(String customerId, title, body) async {
    DateTime currentDate = DateTime.now();
    // final DateFormat formatter = DateFormat('dd-MM-yyyy').format;
    final String formatted = DateFormat('dd-MM-yyyy').format(currentDate);
    var url = Constants.base_url + "api/extranet/add/notification";

    var request = await http.MultipartRequest('POST', Uri.parse(url));
    request.fields['notifier_id'] = customerId;
    request.fields['notification_type'] = "0";
    request.fields['title'] = title;
    request.fields['body'] = body;
    request.fields['entity_id'] = customerId;
    request.fields['date'] = formatted;

    var response = await request.send();
    if (response.statusCode == 200) {
      print("==response success add Notification");
      var result = await http.Response.fromStream(response);
      var data = json.decode(result.body.toString());
      print("==add Notifiation" + data.toString());
      // mobile1.text=mobile.toString();
    } else {
      print("==response ADD NOTIFICATION error" + response.toString());
    }
  }

  var name1;

  issueCategory() async {
    var url = Constants.base_url + "api/extranet/issueCategories";
    var request = await http.MultipartRequest('POST', Uri.parse(url));
    request.fields['assetType'] = assetType.text.toUpperCase();
    var response = await request.send();
    print("==response success issueCategory");
    if (response.statusCode == 200) {
      EasyLoading.dismiss(animation: false);

      // setState(() {
      //   isLoading =false;
      //   // EasyLoading.dismiss(animation: false);
      // });
      var result = await http.Response.fromStream(response);
      var data = json.decode(result.body.toString());
      print("==response success issueCategory" + data.toString());

      // getSubCategoryList(assetType.text.toUpperCase());
      _loadCounter(assetType.text.toUpperCase());
    }
  }

  _loadCounter(String assetType) async {
    nameList.clear();
    EasyLoading.show(status: "Loading");
    var url = Constants.base_url + "api/extranet/issueDetail";

    // String link = "http://10.11.4.59:8080/api/extranet/issueCategories";
    var request = await http.MultipartRequest('POST', Uri.parse(url));
    request.fields['assetType'] = assetType;
    var response = await request.send();
    if (response.statusCode == 200) {
      EasyLoading.dismiss(animation: false);

      var result = await http.Response.fromStream(response);
      var data = json.decode(result.body.toString());
      setState(() {
//2mntsok

        print("dtata" + data.toString());
        var customerarray = data["result"] as List;

        print("==categorylist" + customerarray.toString());
        // var list=customerarray.map<IssueCategory>((json) => IssueCategory.fromJson(json)).toList();
        // for (int i=0;i<list.length;i++){
        //   var ii=list[i].id;
        //   var ii1=list[i].issueCategory;
        //   var uu=new IssueCategory();
        //   uu.id=ii;
        //   uu.issueCategory=ii1;
        //   categories.add(uu);
        // }
        for (int i = 0; i < customerarray.length; i++) {
          var name1 = customerarray[i].toString();
          print("name: " + name1);
          nameList.add(name1);
          nameList.sort();
        }
        drop = nameList[0];
        getSubCategory(drop);
      });
    }
  }

  getSubCategory(String assetType1) async {
    list.clear();
    EasyLoading.show(status: "Loading");
    var url = Constants.base_url + "api/extranet/issueDetailOther";

    // String link = "http://10.11.4.59:8080/api/extranet/issueCategories";
    var request = await http.MultipartRequest('POST', Uri.parse(url));
    request.fields['catName'] = assetType1;
    request.fields['assetType'] = assetType.text.toUpperCase();
    var response = await request.send();
    if (response.statusCode == 200) {
      EasyLoading.dismiss(animation: false);
      var result = await http.Response.fromStream(response);
      var data = json.decode(result.body.toString());
      setState(() {
        print("dtata is" + data.toString());
        // var customerarray = data as List;
        if (data['result'] != null) {
          var customerarray = data['result'] as List;
          //  print("==categorylist"+customerarray.toString());
          // var list=customerarray.map<IssueCategory>((json) => IssueCategory.fromJson(json)).toList();
          // for (int i=0;i<list.length;i++){
          //   var ii=list[i].id;
          //   var ii1=list[i].issueCategory;
          //   var uu=new IssueCategory();
          //   uu.id=ii;
          //   uu.issueCategory=ii1;
          //   categories.add(uu);
          // }
          for (int i = 0; i < customerarray.length; i++) {
            var name1 = customerarray[i].toString();
            print("name: " + name1);
            list.add(name1);
            list.sort();
          }
          dropdownValue = list[0];
          getTypeandDepartment(drop, dropdownValue);
        }
      });
    }
  }

  sendEmail1(String id, assetId, email, address, custName,
      BuildContext context) async {
    var url = Constants.base_url + "api/ticket/createNotify";
    var request = await http.MultipartRequest('POST', Uri.parse(url));
    request.fields['ticketSerial'] = id.toString();
    request.fields['assetId'] = assetId.toString();
    request.fields['email'] = email;
    request.fields['custAddress'] = address;
    request.fields['custName'] = custName;
    print("==request" + request.toString());
    var response = await request.send();
    if (response.statusCode == 200) {
      print("==response success email notifi ");
      var result = await http.Response.fromStream(response);
      var jsonResponse = json.decode(result.body);
      print("==response email notifi" + jsonResponse.toString());
    } else {
      var result = await http.Response.fromStream(response);
      var jsonResponse = json.decode(result.body);
      print("==response error" + result.body.toString());
    }
  }

// Future<http.Response> postRequest (assetid,id,email) async {
//   var url =Constants.base_url+'api/ticket/createNotify';
//
//   Map content = {
//       'assetId': assetid,
//       'ticketSerialNo': id,
//        'email':email
//
//   };
//   //encode Map to JSON
//   var body = json.encode(content);
//
//   var response = await http.post(url,
//
//       body: body
//   );
//
//
//   print("${response.statusCode}");
//   print("${response.body}");
//   return response;
// }
}
