import 'package:flutter/material.dart';

void main() {
  runApp(new MaterialApp(
    title: "Example",
    home: new HideMTabApp(),
  ));
}

class HideMTabApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<HideMTabApp> {
  int _tabBarCount = 1;

  List<Widget> getTabBarList() { // Tab Bar items displayed on the App Bar
    switch (_tabBarCount) {
      case 1:
        return [Tab(icon: Icon(Icons.search))];
      case 2:
        return [
          Tab(icon: Icon(Icons.search)),
          Tab(icon: Icon(Icons.file_download))
        ];
      default:
        return [];
    }
  }

  List<Widget> getTabScreen() { // Screens to be displayed on Tab Bar
    switch (_tabBarCount) {
      case 1:
        return [
          RaisedButton(onPressed: () {
            setState(() {
              _tabBarCount = 2; // Click event, here tab bar count should increse, so that multiple tab bar can be visible.
            });
          }, child: Text('Save'),)
        ];
      case 2:
        return [
          Text('First Screen'),
          Text('Second Screen')
        ];
      default:
        return [];
    }
  }

  Widget build(BuildContext context) {
    return MaterialApp(
      home: DefaultTabController(
        length: _tabBarCount,
        child: Scaffold(
          backgroundColor: Colors.white,
          appBar: AppBar(
            bottom: TabBar(
              tabs: getTabBarList(),
            ),
            title: Text('Tabs Demo'),
          ),
          body: TabBarView(
            children: getTabScreen(),
          ),
        ),
      ),
    );
  }
}